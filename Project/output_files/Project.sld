<sld_project_info>
  <sld_infos>
    <sld_info hpath="DramUser:DramUserInst_c0|ddr3_dimm0:DIMM_0" name="DIMM_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME ddr3_dimm0 HAS_SOPCINFO 1"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="DramUser:DramUserInst_c0|ddr3_dimm1:DIMM_1" name="DIMM_1">
      <assignment_values>
        <assignment_value text="QSYS_NAME ddr3_dimm1 HAS_SOPCINFO 1"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="PCIe_Gen3_AbaloneWrapper:PCIe_0|PCIe_Gen3:PCIe_Gen3|PCIe_Top:PCIe_Top|PCIe_AlteraIPWrapper:PCIe_AlteraIPWrapper|pcie3_ttk_rcd:gen_pcie_ttk_rcd.pcie_ep" name="gen_pcie_ttk_rcd.pcie_ep">
      <assignment_values>
        <assignment_value text="QSYS_NAME pcie3_ttk_rcd HAS_SOPCINFO 1 GENERATION_ID 1416252632"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="sld_hub:auto_hub|alt_sld_fab:\instrumentation_fabric_with_node_gen:instrumentation_fabric" library="alt_sld_fab" name="instrumentation_fabric">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_sld_fab HAS_SOPCINFO 1"/>
      </assignment_values>
    </sld_info>
  </sld_infos>
</sld_project_info>
