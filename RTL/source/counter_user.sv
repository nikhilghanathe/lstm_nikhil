`timescale 1ns/1ps


module counter_user#(parameter SIZE=10, parameter COND = 0) (clk, rst, en, dout);

	localparam ADDR_WIDTH = $clog2(SIZE);

	input 								clk,rst, en;
	output logic [ADDR_WIDTH-1:0]		dout;

	//signals
	logic [ADDR_WIDTH-1:0] 				count;

	always@(posedge clk)begin 
		if(rst==1'b1)begin
			count <= 0;
		end
		else begin
			if(en==1'b1)
				count <= count + 1;
		end
	end

	assign dout  = count;
endmodule // counter
