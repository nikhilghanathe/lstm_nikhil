
`ifndef CUSTOMTYPES_SV_INCLUDED
`define CUSTOMTYPES_SV_INCLUDED

import ShellTypes::*;

//simulation parameters
localparam DEBUG =0;
localparam JOBSIZE = 4096;


localparam DATA_WIDTH 		= 	32;//SINGLE PRECISION
localparam B			 		=	8;//Batch Size
localparam H			 		=	1024;//hidden size
localparam D			 		=	1024;//feature dimension
localparam T			 		=	1;//timesteps

localparam TILE_SIZE			=	128;
localparam NUM_TILES			=	4*H/TILE_SIZE;
localparam TILE_PASS_COUNT =  (D+H)/TILE_SIZE;//number of times TILE has to move to complete one pass

localparam DRAM_BUS_WIDTH	=	512;
localparam NUM_PARALLEL		=	DRAM_BUS_WIDTH/DATA_WIDTH;


typedef struct packed {
	logic                       valid;
	logic [UMI_DATA_WIDTH-1:0] data;
	logic [PCIE_SLOT_WIDTH-1:0] slot;
	logic [PCIE_PAD_WIDTH-1:0]  pad;
	logic                       last;
} DramPacket;




`endif