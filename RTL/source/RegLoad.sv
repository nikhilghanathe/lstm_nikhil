/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    12/9/2017 
// Design Name:    reg_load
// Module Name:    reg_load 
// Project Name:   MATRIX_MUL
// Target Devices: Stratix V
// Tool versions: 15.1.1
// Description: Register with laod capability and CE
//
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

`include "CustomTypes.sv"
module RegLoad #(parameter WIDTH = 32) (rst, clk, din, LE, CE, LoadVal, dout);

input 							rst, clk, LE,CE;
input [WIDTH-1:0] 			din, LoadVal;
output logic [WIDTH-1:0] 	dout;

//signals
logic [WIDTH-1:0] 			MuxOut;



always@(posedge clk) begin
	if(rst==1'b1) begin
		dout		<= 0;
	end
	else begin
		if(LE==1'b1 | CE==1'b1)
			dout	<=	MuxOut;
	end
end

assign MuxOut = LE ? LoadVal : din;

endmodule