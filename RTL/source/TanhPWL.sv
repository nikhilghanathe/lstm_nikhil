`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	MSRI	
// Engineer: 	NP Ghanathe	
// 
// Create Date: 1/18/2017 
// Design Name: TanhPWL
// Module Name: TanhPWL
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 	Compute Tanh output using PWL method (Isosceles triangular Approximation)
// 
// Dependencies: All underlying modules
// 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
//							
//						PWL Implementation
//							tanh(x)=
//										x − 0.25 * sign(x) * sq(x),	0≤|x|≤2;
//										sign(x)						  ,	otherwise
// 
//////////////////////////////////////////////////////////////////////////////////
`include "CustomTypes.sv"

module TanhPWL #(parameter WIDTH=DATA_WIDTH)(clk,rst, x, SqX,CoeffMul1,CoeffMul2, CoeffAdd1, range);

input 							clk, rst;
input [WIDTH-1:0]				x,SqX;
output logic [WIDTH-1:0]	CoeffMul1,CoeffMul2, CoeffAdd1;
output logic 					range;


//Signals
logic [WIDTH-1:0]				MulOut,xAbs,xSq ,MulOp1, MulOp2;
logic 							signX,c0,c1,z0;


//sign bit
assign signX 	=	x[WIDTH-1];



//Abs value
assign xAbs =	{1'b0, x[WIDTH-2:0]};



//magnitude comaparator

	//y=1
	FPGreatEq u0 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h00000000),      // >=0     b.b
		.q      (c0)       //      q.q
	);
	
	FPLessEq u1 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40000000),      // <=2     b.b
		.q      (c1)       //      q.q
	);

	
	assign z0		=	c0	&	c1;
	assign range 	= 	z0;

	
//			x − 0.25 * sign(x) * sq(x),	0≤|x|≤2;    =>	0.25* sign(x) (x/0.25*sign(x) - sq(x))   => -0.25 * sign (sq(x) - x/0.25 * sign)
//			sign(x)						  ,	otherwise
	always@(posedge clk) begin
		if(z0==1'b1)begin
			CoeffMul1	<=	x;
			CoeffMul2	<=	x;
			CoeffAdd1	<=	{signX ^ 1'b1,x[30:25],x[24:23] + 2'b10,x[22:0]};
		end
		else begin
			CoeffMul1	<=	0;
			CoeffMul2	<=	0;
			CoeffAdd1	<=	{signX,31'h3f800000};
		end
	
	end




	
	
endmodule
