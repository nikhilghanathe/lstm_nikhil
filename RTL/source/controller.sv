`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.12.2017 11:59:52
// Design Name: 
// Module Name: Controller
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "CustomTypes.sv"


module controller(clk, rst, 
				//forward prop ports
				go, validOut, initLoadDone, matMulValid, fifoWrSig, fifoRdSig,fifoSelLoad, RdSel, WrSel,changeMode, load, 
					validIn, elemValid, BASE_ADDRA, BASE_ADDRB, CEdpth, fifoInSel, gateCompValid, gateMode, onePassFifoDone, 
					activationMode,stateCompValid, stateBuffSel,stateCompDone, forwardDone,done,
						
						//backprop ports
						go_b,  dnextCWrDone, cellAddrDone_b,  dnextCUpdateDone,computeDerivWrStart, derivWrDone,
						dnextCFill, dnextCUpdateSig, computeDerivativeValid, derivWen,
						aValsLoadDone
											);

input																clk, rst, go, initLoadDone, matMulValid, changeMode,onePassFifoDone, validOut, gateCompValid;
input 																stateCompDone, forwardDone;
output logic														load, validIn, elemValid, fifoInSel;
output logic [63:0]													BASE_ADDRA, BASE_ADDRB;
output logic [2:0]													activationMode;
output logic [1:0]													gateMode;
output logic														done, fifoWrSig, fifoRdSig, CEdpth[1:0], fifoSelLoad;
output logic [B-1:0]												RdSel, WrSel;
output logic 														stateCompValid,stateBuffSel ;


//backprop
input 																go_b,  dnextCWrDone, cellAddrDone_b,  dnextCUpdateDone,computeDerivWrStart, derivWrDone;																		
output logic 														dnextCFill, dnextCUpdateSig, computeDerivativeValid, derivWen;
input																	aValsLoadDone;


//signals

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//			state machine for matMul
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
localparam																ST_INIT_MMUL					=	3'd0,
																		ST_LOAD_MMUL					=	3'd1,
																		ST_WAIT_TILL_MULVALID		=	3'd2,
																		ST_COMPUTE_INITPSUM_MMUL	=	3'd3,
																		ST_DUMMYRD						=	3'd4,
																		ST_COMPUTE_PSUM_MMUL			=	3'd5,
																		ST_DUMMYWR						=	3'd6,
																		ST_ELEMVALID_MMUL				=	3'd7;
							
logic [2:0]														stateMMul, next_stateMMul;

//logic [$clog2(B*TILE_SZIE)-1:0]	initPsumCount;
logic [$clog2(TILE_PASS_COUNT)-1:0]								elemValidCount;
logic [$clog2(NUM_TILES+1)-1:0]									matMulCount;
logic															allPSumComputed, matMulDone;
logic															WrLoad, WrCE, WrClr,oneWrDone;
logic [$clog2(TILE_SIZE)-1:0]									WrCount,RdCount;
logic															RdLoad,RdCE, RdClr,oneRdDone, RdLoadDelay[6:0];
logic [B-1:0]													RdSel_ff,WrSel_ff;

integer i;

////////////////////shiftreg for fifos - READ////////////////////////////////////
always@(posedge clk)begin
	if(rst==1'b1)begin
		for(i=0;i<B;i++)begin
			RdSel_ff[i]	<=	1'b0;
		end
	end
	else begin
		if(RdLoad==1'b1)begin
			RdSel_ff[0]	<=	1'b1;//loadval
		end
		else begin
			if(oneRdDone==1'b1 && next_stateMMul==ST_COMPUTE_PSUM_MMUL)begin
				RdSel_ff[0]	<=	RdSel_ff[B-1];//circuar shift
			end
			else if(oneRdDone==1'b1)begin
				RdSel_ff[0]	<=	1'b0;
			end
		end
	
	//shift
		for(i=0;i<B-1;i++)begin
			if(oneRdDone==1'b1)begin
				RdSel_ff[i+1]	<=	RdSel_ff[i];	
			end
		end
	end
end

always@(posedge clk)begin
	if(rst==1'b1)begin
		RdCount	<=	0;
	end
	else begin
		if(oneRdDone==1'b1)begin
			RdCount	<=	0;
		end
		else if(RdCE==1'b1)begin
			RdCount	<=	RdCount + 1'b1;
		end
	end
end

assign oneRdDone	=	RdCount==TILE_SIZE-1;
/////////////////////////////////////////////////////////////////////////////////



////////////////////shiftreg for fifos - WRITE////////////////////////////////////
always@(posedge clk)begin
	if(rst==1'b1)begin
		for(i=0;i<B;i++)begin
			WrSel_ff[i]	<=	1'b0;
		end
	end
	else begin
		if(WrLoad==1'b1)begin
			WrSel_ff[0]	<=	1'b1;//loadval
		end
		else begin 
			if(oneWrDone==1'b1 && stateMMul==ST_COMPUTE_PSUM_MMUL)begin
				WrSel_ff[0]	<=	WrSel_ff[B-1];//circuar shift
			end
			else if(oneWrDone==1'b1)begin
				WrSel_ff[0]	<=	1'b0;
			end
		end
	
	//shift
		for(i=0;i<B-1;i++)begin
			if(oneWrDone==1'b1)begin
				WrSel_ff[i+1]	<=	WrSel_ff[i];	
			end
		end
	end
end

always@(posedge clk)begin
	if(rst==1'b1)begin
		WrCount	<=	0;
	end
	else begin
		if(oneWrDone==1'b1)begin
			WrCount	<=	0;
		end
		else if(WrCE==1'b1)begin
			WrCount	<=	WrCount + 1'b1;
		end
	end
end
assign oneWrDone	=	 WrCount==TILE_SIZE-1;
/////////////////////////////////////////////////////////////////////////////////


always@(posedge clk)begin
	RdLoadDelay[0]	<=	RdLoad;
	for(i=0;i<6;i++)begin
		RdLoadDelay[i+1]	<=	RdLoadDelay[i];
	end
end




////////////////////////////////////////////////////////////////////////////

//state asssign
always@(posedge clk)begin
	if(rst==1'b1)begin
		stateMMul		<=	ST_INIT_MMUL;
	end
	else begin
		stateMMul		<=	next_stateMMul;
	end
end



//state calc
always@*begin
	next_stateMMul			=	stateMMul;
	load						=	0;
	validIn					=	0;
	elemValid				=	0;
	fifoInSel				=	0;
//	CEdpth[0]				=	0;
//	CEdpth[1]				=	0;
	allPSumComputed		=	0;
	fifoWrSig				=	0;
	fifoRdSig				=	0;
	fifoSelLoad				=	0;
	WrLoad					=	0;
	WrCE						=	0;
	RdLoad					=	0;
	RdCE						=	0;
	
	case(stateMMul)
		ST_INIT_MMUL:begin
			if(go==1'b1)begin
				next_stateMMul	=	ST_LOAD_MMUL;
				load				=	1;
			end
		end
		
		ST_LOAD_MMUL:begin//wait till buffers are loaded
			if(initLoadDone==1'b1)begin
				validIn			=	1;
				next_stateMMul	=	ST_WAIT_TILL_MULVALID;		
			end
		end
		
		ST_WAIT_TILL_MULVALID:begin
			validIn			=	1;
			if(validOut	==1'b1)begin
				WrLoad			=	1;
				next_stateMMul	=	ST_COMPUTE_INITPSUM_MMUL;
			end
		end
		
		ST_COMPUTE_INITPSUM_MMUL:begin//compute initial partial-sum (PSUM) 
			validIn			=	1;
			fifoWrSig		=	1;
			fifoRdSig		=	0;
			WrCE				=	1;
//			CEdpth[0]		=	1;
//			CEdpth[1]		=	1;
			if(oneWrDone==1'b1 && WrSel_ff[B-1]==1'b1)begin
				next_stateMMul	= 	ST_DUMMYRD;
				//fifoRdSig		=	1;
				RdLoad			=	1;
			end
		end
		
		ST_DUMMYRD:begin
			fifoRdSig		=	1;
			RdCE				=	1;
			validIn			=	1;
			if(RdLoadDelay[5]==1'b1)begin//wait one clk cycle for loadding and 5 clock cycle for preadd delay
				next_stateMMul	=	ST_COMPUTE_PSUM_MMUL;
				WrLoad			=	1;
			end
			
		end
		
		ST_COMPUTE_PSUM_MMUL:begin//supply valid signal to valid chain in datapath
			validIn			=	1;
			fifoInSel		=	1;
			fifoWrSig		=	1;
			fifoRdSig		=	1;
			RdCE				=	1;
			WrCE				=	1;
			if(elemValidCount == (TILE_PASS_COUNT-2) && RdSel_ff[B-1]==1'b1 && oneRdDone==1'b1)begin//when you are set for the last pass
				next_stateMMul	=	ST_DUMMYWR;
			//	fifoWrSig		=	0;
				fifoRdSig		=	0;
			end
		end
		
		ST_DUMMYWR:begin
			fifoWrSig		=	1;
			WrCE				=	1;
			//RdCE				=	1;
			validIn			=	1;
			fifoInSel		=	1;
//			elemValid		=	1;
			if(oneWrDone==1'b1)begin
				next_stateMMul	=	ST_ELEMVALID_MMUL;
				RdLoad			=	1;
				WrCE				=	0;
			end
		end
		
		ST_ELEMVALID_MMUL:begin//all partial sums are computed and the elements are now valid
			elemValid		=	1;
			validIn			=	1;
			fifoWrSig		=	0;
			fifoRdSig		=	1;
			RdCE				=	1;
			if(matMulDone==1'b1)begin//all elements computed
				next_stateMMul	=	ST_INIT_MMUL;
				//validIn			=	0;
				//elemValid		=	1;
			end
			else begin
				if(oneRdDone==1'b1 && RdSel_ff[B-1]==1'b1)begin
					next_stateMMul	=	ST_COMPUTE_INITPSUM_MMUL;
					allPSumComputed= 	1;
					WrLoad			=	1;
					//fifoRdSig		=	0;
				end
			end
		end
	endcase
	
end


////////////////



//counters for counting elements that are computed
always@(posedge clk)begin
	if(rst==1'b1)begin
		elemValidCount	<=	0;
	end
	else begin
		if(allPSumComputed==1'b1)begin
			elemValidCount	<=	0;
		end
		else if(oneWrDone==1'b1 && WrSel_ff[B-1]==1'b1)begin
			elemValidCount <=	elemValidCount	+ 1'b1;
		end
	end
end

//counter to check whether all elements of matrix multiplication are done
always@(posedge clk)begin
	if(rst==1'b1)begin
		matMulCount	<=	0;
	end
	else begin
		if(matMulDone==1'b1)begin
			matMulCount	<=	0;
		end
		else if(allPSumComputed==1'b1)begin
			matMulCount <=	matMulCount	+ 1'b1;
		end
	end
end


//assign allPSumComputed	=	elemValidCount==TILE_PASS_COUNT 				? 	1'b1 	:	1'b0;
assign matMulDone			=	matMulCount==(NUM_TILES-1) && oneRdDone==1'b1 && RdSel_ff[B-1]==1'b1 && elemValid==1'b1 ?	1'b1	: 	1'b0;
assign WrSel				=	WrSel_ff;
assign RdSel				=	RdSel_ff;

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	state machine for elem-wise computations
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

localparam															ST_INIT_ELEM				=	3'd0,
																		ST_MODE_i					=	3'd1,
																		ST_MODE_f					=	3'd2,
																		ST_MODE_o					=	3'd3,
																		ST_MODE_g					=	3'd4,
																		ST_COMPUTE_CELL_HIDDEN	=	3'd5,
																		ST_WAIT_FOR_DONE			=	3'd6;
																		
logic	[2:0]															stateElem, next_stateElem;
logic [$clog2(NUM_TILES*TILE_PASS_COUNT)-1:0]			modeCount;
logic																	modeValid_i,modeValid_f,modeValid_o,modeValid_g,stateBuffSel_nxt;

//state assign
always@(posedge clk)begin
	if(rst==1'b1)begin
		stateElem		<=	ST_INIT_ELEM;
		stateBuffSel	<=	0;
	end
	else begin
		stateElem		<=	next_stateElem;
		stateBuffSel	<=	stateBuffSel_nxt;
	end
end									


//state calc
always@* begin
	gateMode				=	0;
	activationMode		=	0;
	next_stateElem		=	stateElem;
	done					=	0;
	stateCompValid		=	0;
	stateBuffSel_nxt	=	stateBuffSel;
	case(stateElem)
		ST_INIT_ELEM:begin
			if(elemValid==1'b1)begin
				next_stateElem			=	ST_MODE_i;
				gateMode					=	2'b00;
				activationMode			=	3'b011;//cgange
			end
		end
		
		ST_MODE_i:begin//compute sigmoid in mode 'i'; just stores value in BRAM
			gateMode					=	2'b00;
			activationMode			=	3'b011;
			if(!modeValid_i)begin
				next_stateElem			=	ST_MODE_f;
				gateMode					=	2'b01;
				activationMode			=	3'b011;
			end
		end
		
		ST_MODE_f:begin//compute sigmoid and store value in BRAM
			gateMode					=	2'b01;
			activationMode			=	3'b011;
			if(!modeValid_f)begin
				next_stateElem			=	ST_MODE_o;
				gateMode					=	2'b10;
				activationMode			=	3'b011;
			end
		end
		
		ST_MODE_o:begin//compute sigmoid and store value in BRAM
			gateMode					=	2'b10;
			activationMode			=	3'b011;
			if(!modeValid_o)begin
				next_stateElem			=	ST_MODE_g;
				gateMode					=	2'b11;
				activationMode			=	3'b100;
			end
		end
		
		ST_MODE_g:begin//compute tanh and compute next_c and next_h i.e. cell state and hidden state
			gateMode					=	2'b11;
			activationMode			=	3'b100;
			if(!modeValid_g)begin
				next_stateElem			=	ST_COMPUTE_CELL_HIDDEN;
				//done						=	1;
				gateMode					=	0;
				stateCompValid			=	1;
			end
		end
		
		ST_COMPUTE_CELL_HIDDEN:begin
			stateCompValid		=	1;
			if(stateCompDone==1'b1)begin
				next_stateElem		=	ST_WAIT_FOR_DONE;
			end
		end
		
		ST_WAIT_FOR_DONE:begin
			if(forwardDone==1'b1)begin
				next_stateElem		=	ST_INIT_ELEM;
				done 					=	1;
			end	
		end	
		
	endcase
	
end

//flags indicating which mode to operate on - op mode 'i'/'f'/'o'/'g'
assign modeValid_i	=	(matMulCount>= 0 * (NUM_TILES) / 4) && (matMulCount < 1 * (NUM_TILES) / 4);  
assign modeValid_f	=	(matMulCount>= 1 * (NUM_TILES) / 4) && (matMulCount < 2 * (NUM_TILES) / 4); 
assign modeValid_o	=	(matMulCount>= 2 * (NUM_TILES) / 4) && (matMulCount < 3 * (NUM_TILES) / 4); 
assign modeValid_g	=	(matMulCount>= 3 * (NUM_TILES) / 4) && (matMulCount < 4 * (NUM_TILES) / 4); 




/////////////////////////////////////////////////////////FORWARD PASS ENDS HERE//////////////////////////////////////////////////////////////////////////////////////////////






/////////////////////////////////////////////////////////BACKPROP STARTS HERE//////////////////////////////////////////////////////////////////////////////////////////////
localparam 															ST_INIT_ELEM_b				=	3'd0,
																		ST_FILL_DNEXTC				=	3'd1,
																		ST_UPDATE_DNEXTC			=	3'd2,
																		ST_WAIT_DNEXTC_UPDATE	=	3'd3,
																		ST_COMPUTE_DERIV			=	3'd4,
																		ST_WRITE_DERIV				=	3'd5,
																		ST_WAIT_WRITE_DERIV		=	3'd6,
																		ST_LOAD_AVALS				=	3'd7;
													
logic [2:0]																stateElem_b, next_stateElem_b;



//state assign
always@(posedge clk)begin
	if(rst==1'b1)begin
		stateElem_b	<=	ST_INIT_ELEM_b;
	end
	else begin
		stateElem_b		<=	next_stateElem_b;
	end
end
																		

//state calc
always@*begin
	next_stateElem_b			=	stateElem_b;
	dnextCFill					=	0;
	dnextCUpdateSig			=	0;
	computeDerivativeValid	=	0;
	derivWen						=	0;
	
	case(stateElem_b)
		ST_INIT_ELEM_b:begin
			if(go_b==1'b1)begin
				next_stateElem_b		=	ST_FILL_DNEXTC;
			end
		end
		
		ST_FILL_DNEXTC:begin
			dnextCFill		=	1;
			if(dnextCWrDone==1'b1)begin
				next_stateElem_b		=	ST_UPDATE_DNEXTC;
			end
		end
		//fill dnextC end


		//update dnextC start
		ST_UPDATE_DNEXTC:begin
			dnextCUpdateSig	=	1;
			if(cellAddrDone_b==1'b1)begin
				next_stateElem_b		=	ST_WAIT_DNEXTC_UPDATE;
			end
		end
		
		ST_WAIT_DNEXTC_UPDATE:begin
			if(dnextCUpdateDone==1'b1)begin
				next_stateElem_b		=	ST_COMPUTE_DERIV;
			end
		end
		//update dnextC start
		//backprop step5 end



		//backprop step 4 start -  compute df,dprev,di,dg
		ST_COMPUTE_DERIV:begin
			computeDerivativeValid	=	1;
			if(computeDerivWrStart==1'b1)begin
				next_stateElem_b		=	ST_WRITE_DERIV;
			end
		end
		
		ST_WRITE_DERIV:begin
			if(derivWrDone==1'b1)begin
				next_stateElem_b		=	ST_LOAD_AVALS;
			end
		end
		
		
		ST_LOAD_AVALS:begin
			if(aValsLoadDone==1'b1)begin
				next_stateElem_b		=	ST_INIT_ELEM_b;
			end
		end
		
	endcase
end




/////////////////////////////////////////////////////////BACKPROP ENDS HERE//////////////////////////////////////////////////////////////////////////////////////////////



endmodule