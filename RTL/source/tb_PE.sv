`timescale 1ns/1ps

`include "CustomTypes.sv"

module tb_PE;

parameter WIDTH = DATA_WIDTH;

logic 								clk,rst,CE;
logic [WIDTH-1:0]					MulIn1, MulIn2, AddIn1, AddIn2;
logic [2:0]							mode;
 logic [WIDTH-1:0]		MulResult, AddResult, SigResult, TanhResult, MacResult;

	
	PE UUT (
			.clk(clk),
			.rst(rst),
			.CE(CE),
			.MulIn1(MulIn1),
			.MulIn2(MulIn2),
			.AddIn1(AddIn1),
			.AddIn2(AddIn2),
			.mode(mode),
			.MulResult(MulResult),
			.AddResult(AddResult),
			.TanhResult(TanhResult),
			.MacResult(MacResult)
	
	);
	
	 
	
	initial begin
		clk=0;
		rst=1;
		mode=0;
		AddIn1=0;
		AddIn2=0;
		MulIn1=0;
		MulIn2=0;
		CE=0;
		
		#100;
		@(posedge clk);
		rst=0;
		CE=1'b1;
		mode=0;
		AddIn1=0;
		AddIn2=0;
		MulIn1=0;
		MulIn2=0;
		
		
		mode=3'b001;
		AddIn1=32'h40400000;
		AddIn2=32'h3f800000;
		MulIn1=32'h3f800000;
		MulIn2=32'h3f800000;
		
		@(posedge clk);
		AddIn2=32'h40000000;
		@(posedge clk);
		AddIn2=32'h40400000;
		#100;
		@(posedge clk);
		mode=3'b010;
		AddIn1=32'h40400000;
		AddIn2=32'h3f800000;
		MulIn1=32'h3f800000;
		MulIn2=32'h40000000;
		
		#100;
		@(posedge clk);
		mode=3'b011;
		AddIn1=32'h40400000;
		AddIn2=32'h3f800000;
		MulIn1=32'h3f800000;
		MulIn2=32'h40000000;
		@(posedge clk);
		MulIn2=32'h40400000;
		@(posedge clk);
		MulIn2=32'h40000000;
		@(posedge clk);
		MulIn2=32'h40400000;
		@(posedge clk);
		MulIn1=32'h00000000;
		MulIn2=32'h00000000;
		
		#100;
		@(posedge clk);
		mode=3'b100;
		AddIn1=32'h40000000;
		AddIn2=32'h3f800000;
		MulIn1=32'h3f800000;
		MulIn2=32'h04040000;
		
		
		#100;
		@(posedge clk);
		mode=3'b101;
		AddIn1=32'h40000000;
		AddIn2=32'h3f800000;
		MulIn1=32'h3f800000;
		MulIn2=32'h3f800000;
		
		
		
		
		
		
		
		
	
	
	end
	
	always begin
		#5  clk	=	1;
		#5  clk	=	0;
	end
	
endmodule