`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	MSRI	
// Engineer: 	NP Ghanathe	
// 
// Create Date: 24/4/2018 
// Design Name: syncRegs
// Module Name: syncRegs
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 	paramterised register chain for syncing or creating valid chain
// 
// Dependencies: All underlying modules
// 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
/////////////////////////////////////////////////////////////////////////////////

module syncRegs #(parameter WIDTH=1, parameter SIZE=9) (clk, din, dout, shift);

input 								clk;
input [WIDTH-1:0]					din;
output logic [WIDTH-1:0]		dout;
//shift array
output logic [WIDTH-1:0]		shift[SIZE-1:0];

always@(posedge clk)begin
	shift[0]		<=	din;
	for(int i=0;i<SIZE-1;i++)begin
		shift[i+1]	<=	shift[i];
	end
end
assign dout	=	shift[SIZE-1];


endmodule

