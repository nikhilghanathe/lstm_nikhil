`timescale 1ns/1ps



module tb_test;


parameter WIDTH = 32;
logic 							clk, rst;
logic [WIDTH-1:0]				x;
 logic [WIDTH-1:0]			y;


test UUT(
	.clk(clk),
	.rst(rst),
	.x(x),
	.y(y)
 );

 
 
 
 initial begin
 clk 	= 	0;
 rst 	= 	1;
 x		=	0;
 
 #1000 ;
 
 @(posedge clk);
 rst=0;
 @(posedge clk);
 x = 32'h3f800000;
 @(posedge clk);
 x = 32'h40000000;
  @(posedge clk);
 x = 32'h3f800000;
 end
 
 always begin
	#5 clk=1;
	#5 clk=0;
 end


endmodule