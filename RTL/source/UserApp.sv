`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	MSRI	
// Engineer: 	NP Ghanathe	
// 
// Create Date: 1/18/2017 
// Design Name: UserApp
// Module Name: UserApp
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 	top-level module of user's application
// 
// Dependencies: All underlying modules
// 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
//							
//						
// 
//////////////////////////////////////////////////////////////////////////////////
`include "CustomTypes.sv"
module UserApp #(parameter WIDTH=DATA_WIDTH)
					(clk, rst, go,go_b, DramCh0, DramCh1, result,done);

input 													clk,go, go_b, rst;
input [DRAM_BUS_WIDTH-1:0]							DramCh1, DramCh0;
output logic [31:0]									result;
output logic 											done;


//signals

 logic 													doneB, loadB;
 logic [$clog2(TILE_SIZE/NUM_PARALLEL)-1:0] 	addrB;
 logic [2*TILE_SIZE-1:0] 							colSelB;
 logic [$clog2(TILE_SIZE/NUM_PARALLEL)-1:0]  addrA;//divide by NUM_PARALLEL because we read "NUM_PARALLEL" 32-bit inputs at a time
 logic [1:0]											BufferLoadSigA;
 logic [1:0]											bufferSelA, bufferSelB;
 
 logic													initLoadDone, matMulValid, changeMode,onePassFifoDone, computeStart;
 logic													load, validIn, elemValid, gateCompValid, fifoInSel;
 logic [63:0]											BASE_ADDRA, BASE_ADDRB;
 logic [2:0]											activationMode;
 logic [1:0]											gateMode;
 logic													CEdpth[1:0];
 logic													changeTile, computeDone, validOut, fifoWrSig, fifoRdSig, fifoSelLoad;
 logic [B-1:0]											RdSel, WrSel;
 logic													stateCompValid, stateCompDone, forwardDone, stateBuffSel;
 
 //backprop signals
 logic 													go_b,  dnextCWrDone, cellAddrDone_b,  dnextCUpdateDone,computeDerivWrStart, derivWrDone;																		
 logic 													dnextCFill, dnextCUpdateSig, computeDerivativeValid, derivWen;


//addr gen for inout features - X
AddrGenA U_ADDR_GENA (
		.clk(clk),
		.rst(rst),
		.load(load),
		.initLoadDone(initLoadDone),
		.BufferLoadSigA(BufferLoadSigA),
		.BufferSelA(bufferSelA),
		.BASE_ADDR(64'h0),//test purposes
		.addrA(addrA)
		);


		
//addr gen for model stored in DRAM
AddrGenB U_ADDR_GENB (
		.clk(clk),
		.rst(rst),
		.load(load),
		.CEdpth(CEdpth),
		.BASE_ADDR(64'h0),//for test purposes
		.done(doneB),
		.loadB(loadB),
		.InitLoadDone(initLoadDone),
		.AddrB(addrB),
		.ColSelB(colSelB),
		.BufferSelB(bufferSelB)
		);
		
		




		
//datapath
datapath dpth (
		.clk(clk),
		.rst(rst),
		.CE(CEdpth),
		.BufferSelA(bufferSelA),
		.BufferSelB(bufferSelB),
		.ValidIn(validIn),
		.BufferLoadSigA(BufferLoadSigA),
		.stateBuffSel(stateBuffSel),
		.DramCh0(DramCh0),
		.DramCh1(DramCh1),
		.load(load),
		.fifoWrSig(fifoWrSig),
		.fifoRdSig(fifoRdSig),
		.fifoSelLoad(fifoSelLoad),
		.RdSel(RdSel),
		.WrSel(WrSel),
		.ColSelB(colSelB),
		.ComputeStart(computeStart),
		.ChangeTile(changeTile),
		.ComputeDone(computeDone),
		.ValidOut(validOut),
		.onePassFifoDone(onePassFifoDone),
		.fifoInSel(fifoInSel),
		.matMulValid(matMulValid),
		.changeMode(changeMode),
		.gateMode(gateMode),
		.gateCompValid(gateCompValid),
		.elemValid(elemValid),
		.activationMode(activationMode),
		.stateCompValid(stateCompValid),
		.stateCompDone(stateCompDone),
		.forwardDone(forwardDone),
		.Result(result),
		
		//backprop
		.dnextCWrDone(dnextCWrDone),
		.cellAddrDone_b(cellAddrDone_b),
		.dnextCUpdateDone(dnextCUpdateDone),
		.computeDerivWrStart(computeDerivWrStart),
		.derivWrDone(derivWrDone),
		.dnextCFill(dnextCFill),
		.dnextCUpdateSig(dnextCUpdateSig),
		.computeDerivativeValid(computeDerivativeValid),
		//.derivWen(derivWen)
		
		);
			


		
						
//controller
controller cntrl(
		.clk(clk),
		.rst(rst),
		.go(go),
		//.CEdpth(CEdpth),
		.initLoadDone(initLoadDone),
		.matMulValid(matMulValid),
		.changeMode(changeMode),
		.onePassFifoDone(onePassFifoDone),
		.load(load),
		.validIn(validIn),
		.fifoWrSig(fifoWrSig),
		.fifoRdSig(fifoRdSig),
		.fifoSelLoad(fifoSelLoad),
		.RdSel(RdSel),
		.WrSel(WrSel),
		.validOut(validOut),
		.elemValid(elemValid),
		.stateBuffSel(stateBuffSel),
		.gateCompValid(gateCompValid),
		.fifoInSel(fifoInSel),
		.activationMode(activationMode),
		.gateMode(gateMode),
		.forwardDone(forwardDone),
		.stateCompValid(stateCompValid),
		.stateCompDone(stateCompDone),
		.done(done),
		
		//backprop
		.go_b(go_b),
		.dnextCWrDone(dnextCWrDone),
		.cellAddrDone_b(cellAddrDone_b),
		.dnextCUpdateDone(dnextCUpdateDone),
		.computeDerivWrStart(computeDerivWrStart),
		.derivWrDone(derivWrDone),
		.dnextCFill(dnextCFill),
		.dnextCUpdateSig(dnextCUpdateSig),
		.computeDerivativeValid(computeDerivativeValid)
		//.derivWen(derivWen)
		);

						
endmodule