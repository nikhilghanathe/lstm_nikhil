/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    12/9/2017 
// Design Name:    AddrGenB
// Module Name:    AddrGenB
// Project Name:   LSTM
// Target Devices: Stratix V
// Tool versions: 15.1.1
// Description: Addr generator for matrix B for pipelined design (data type - Int32)
//
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps


`include "CustomTypes.sv"

module AddrGenB #(parameter WIDTH=32, parameter SIZE=32*32) (rst,clk, done, load,loadB,BufferSelB, InitLoadDone, AddrB, ColSelB, BASE_ADDR, CEdpth);

input 														clk, rst;
input 														load;//start state machine
input [63:0]												BASE_ADDR;
output logic 												done, loadB, InitLoadDone;
output logic [64-1:0] 									AddrB;
output logic [2*TILE_SIZE-1:0] 						ColSelB;
output logic 												BufferSelB, CEdpth[1:0];


//signals
	//reg for addr counter
logic [63:0] 												addrB_ff;
logic [$clog2(TILE_SIZE/NUM_PARALLEL)-1:0]		count;
logic [$clog2(TILE_SIZE)-1:0] 						LoadCount;
logic [$clog2(B * TILE_SIZE) -1:0]					ComputeCount;
logic 														OneColDone, ComputeDone, LoadDone;
logic [2*TILE_SIZE-1:0] 								ColSelB_ff;
logic 														SelLoad[1:0];
			
	//control signals
logic 														CE, computeCE;
logic [1:0]													ColSelSigB, ColSelSigB_nxt;
logic 														loadDone0, loadDone1;

integer 														i;
genvar 														gi;

//MUX for loading 1 in initial stage
assign SelLoad[0] 	= ColSelSigB[0];
assign SelLoad[1] 	= ColSelSigB[1];
assign ColSelB 		= ColSelB_ff;




///////////////////////////////////////////////////////////////////////

//SHIFT reg for colB select lines - Buffer1
generate 
begin: ColSelB_SHIFT_REG_BLOCK_1
		for(gi=0;gi<TILE_SIZE;gi++)begin	:ColSelB_1
		RegLoad #(.WIDTH(1'b1)) U_COL_SEL_REG
						(.rst(rst),
						 .clk(clk),
						 .LE(1'b0),
						 .CE(ColSelSigB[0] | (OneColDone)),
						 .LoadVal(1'b0),
						 .din((gi==0) ? SelLoad[0] : ColSelB_ff[gi-1]),
						 .dout(ColSelB_ff[gi])
						);
		end
end 
endgenerate



//SHIFT reg for colB select lines - Buffer2
generate 
begin: ColSelB_SHIFT_REG_BLOCK_2
		for(gi=TILE_SIZE;gi<2*TILE_SIZE;gi++)begin	:ColSelB_2
		RegLoad #(.WIDTH(1'b1)) U_COL_SEL_REG
						(.rst(rst),
						 .clk(clk),
						 .LE(1'b0),
						 .CE(ColSelSigB[1] | (OneColDone)),
						 .LoadVal(1'b0),
						 .din((gi==TILE_SIZE) ? SelLoad[1] : ColSelB_ff[gi-1]),
						 .dout(ColSelB_ff[gi])
						);
		end
end 
endgenerate


always@(posedge clk)begin
	loadDone0	<=	ColSelB_ff[TILE_SIZE-1];
	loadDone1	<=	ColSelB_ff[(2*TILE_SIZE)-1];
end
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								Counters for generating flags indicating the end of load and computes
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
always@(posedge clk ) begin
		if(rst==1'b1) begin
			count <=0;  
		end
		else begin
			if(OneColDone==1'b1)
				count<=0;
			else if(CE==1'b1)
				count<= count + 1'b1; 
		end
end

assign OneColDone = (count==(TILE_SIZE/NUM_PARALLEL)-1) ? 1'b1 : 1'b0; //generate flag when equal to ceil(TILE_SIZE/NUM_PARALLEL)



////increment counter after every row read - load counter
//always@(posedge clk) begin
//	if(rst==1'b1)begin
//		LoadCount<=0;
//	end
//	else begin
//		if(LoadDone==1'b1) begin
//			LoadCount <= 0;
//		end
//		if(OneColDone==1'b1)begin
//			LoadCount <= LoadCount + 1'b1;
//		end
//	end
//end
//
//assign LoadDone = (LoadCount==TILE_SIZE-1) ? 1'b1 : 1'b0;




//counter to check if all computations are done
always@(posedge clk) begin
	if(rst==1'b1)begin
		ComputeCount	<=0;
	end
	else begin
		if(ComputeDone==1'b1) begin
			ComputeCount <= 0;
		end
		if(computeCE==1'b1)begin
			ComputeCount <= ComputeCount + 1'b1;
		end
	end
end

assign ComputeDone = (ComputeCount==(B * TILE_SIZE)-1) ? 1'b1 : 1'b0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////








/////////////////////////////////////////////////////////////////////////////////////////////
//										State Machine
////////////////////////////////////////////////////////////////////////////////////////////////
localparam													ST_INIT 									=	3'd0,
																ST_DUMMY_WAIT							=	3'd1,
																ST_LOAD_BUFF1_INIT					=	3'd2,
																ST_LOAD_BUFF1							=	3'd3,
																ST_WAIT_BUFF1							=	3'd4,
																ST_LOAD_BUFF2							=	3'd5,
																ST_WAIT_BUFF2							=	3'd6;
																
logic	[2:0]													state, next_state;


//state assign
always@(posedge clk)begin
	if(rst==1'b1)begin
		state			<=	ST_INIT;
	end
	else begin
		state			<=	next_state;	
	end
end


//state calc
always@ * begin
	CE					=	0;
	computeCE		=	0;
	loadB				=	0;
	next_state		=	state;
	ColSelSigB		=	0;
	BufferSelB		=	0;
	InitLoadDone	=	0;
	CEdpth[0]		=	0;
	CEdpth[1]		=	0;
	case(state)
		ST_INIT:begin
			if(load==1'b1)begin
				next_state		=	ST_LOAD_BUFF1_INIT;
				ColSelSigB		=	2'b01;
				loadB				=	1;
			end
		end
		
		ST_DUMMY_WAIT:begin//wait one clk cycle for ColSelB_ff to show up and then start counter
			ColSelSigB			=	0;
			next_state			=	ST_LOAD_BUFF1_INIT;
		end
		
		//Fill both buffers initially
		ST_LOAD_BUFF1_INIT: begin
			CE					=	1;
			ColSelSigB		=	2'b00;
			if(loadDone0==1'b1)begin
				CE					=	0;
				CEdpth[0]		=	1;
				CEdpth[1]		=	0;
				ColSelSigB		=	2'b10;
				loadB				=	1;
				next_state		=	ST_LOAD_BUFF2;
				InitLoadDone	=	1;
			end
		end
		
		//load buffer 1 and wait till compute is done
		ST_LOAD_BUFF1: begin
			CE					=	1;
			computeCE		=	1;
			ColSelSigB		=	2'b00;
			BufferSelB		=	1;
			CEdpth[0]		=	0;
			CEdpth[1]		=	1;
			if(loadDone0==1'b1)begin
				CE					=	0;
				computeCE		=	0;
				loadB				=	1;
				next_state		=	ST_WAIT_BUFF1;
			end
		end	
		
		ST_WAIT_BUFF1:begin
			computeCE		=	1;
			BufferSelB		=	1;
			ColSelSigB		=	2'b00;
			CEdpth[0]		=	0;
			CEdpth[1]		=	1;
			if(ComputeDone==1'b1)begin
				next_state		=	ST_LOAD_BUFF2;
				computeCE		=	0;
				BufferSelB		=	0;
				CEdpth[0]		=	1;
				CEdpth[1]		=	0;
				ColSelSigB		=	2'b10;
			end
		end
		
		//load buffer 2 and wait till compute is done
		ST_LOAD_BUFF2: begin
			CE 				=	1;
			computeCE		=	1;
			ColSelSigB		=	2'b00;
			BufferSelB		=	0;
			CEdpth[0]		=	1;
			CEdpth[1]		=	0;
			if(loadDone1==1'b1)begin
				next_state		=	ST_WAIT_BUFF2;
				CE					=	0;
				loadB				=	1;
			end	
			
		end
		
		ST_WAIT_BUFF2:begin
			BufferSelB		=	0;
			ColSelSigB		=	2'b00;
			computeCE		=	1;
			CEdpth[0]		=	1;
			CEdpth[1]		=	0;
			if(ComputeDone==1'b1)begin
				next_state		=	ST_LOAD_BUFF1;
				computeCE		=	0;
				BufferSelB		=	1;
				CEdpth[0]		=	0;
				CEdpth[1]		=	1;
				ColSelSigB		=	2'b01;
			end
		end
		
		
	endcase	
end




//reg for holding address
always@(posedge clk) begin
	if(rst==1'b1)begin
		addrB_ff	<= BASE_ADDR;//!!!!!!!!!!!!!!!!!!!!!!!!UNSURE OF THIS LOGIC
	end
	else begin
		if(CE==1'b1) 
			addrB_ff <= addrB_ff + 12'd64;
	end
end

assign AddrB = addrB_ff;

assign done = ComputeDone;

endmodule