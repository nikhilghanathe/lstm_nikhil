`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.12.2017 11:59:52
// Design Name: 
// Module Name: RAM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RAM #(parameter WIDTH = 32, parameter SIZE = 16)(clk, wdata, waddr, raddr, wen, rdata); 

localparam ADDR_WIDTH = $clog2(SIZE);

input                       clk, wen;
input [WIDTH-1:0]           wdata;
input [ADDR_WIDTH-1:0]      waddr, raddr;
output logic [WIDTH-1:0]    rdata;

logic [WIDTH-1:0]   memory [SIZE-1:0];


initial begin
	$readmemh("C:/Users/v-nighan/ProjectsPratap/LSTM/src/Hw/core/source/cellInit.lut", memory);
end

always@(posedge clk)begin
    if(wen==1'b1)begin
        memory[waddr]   <=  wdata;
     end
        
end

always@(posedge clk)begin
    rdata   <=  memory[raddr];
end

endmodule