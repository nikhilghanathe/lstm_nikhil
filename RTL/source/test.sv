`timescale 1ns/1ps
`include "CustomTypes.sv"
module test#(parameter WIDTH=DATA_WIDTH)(clk,rst,x,y);


input 							clk, rst;
input [WIDTH-1:0]				x;
output logic [WIDTH-1:0]	y;




		FpMult uMul (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (x),      //      a.a
		.b      (x),      //      b.b
		.q      (y )       //      q.q
		);


endmodule