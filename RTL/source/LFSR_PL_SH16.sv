/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    28/9/2017 
// Design Name:    LFSR_PL_SH16
// Module Name:    LFSR_PL_SH16 
// Project Name:   MATRIX_MUL
// Target Devices: Stratix V
// Tool versions: 15.1.1
// Description: Linear feedback shift reg with parallel load capability and load en/Clock en.
//						***********************************************************
//						*****THIS SHIFTS THE VALUES 16 TIMES on every clk cycle*****
//						***********************************************************
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

`include "CustomTypes.sv"
module LFSR_PL_SH16 	#(parameter WIDTH=1, 
					  parameter SIZE=512,//size of LFSR 
					  parameter SHIFT_SIZE = 16)//shift values SHIFT_SIZE times on each cycle 
					 (rst,clk, CE,LE, din, dout);

input 						rst,clk;
input							CE,LE;
input  [WIDTH-1:0]  		din [SIZE-1:0];
output [WIDTH-1:0] 		dout[SIZE-1:0];


//signals
logic [WIDTH-1:0] 		ShiftArray [SIZE-1:0]; 


genvar gi,gj;
generate
begin: SHIFT_BLOCK
	for(gi=0;gi<SIZE;gi++)begin:SHIFT_i
		reg_load #(.WIDTH(WIDTH)) U_SHIFT16
						(.rst(rst),
						 .clk(clk),
						 .LE(LE),
						 .CE(CE),
						 .load_val(din[gi]),
						 .din(ShiftArray[(gi+SHIFT_SIZE) % SIZE]),
						 .dout(ShiftArray[gi])
						);
	end
end	
endgenerate

	





assign dout = ShiftArray;
endmodule