`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	MSRI	
// Engineer: 	NP Ghanathe	
// 
// Create Date: 1/18/2017 
// Design Name: dSigmoidPLAN5
// Module Name: dSigmoidPLAN5
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 	Compute first order derivative of sigmoid function using Piecewise linear approximation technique using 5 segments (PLAN-5)
// 
// Dependencies: All underlying modules
// 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
//							
//						PLAN Implementation															Encoding					
//							dY= 0 										|	|x| >= 5 				000001															
//							dY= 0.2383 									|	-1 < X< 1				000010			
//							dY= sign * 0.0934* |X| + 0.0.1239 			|	1 =<|X|< 2 				000100
//							dY= sign * 0.0596* |X| + 0.2202 			|	2 =<|X|< 3 				001000
//							dY= sign * 0.027*  |X| + 0.1239 			|	3 =<|X|< 4 				010000
//							dY= sign * 0.0108* |X| + 0.0597				|	4 =<|X|< 5				100000
// 
//////////////////////////////////////////////////////////////////////////////////
`include "CustomTypes.sv"

module dSigmoidPLAN5 #(parameter WIDTH= DATA_WIDTH)( clk, rst,x, CoeffMul1,CoeffMul2, CoeffAdd1);

input 							clk,rst;
input [WIDTH-1:0]				x;
output logic [WIDTH-1:0]	CoeffMul1, CoeffMul2, CoeffAdd1;


//Signals
logic [WIDTH-1:0]				MulOut,xAbs, CoeffMul1_net, CoeffMul2_net, CoeffAdd1_net;
logic 							sign,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,z0,z1,z2,z3,z4,z5;
logic [WIDTH-1:0]				y_net1, y_net2;


//sign bit
assign sign 	=	x[WIDTH-1];


//Abs value
assign xAbs 	=	{1'b0, x[WIDTH-2:0]};



//magnitude comaparator

	//dy=0
	FPGreatEq u0 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40a00000),      // >=5     b.b
		.q      (z0)       //      q.q
	);

	
	//dY = 0.2383
	
		FPGreat u1 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (x),      //      a.a
		.b      (32'hbf800000),      // >-1     b.b
		.q      (c0)       //      q.q
		);
		
		
		FPLess u2 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (x),      //      a.a
		.b      (32'h3f800000),      //  <1    b.b
		.q      (c1)       //      q.q
	);

assign z1	=	c0 & c1;
	
	
	//dY= sign * 0.0934* |X| + 0.0.1239
		FPGreatEq u3 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h3f800000),      // >=1     b.b
		.q      (c2)       //      q.q
		);
		
		
		FPLess u4 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40000000),      //  <2    b.b
		.q      (c3)       //      q.q
	);
	
	assign z2	=	c2 & c3;
	
	
	//dY= sign * 0.0596* |X| + 0.2202
		FPGreatEq u5 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40000000),      // >=2     b.b
		.q      (c4)       //      q.q
		);
		
		
		FPLess u6 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40400000),      //  <3    b.b
		.q      (c5)       //      q.q
	);
	
	assign z3	=	c4 & c5;	
	

	//dY= sign * 0.027*  |X| + 0.1239
		FPGreatEq u7 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40400000),      // >=3     b.b
		.q      (c6)       //      q.q
		);
		
		
		FPLess u8 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40800000),      //  <4    b.b
		.q      (c7)       //      q.q
	);
	
	assign z4	=	c6 & c7;	

	//dY= sign * 0.0108* |X| + 0.0597
		FPGreatEq u9 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40800000),      // >=4    b.b
		.q      (c8)       //      q.q
		);
		
		
		FPLess u10 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40a00000),      //  <5    b.b
		.q      (c9)       //      q.q
	);
	
	assign z5	=	c8 & c9;		

	
	
//compute sigmoid output
		
	//Supply coeff inputs
	always@* begin
		CoeffMul1_net		=	0;
		CoeffMul2_net		=	0;
		CoeffAdd1_net		=	0;
//						PLAN Implementation															Encoding					
//							dY= 0 										|	|x| >= 5 				000001															
//							dY= 0.2383 									|	-1 < X< 1				000010			
//							dY= sign * 0.0934* |X| + 0.0.1239 	|	1 =<|X|< 2 				000100
//							dY= sign * 0.0596* |X| + 0.2202 		|	2 =<|X|< 3 				001000
//							dY= sign * 0.027*  |X| + 0.1239 		|	3 =<|X|< 4 				010000
//							dY= sign * 0.0108* |X| + 0.0597		|	4 =<|X|< 5				100000
		case({z5,z4,z3,z2,z1,z0})
		
			6'b000001: begin//							dY= 0 										|	|x| >= 5  
				CoeffMul1_net		=	32'h0;
				CoeffMul2_net		=	32'h0;
				CoeffAdd1_net		=	32'h0;
			end
		
			6'b000010: begin//dY= 0.2383 									|	-1 < X< 1
				CoeffMul1_net		=	32'h0;
				CoeffMul2_net		=	32'h0;
				CoeffAdd1_net		=	32'h3e7404ea;//0.2383
			end
			
			
			6'b000100: begin//dY= sign * 0.0934* |X| + 0.1239 	|	1 =<|X|< 2
				CoeffAdd1_net		=	32'h3dfdbf48;
				CoeffMul2_net		=	xAbs;
				if(sign==1'b1)begin
					CoeffMul1_net		=	32'hbdbf4880;//-0.0934
				end
				else	begin
					CoeffMul1_net		=	32'h3dbf4880;//0.0934
				end
			end
			
			
			6'b001000: begin//dY= sign * 0.0596* |X| + 0.2202 		|	2 =<|X|< 3
				CoeffAdd1_net		=	32'h3e617c1c;
				CoeffMul2_net		=	xAbs;
				if(sign==1'b1)begin
					CoeffMul1_net		=	32'hbd741f21;//-0.0596
				end
				else	begin
					CoeffMul1_net		=	32'h3d741f21;//0.0596
				end
			end
			
			
			6'b010000: begin//dY= sign * 0.027*  |X| + 0.1239 		|	3 =<|X|< 4
				CoeffAdd1_net		=	32'h3dfdbf48;
				CoeffMul2_net		=	xAbs;
				if(sign==1'b1)begin
					CoeffMul1_net		=	32'hbcdd2f1b;//-0.027
				end
				else	begin
					CoeffMul1_net		=	32'h3cdd2f1b;//0.027
				end
			end
			
			
			6'b100000: begin//dY= sign * 0.0108* |X| + 0.0597		|	4 =<|X|< 5	
				CoeffAdd1_net		=	32'h3d7487fd;
				CoeffMul2_net		=	xAbs;
				if(sign==1'b1)begin
					CoeffMul1_net		=	32'hbc30f27c;//-0.0108
				end
				else	begin
					CoeffMul1_net		=	32'h3c30f27c;//0.0108
				end
			end
		
		endcase
		
	end
	
	
	//register inputs
	always@(posedge clk)begin
		CoeffMul1	<=	CoeffMul1_net;
		CoeffMul2	<=	CoeffMul2_net;
		CoeffAdd1	<=	CoeffAdd1_net;
	end



	


endmodule
