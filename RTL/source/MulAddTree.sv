/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    12/9/2017 
// Design Name:    Multiply accumulate
// Module Name:    MulAddTree 
// Project Name:   LSTM
// Target Devices: Stratix V
// Tool versions: 15.1.1
// Description:  Multiply - adder tree
//
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps
`include "CustomTypes.sv"
module MulAddTree #(parameter INPUT_SIZE = 16, 
						  parameter WIDTH = DATA_WIDTH)
						(clk, rst, din1, din2, result);
						
						
input 							clk,rst;						
input [WIDTH-1:0]				din1 [INPUT_SIZE-1:0];						
input [WIDTH-1:0]				din2 [INPUT_SIZE-1:0];
output [WIDTH-1:0]			result;

//local parameters
localparam ADD_TREE_LEVELS = $clog2(INPUT_SIZE);


genvar gi, gj;

//signals
logic [WIDTH-1:0]				mul_t[INPUT_SIZE-1:0];//to hold mul array result
logic [WIDTH-1:0]				add_t[ADD_TREE_LEVELS-1:0][(INPUT_SIZE/2)-1:0];//to hold add tree results


//array of multipliers
generate 
begin: MulArray_BLOCK
	for(gi=0;gi<INPUT_SIZE;gi++)begin: MulArray_i
		PE MulArray(
			.clk(clk),
			.rst(rst),
			.CE(1'b1),
			.Op1(din1[gi]),
			.Op2(din2[gi]),
			.OpAct(32'h0),
			.mode(3'b001),//Mul
			.MulResult(mul_t[gi])
			);
	end
end
endgenerate


//Add tree
generate
begin: AddTree_Block
	for(gi=0;gi<ADD_TREE_LEVELS;gi++)begin:AddTree_i
		for(gj=0;gj<(2**(ADD_TREE_LEVELS-(gi+1)));gj++)begin: AddTree_j
			PE AddTree(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(gi==0 ? 	mul_t[2*gj] 		: add_t[gi-1][2*gj]),
				.Op2(gi==0 ? 	mul_t[(2*gj)+1] 	: add_t[gi-1][(2*gj)+1]),
				.OpAct(32'h0),
				.mode(3'b010),//add
				.AddResult(add_t[gi][gj])
				);
		end
	end

end
endgenerate


assign result	=	add_t[ADD_TREE_LEVELS-1][0];

						
endmodule			