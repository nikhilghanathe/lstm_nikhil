///////////////////////////////////////////////////////////////////////////////////
//// Company: MSRI
//// Engineer: N P Ghanathe
//// 
//// Create Date:    20/9/2017 
//// Design Name:    tb_matrix_mult_datapath
//// Module Name:    test bench matrix mult
//// Project Name:   MATRIX_MUL
//// Target Devices: Stratix V
//// Tool versions: 15.1.1
//// Description: test bench to test the datatpath for matrix multilpication- includes mimicking DRAM and controller
////
//// Dependencies: all underlying modules
////
//// Revision: 
//// Revision 0.01 - File Created
//// Additional Comments: 
////
////////////////////////////////////////////////////////////////////////////////////
//`timescale 1ns/1ps
//`include "CustomTypes.sv"
//
//module tb_matrix_mult_datapath;
//
//parameter WIDTH= DATA_WIDTH;
////Input matrices
//
//logic										clk, rst,CE[1:0], BufferSel, BufferLoadSig[1:0],ValidIn;
//logic [NUM_PARALLEL*WIDTH-1:0] 	DramCh0, DramCh1;//data from 2 channels of DRAM
//logic 									load;//load signal
//logic [2*TILE_SIZE-1:0]				ColSelB;
//logic 									ComputeStart;
// logic 									ChangeTile, ComputeDone,ValidOut;
// logic [WIDTH-1:0]					Result;
//
//
//
////signals
//logic [$clog2(TILE_SIZE/NUM_PARALLEL)-1:0] 	countB,countA;
//logic 													CTF;
//
//logic wen;
//
//integer i,j;
//
//matrix_mult_datapath UUT
//				(.clk(clk),
//				 .rst(rst),
//				 .CE(CE),
//				 .BufferSel(BufferSel),
//				 .BufferLoadSig(BufferLoadSig),
//				 .DramCh0(DramCh0),
//				 .DramCh1(DramCh1),
//				 .load(load),
//				 .ColSelB(ColSelB),
//				 .CompUteStart(CompUteStart),
//				 .Result(Result),			
//				);
//				
//localparam TEST_SIZE=TILE_SIZE;
//
//initial begin
//
//clk=0;
//rst=0;
//ValidIn=0;
//DramCh0	=	0;
//DramCh1	=	0;
//BufferSel=	0;
//BufferLoadSig[0]=0;
//BufferLoadSig[1]=0;
//load=0;
//ComputeStart=0;
//
//rst=1;
//#100;
//
//@(posedge clk);
//rst=0;
//
//
//
//load=1;//initialize selBload lines
////stream in matrices
////DramCh0 = 512'h00000001_h00000001_h00000001_h00000001_00000001_h00000001_h00000001_h00000001_00000001_h00000001_h00000001_h00000001_00000001_h00000001_h00000001_h00000001;
////DramCh1 = 512'h00000001_h00000001_h00000001_h00000001_00000001_h00000001_h00000001_h00000001_00000001_h00000001_h00000001_h00000001_00000001_h00000001_h00000001_h00000001;
//
//@(posedge clk);
//load=1;
//@(posedge clk);
//load=0;
//ComputeStart=1;
//@(posedge clk);
//
//
//
//
//
//
////A matrix
//valid=1;
//for(i=0;i<TEST_SIZE;i++)begin
//
//	for(j=0;j<TEST_SIZE/NUM_PARALLEL;j++)begin
//	
//	if(j%(COL_A/NUM_PARALLEL)==0)begin
//		rowA_sel= rowA_sel<<1;
//	end
//
//	if(j==0 )begin
//		rowA_sel=1;
//	end
//	
//
//	
//		if(j>=0 && j<PARALLELISM*(COL_A/NUM_PARALLEL))begin
//			addr_enA=1;
//		end
//		else begin
//			addr_enA=0;
//		end
//		
//		
////		
////		if(j==TEST_SIZE-1)begin
////			load_next_rowA=1;
////		end
////		else begin
////			load_next_rowA=0;
////		end
////		
//		a=64'h1111_1111_1111_1111;
//		//a=512'h00000001000000010000000100000001_00000001000000010000000100000001_00000001000000010000000100000001_00000001000000010000000100000001;
//		@(posedge clk);
//	end
//
//
//end
//
////counters for B
//always @(posedge clk)begin
//	if(rst==1'b1)begin
//		countA	<=	0;
//	end
//	else begin
//		if(countA==TILE_SIZE/NUM_PARALLEL-1)begin
//			countA	<=	0;
//		end
//		else if(ComputeStart==1'b1)begin
//			countA <= countA + 1'b1;
//		end
//	end
//end
//assign CTF	=	countA==(TILE_SIZE/NUM_PARALLEL)-1 ? 1'b1 : 1'b0;
//
//
//always @(posedge clk)begin
//	if(rst==1'b1)begin
//		countB	<=	0;
//		ColSelB	<=	0;
//	end
//	else begin
//		if(ChangeTile==1'b1)begin
//			countB	<=	0;
//		end
//		else if(CTF==1'b1)begin
//			countB <= countB + 1'b1;
//		end
//		//colselB
//		if(load==1'b1)begin
//			ColSelB	<=	1;
//		end
//		else if(CTF==1'b1)begin
//			countB <= countB + 1'b1;
//		end
//	end
//end
//
//assign ChangeTile	=	countB==(TILE_SIZE*(TILE_SIZE/NUM_PARALLEL))-1 ? 1'b1 : 1'b0; // change for B when we have filled one set of buffer of B
//
//always@(posedge clk)begin
//	if(rst==1'b1)begin
//		BufferLoadSig[0]	<=	1;
//		BufferLoadSig[1]	<=	0;
//		BufferSel			<=	0;
//	end
//	else if(ChangeTile==1'b1)begin
//		BufferLoadSig[0] <= ~ BuffeLoadSig[0];
//		BufferLoadSig[0] <= ~ BuffeLoadSig[0];
//		BufferSel		  <= ~ BufferSel;
//	end
//end	
//
////B matrix
//always begin
//
//	for(i=0;i<TEST_SIZE;i++)begin
//		
//		
//		if(i==0)begin
//			ColSelB={{TEST_SIZE-1{1'b0}},1'b1};		
//		end
//		else begin
//			ColSelB = ColSelB <<1;
//		end
//		
//		
//		
//		for(j=0;j<TEST_SIZE/16;j++)begin
//			@(posedge clk);//wait till one row is filled
//		end
//		
//	end
//
//end
//	
//
//	
//	
//	
//
//
//
//
//always begin
//	#5 clk=1;
//	#5 clk=0;
//end
//endmodule