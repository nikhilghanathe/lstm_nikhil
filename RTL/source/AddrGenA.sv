/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    12/9/2017 
// Design Name:    Addr_gen_MATRIX_A
// Module Name:    Addr_gen_MATRIX_A 
// Project Name:   MATRIX_MUL
// Target Devices: Stratix V
// Tool versions: 15.1.1
// Description: Addr generator for matrix A for pipelined design (data type - FP32)
//
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps
`include "CustomTypes.sv"



module AddrGenA #(parameter WIDTH=32, parameter SIZE=32*32) 
			(clk, rst, load, addrA, BufferLoadSigA, BufferSelA, BASE_ADDR, initLoadDone);



input 															rst,clk,load,initLoadDone;
input [63:0]													BASE_ADDR;
output logic [63:0] 											addrA;
output logic [1:0]											BufferLoadSigA;
output logic 													BufferSelA;


//signals
	//counters
logic [$clog2(TILE_SIZE)-1:0] 	 						rowComputeCount;//counter to generate flag after each row of A has been placed
logic [$clog2(TILE_SIZE/NUM_PARALLEL)-1:0] 			rowChangeCount;


//logic addr_en; //flag to generate addresses for DRAM
logic 															oneComputeDone; //flag to indicate one row has completed computation
logic [$clog2(TILE_SIZE/NUM_PARALLEL)-1:0] 			addrA_t;
logic 															loadDone;
logic 															addEn,rowChange, CE, computeCE;

integer 															C_ROW_CHANGE;

integer i;



///////////////////////////////////////////////////////////////////////////////
// 								counters
//////////////////////////////////////////////////////////////////////////////
always@(posedge clk)begin
	if(rst==1'b1)begin
		rowChangeCount <= 0;
	end
	else if(loadDone==1'b1) begin
		rowChangeCount <= 0 ;
	end
	else begin
		if(CE==1'b1)
			rowChangeCount <= rowChangeCount + 1'b1;
	end
end
assign loadDone = (rowChangeCount==((TILE_SIZE/NUM_PARALLEL)-1)) ? 1'b1 : 1'b0;



//increment counter for every element read from a row
always@(posedge clk) begin
	if(rst==1'b1)begin
		rowComputeCount<=0;
	end
	else begin
		if(oneComputeDone==1'b1) begin
			rowComputeCount <= 0;
		end
		else if(computeCE==1'b1)begin
			rowComputeCount <= rowComputeCount + 1'b1;
		end
	end
end

//assert when row is computed
assign oneComputeDone = (rowComputeCount==TILE_SIZE-1) ? 1'b1 : 1'b0;




//increment counter after every row read

//////////////////////////////////////////////////////////////////////////////////////////////////////
//											State machine
//////////////////////////////////////////////////////////////////////////////////////////////////////

localparam										ST_INIT 					=	3'd0,
													ST_LOAD_BUFF1_INIT	=	3'd1,
													ST_LOAD_BUFF2_INIT	=	3'd2,
													ST_WAIT					=	3'd3,
													ST_LOAD_BUFF1			=	3'd4,
													ST_WAIT_BUFF1			=	3'd5,
													ST_LOAD_BUFF2			=	3'd6,
													ST_WAIT_BUFF2			=	3'd7;

logic[2:0]										state, next_state;



//state assign
always@(posedge clk)begin
	if(rst==1'b1)begin
		state	<=	ST_INIT;
	end
	else begin
		state	<=	next_state;
	end
end													
													

//state calc
always@* begin
	BufferLoadSigA 		=	2'b00;
	CE							=	0;
	computeCE				=	0;
	next_state				=	state;
	BufferSelA				=	0;
	case(state)
		ST_INIT: begin
			if(load==1'b1)begin
				next_state		=	ST_LOAD_BUFF1_INIT;
			end
		end
		
		ST_LOAD_BUFF1_INIT:begin
			CE					=	1;
			BufferLoadSigA	=	2'b01;
			if(loadDone==1'b1)begin
				next_state		=	ST_LOAD_BUFF2_INIT;
				CE					= 	0;
			end
		end	
		
		ST_LOAD_BUFF2_INIT:begin
			CE					=	1;
			BufferLoadSigA	=	2'b10;
			if(loadDone==1'b1)begin
				next_state		=	ST_WAIT;
				CE					= 	0;
			end
		end	
		
		ST_WAIT:begin//wait for initLoadDone to start computation
			if(initLoadDone==1'b1)begin
				next_state		=	ST_WAIT_BUFF2;
			end
		end
		
		ST_LOAD_BUFF1:begin
			CE					=	1;
			computeCE		=	1;
			BufferLoadSigA	=	2'b01;
			BufferSelA		=	0;
			if(loadDone==1'b1)begin
				next_state	=	ST_WAIT_BUFF1;
				CE				=	0;
			end
		end
		
		ST_WAIT_BUFF1:begin// wait till all computations are done
			BufferSelA		=	0;
			computeCE		=	1;
			if(oneComputeDone==1'b1)begin
				next_state	=	ST_LOAD_BUFF2;
				BufferSelA		=	2'b10;
				computeCE		=	0;
			end
		end
		
		ST_LOAD_BUFF2:begin
			CE					=	1;
			computeCE		=	1;
			BufferLoadSigA	=	2'b10;
			BufferSelA		=	1;
			if(loadDone==1'b1)begin
				next_state	=	ST_WAIT_BUFF2;
				CE				=	0;
			end
		end
		
		ST_WAIT_BUFF2:begin// wait till all computations are done
			BufferSelA		=	1;
			computeCE		=	1;
			if(oneComputeDone==1'b1)begin
				BufferSelA		=	0;
				next_state		=	ST_LOAD_BUFF1;
				computeCE		=	0;
			end
		end
		
	endcase
end													
													

//////////////////////////////////////////////////////////////////////////////////////////////////////

////assert done when all comptation has been done
//assign compute_done = (compute_done_count==ROW_A-1) ? 1'b1 : 1'b0;
//
//
////flag for generating addresses
//assign s_addr_en= loadA && (row_compute_count>=0 && row_compute_count<(PARALLELISM*(COL_A/NUM_PARALLEL)))  ?  1 : 0 ;
//assign addr_en = s_addr_en;
//
//
////reg for holding address
//always@(posedge clk ) begin
//	if(rst==1'b1)begin
//		addrA	<= BASE_ADDR;//!!!!!!!!!!!!!!!!!!!!!!!!UNSURE OF THIS LOGIC
//	end
//	else begin
//		if(s_addr_en==1'b1) 
//			s_addr_A <= s_addr_A + 1'b1;
//	end
//end
//
//assign addr_A = s_addr_A;

endmodule