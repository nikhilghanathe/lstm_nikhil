//Module that acts as an independent addr generator to read/write from dram 
`timescale 1ns/1ps

module DramRdWr #(parameter SIZE = 10)(clk, rst, go, done, wen, addrOut);

	input 										clk, rst, go;
	output logic 								wen, done;
	output logic [$clog2(SIZE)-1:0]		addrOut;


	//state machine that controls the operation
	typedef enum {ST_INIT, ST_ADDR_GEN}States;
	States state, next_state;
	
	//SIGNALS
	logic [$clog2(SIZE)-1:0]				count;

	//state assign
	always@(posedge clk)begin
		if(rst==1'b1)begin 
			state = ST_INIT;
		end
		else begin
			state = next_state;
		end // else
	end



	//state calc
	always@* begin
		wen = 0;
		next_state = state;
		case(state)
			ST_INIT:begin
				if(go==1'b1)begin
					next_state = ST_ADDR_GEN;
				end
			end

			ST_ADDR_GEN:begin
				wen = 1;
				if(TF==1'b1)begin
					next_state = ST_INIT;
					wen = 0;
				end
			end
		endcase // state
	end


 	//counter
 	always@(posedge clk)begin
 		if(rst==1'b1)begin
 			count <= 0;
		end
		else begin 
			if(TF==1'b1)begin
				count <= 0;
			end // if(TF=1'b1)
			else if(wen==1'b1)begin 
				count <= count + 1;
			end
 		end
	end

assign TF 		= count==SIZE-1;
assign done  	= TF;


endmodule // DramrrdWr
