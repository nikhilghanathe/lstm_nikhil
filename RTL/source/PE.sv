`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	MSRI	
// Engineer: 	NP Ghanathe	
// 
// Create Date: 1/18/2017 
// Design Name: PE
// Module Name: PE
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 	Basic processing element for LSTM units.
//					Modes of Operation
//						0 - Multiplication  - Latency 4 cycles, DSP -2
//						1 -  Addition 		- Latency 5 cycles, DSP - 0
//						2 -  Multiply add   - Latency 10 cycles, DSP -2
//						3 -  Sigmoid 		- Latency 1 + 5 + 1 = 7 cycles, DSP -0
//						4 -  dSigmoid 		- Latency 1 + 10 + 1 = 12 cycles, DSP -2
//						5 -  Tanh 			- Latency 1 + 10 + 1 = 12 cycles, DSP -2
//						6 -  dTanh 			- Latency 1 + 5 + 1 = 7 cycles, DSP - 0
//
//						Arria10 latency of ops : 
//										  Mul = 5
//										  Add = 5
//										  Sig = 6
//										  Tanh = 7
//										  dSig = 6
//										  dTanh = 6
// 
// Dependencies: All underlying modules
// 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
//							
//						
// 
//////////////////////////////////////////////////////////////////////////////////

`include "CustomTypes.sv"


module PE #(parameter WIDTH = DATA_WIDTH, parameter MODE = 0) (clk, rst, CE, Op1, Op2, OpAct, mode, MulResult, AddResult, SigResult, TanhResult, dSigResult, dTanhResult);

input 									clk, rst, CE;
input [WIDTH-1:0]						Op1, Op2, OpAct;
input [2:0]								mode;
output logic [WIDTH-1:0]				MulResult, AddResult, SigResult, TanhResult, dSigResult, dTanhResult;


//Signals
  logic [WIDTH-1:0]						AddOp1, AddOp2, AddOut, MulOut, MulOp1, MulOp2, AddSig1, AddSig2, AddTanh1, AddTanh2, MulTanh1, MulTanh2, MuldTanh1,MuldTanh2, MuldSig2,MuldSig1, AdddTanh1, AdddSig1,DSPResult;
  logic 								DelayCount;
  logic 								TF,MulSel;
  logic 								CEAcc, CEAccDelay[2:0],   CESig, CESigDelay[3:0],   CETanh, CETanhDelay[7:0], rangeDelay[5:0];
  logic [WIDTH-1:0]						AccDelay[2:0],AddIn1Delay[2:0], AddIn2Delay[2:0], MulIn1Delay[2:0], MulIn2Delay[2:0];//delay for Inputs
  logic [1:0] 							AddSel;
  logic 								ValidChain[12:0];//valid shift reg for measuring latency and storing values of result
  logic [2:0]							modeDelay[12:0];//TO SET correct reult using the mode
  logic									valid, sign, range;
  logic [WIDTH-1:0]						TanhResult_ff, OpAct_ff;
	
  integer 							i;	
 
  //Arria10
	// DSP u0 (
	// 	.aclr     (rst),     //     aclr.aclr
	// 	.ax       (AddOp1),       //       ax.ax
	// 	.ay       (MulOp1),       //       ay.ay
	// 	.az       (MulOp2),       //       az.az
	// 	//.chainout (<connected-to-chainout>), // chainout.chainout
	// 	.clk      (clk),      //      clk.clk
	// 	.ena      (CE),      //      ena.ena
	// 	.result   (DSPResult)    //   result.result
	// );

	//StratixV


generate
begin:PE_GEN_BLK	
	if(MODE==2 || MODE==4 || MODE==5)
		FPMulAdd fpmuladd_inst (
			.clk    (clk),    //    clk.clk
			.areset (rst), // areset.reset
			.a      (MulOp1),      //      a.a
			.b      (MulOp2),      //      b.b
			.c      (AddOp1),      //      c.c
			.q      (DSPResult)       //      q.q
		);
	end


	if(MODE==0)begin
		FPMul fpmul_inst (
			.clk    (clk),    //    clk.clk
			.areset (rst), // areset.reset
			.a      (MulOp1),      //      a.a
			.b      (MulOp2),      //      b.b
			.q      (DSPResult)       //      q.q
		);
	end

	if(MODE==1 || MODE==3 || MODE==6)begin
		FPAdd fpadd_inst (
			.clk    (clk),    //    clk.clk
			.areset (rst), // areset.reset
			.a      (AddOp1),      //      a.a
			.b      (AddOp2),      //      b.b
			.q      (DSPResult)       //      q.q
		);
	end


end
endgenerate
	


	
	//state assign
	always@(posedge clk)begin
		if(rst==1'b1)begin
			state		<=	ST_INIT;
			OpAct_ff	<=	32'h0;
		end
		else begin
			state	<=	next_state;
			OpAct_ff	<=	OpAct;
		end
	end	
										
	
	
	
assign sign		=	OpAct[31];
	


//results
	always@(posedge clk)begin

		//FIX VALID SIGNAL LATER!!!!!!!!!!!!!!!!!!!!!!
		valid <=0;
		

		//valid chain regs for storing outputs
		ValidChain[0]	<=	valid;
		modeDelay[0]	<=	mode;
		rangeDelay[0]	<=	range;
		for(i=0;i<4;i++)begin
			modeDelay[i+1]	 <=	modeDelay[i];
			ValidChain[i+1] <=	ValidChain[i];
			rangeDelay[i+1] <=		rangeDelay[i];
		end
		

		
		if(ValidChain[3]==1'b1 && modeDelay[3]==3'b001)begin
			MulResult		<=	DSPResult;
		end
		
		if(ValidChain[3]==1'b1 && modeDelay[3]==3'b0010)begin
			AddResult		<=	DSPResult;
		end
		
		
	//Acc reg, Sig reg, tanh, dSig,, dTanh registers
		//Sig
		if(ValidChain[4]==1'b1 && modeDelay[4]==3'b011)begin
			SigResult 		<=	DSPResult;
		end
		
		//Tanh
		if(ValidChain[4]==1'b1 && modeDelay[4]==3'b100)begin
			TanhResult_ff	<=	DSPResult;
		end	
		
		if(rangeDelay[5]==1'b1)
			TanhResult 		<=	{sign ^ 1'b1,TanhResult_ff[30:26],TanhResult_ff[25 : 23] - 3'b010,TanhResult_ff[22:0]};
		else
			TanhResult		<=	TanhResult_ff;
		
		//dSig
		if(ValidChain[4]==1'b1 && modeDelay[4]==3'b101)begin
			dSigResult 		<=	DSPResult;
		end
		
		//dTanh
		if(ValidChain[4]==1'b1 && modeDelay[4]==3'b110)begin
			dTanhResult 	<=	DSPResult;
		end
	
		
	end
	
	
	//Activation Functions
	generate
		//mul
		if(MODE==0)begin
			assign MulOp1 = Op1;
			assign MulOp2 = Op2;
			assign AddOp1 = 0;
			assign AddOp2 = 0;
		end

		//add
		if(MODE==1)begin
			assign MulOp1 = 0;
			assign MulOp2 = 0;
			assign AddOp1 = Op1;
			assign AddOp2 = Op2;
		end

		//MulAdd
		if(MODE==2)begin
			assign MulOp1 = Op1;
			assign MulOp2 = Op2;
			assign AddOp1 = 0;
			assign AddOp2 = 0;
		end

		//Sigmoid
		if(MODE==3)begin
			SigmoidPLAN5 U_SIG (
					.clk(clk),
					.rst(rst),
					.x(OpAct),
					.CoeffAdd1(AddOp1),
					.CoeffAdd2(AddOp2)
			);

			assign MulOp1 = 0;
			assign MulOp2 = 0;
		end

		//Tanh
		if(MODE==4)begin
			TanhPWL U_TANH (
					.clk(clk),
					.rst(rst),
					.x(OpAct),
					.SqX(DSPResult),
					.CoeffMul1(MulOp1),
					.CoeffMul2(MulOp2),
					.CoeffAdd1(AddOp1),
					.range(range)
					
			);

			assign AddOp2 = 0;
		end
		
		//dSigmoid
		if(MODE==5)begin
			dSigmoidPLAN5 U_dSIG (
				.clk(clk),
				.rst(rst),
				.x(OpAct),
				.CoeffMul1(MulOp1),
				.CoeffMul2(MulOp2),
				.CoeffAdd1(AddOp1)
			);

			assign AddOp2 = 0;

		end
		
		//dTanh
		if(MODE==6)begin
			dTanhPWL U_dTANH (
					.clk(clk),
					.rst(rst),
					.x(OpAct),
					.CoeffMul1(MulOp1),
					.CoeffMul2(AddOp1),
					.CoeffAdd1(AddOp2)
					
			);

			assign MulOp2 = 0;

		end
	endgenerate
	

endmodule