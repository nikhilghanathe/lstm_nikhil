/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    27/9/2017 
// Design Name:    PE
// Module Name:    PE 
// Project Name:   MATRIX_MUL
// Target Devices: Stratix V
// Tool versions: 15.1.1
// Description: Processing Element for matrix multiplication. Multiple PE's run in parallel
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

`include "CustomTypes.sv"

module MatrixMulPE #(parameter WIDTH=DATA_WIDTH, parameter CONN_POINT=0) (clk, rst, op1, op2, result);

input 																 	clk, rst;
input [NUM_PARALLEL*WIDTH-1:0] 								 	op1 [TILE_SIZE/NUM_PARALLEL-1:0];
input [WIDTH-1:0] 		      								 	op2 [TILE_SIZE-1:0][TILE_SIZE-1:0];
output logic [WIDTH - 1:0] 	result;

//local paramters
localparam	ADD_TREE_LEVELS	=	TILE_SIZE-1;


//signals
wire [WIDTH-1:0] 														dinA [TILE_SIZE/NUM_PARALLEL-1:0][NUM_PARALLEL-1:0];
wire [WIDTH-1:0]										 				dinB [TILE_SIZE/NUM_PARALLEL-1:0][NUM_PARALLEL-1:0];

logic  [WIDTH-1:0] 													mul_t[TILE_SIZE/NUM_PARALLEL-1:0];
logic  [WIDTH-1:0] 													add_t[TILE_SIZE/(NUM_PARALLEL)-1:0];
logic  [WIDTH-1:0] 													Result1Delay[2:0];



genvar gi, gj;




//dinA buffer to decouple NUMPARALLEL*WIDTH bit vector into 16 separate WIDTH bit vector
generate
begin: dinA_BLOCK
	for(gi=0;gi<TILE_SIZE/NUM_PARALLEL;gi++) begin: dinA_buffer_i
		for(gj=0;gj<NUM_PARALLEL;gj++) begin: dinA_buffer_j
			assign dinA[gi][gj]= op1[gi][WIDTH*(gj+1)-1:WIDTH*(gj)];
		end
	end
end
endgenerate


//dinB buffer to couple all B inputs from LFSR rotating columns
generate
begin: dinB_BLOCK
	for(gi=0;gi<TILE_SIZE/NUM_PARALLEL;gi++) begin: dinB_buffer_i
		for(gj=0;gj<NUM_PARALLEL;gj++) begin: dinB_buffer_j
			assign dinB[gi][gj]= op2[(NUM_PARALLEL*gi)+gj][CONN_POINT];
		end
	end
end
endgenerate




//Array of multipliers and add tree
generate 
begin:MulAddTreeArray_block
	for(gi=0;gi<TILE_SIZE/NUM_PARALLEL;gi++)begin:MulAddTreeArray_i
		MulAddTree #(.INPUT_SIZE(NUM_PARALLEL))
			MulAddTreeArray(
			.clk(clk),
			.rst(rst),
			.din1(dinA[gi]),
			.din2(dinB[gi]),
			.result(mul_t[gi])
		
		
			);
	end
end
endgenerate



//reg for first output of mult_acc
always@(posedge clk) begin
	Result1Delay[0]	<= mul_t[0];
//	for(int i=0;i<0;i++)begin
//		Result1Delay[i+1]	<= Result1Delay[i];
//	end
		
end
	
	

generate 
begin: AddRegTree_block
	for(gi=0;gi<TILE_SIZE/NUM_PARALLEL-1;gi++) begin: U_AddReg
		PE AddReg(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(gi==0 ? 	Result1Delay[0] 		: add_t[gi-1]),
				.Op2(mul_t[gi+1]),
				.OpAct(32'h0),
				.mode(3'b010),//add
				.AddResult(add_t[gi])
				);

	end
end
endgenerate


assign result= add_t[TILE_SIZE/NUM_PARALLEL-2];





endmodule