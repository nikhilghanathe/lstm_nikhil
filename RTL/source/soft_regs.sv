module soft_regs

(
input           clk, rst,
input   [31:0]  address_in,
input   [63:0]  data_in,
input           req_in,
input           rd_wr,              //rd - 0, wr - 1


output		[2:0]	  state,
output reg  [63:0]  read_data,
output reg          cpl_val

);


/*******************************************************************************
                Parameters
*******************************************************************************/

//------------------------------------------------------------------------------


wire    [63:0]  reg00, reg08,reg10, reg18, reg20, reg28, reg30, reg38, reg40; 
reg    [63:0]  reg00_r, reg08_r,reg10_r, reg18_r, reg20_r, reg28_r, reg30_r, reg38_r, reg40_r; 



reg                 mems_en;
reg                 bm_en;
reg                 pd_en;
reg                 fbbc;
reg                 rp_err;
reg                 sta;
reg                 rta;
reg                 mas;
reg                 dpe;


////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
// Configuration Registers Read
//------------------------------------------------------------------------------


always @ (posedge clk)
    if (!rst) read_data <= 64'h0;
    else if (req_in & ~rd_wr) case (address_in)
        32'h000 : read_data <= reg00;
        32'h008 : read_data <= reg08;
        32'h010 : read_data <= reg10;
        32'h018 : read_data <= reg18;
        32'h020 : read_data <= reg20;
        32'h028 : read_data <= reg28;
        32'h030 : read_data <= reg30;
        32'h038 : read_data <= reg38;
        32'h040 : read_data <= reg40;
        default : read_data <= 64'h0;
    endcase

always @(posedge clk)
    cpl_val <=  req_in;

//==============================================================================
//  REG 00 (DW 0)   Device ID / Vendor ID
//==============================================================================
//  31-16           Device ID                   R
//------------------------------------------------------------------------------
//  15-0            Vendor ID                   R
//==============================================================================
wire    write_00 = req_in & rd_wr & (address_in == 32'h000);

assign reg00 =  reg00_r;

always @(posedge clk) 
    if (!rst)   reg00_r <=  64'h0; 
    else        reg00_r <=  write_00    ?   data_in :   reg00_r;

//==============================================================================
//  REG 08 (DW 1)   Status / Command 
//==============================================================================
//  31 (15)         Detected Parity Error       R/W1C
//  30 (14)         Reserved
//  29 (13)         Master Abort Status         R/W1C
//  28 (12)         Received Target Abort       R/W1C
//  27 (11)         Signaled Target Abort       R/W1C
//  26-25 (10-9)    DEVSEL Timing Status        R
//  24 (8)          Received Parity Error       R/W1C
//  23 (7)          Fast Back to Back Capable   R
//  22-16 (6-0)     Not used
//------------------------------------------------------------------------------
//  15-7            Reserved                    -
//  6               Parity Error Detection Enable   R/W
//  5-3             Reserved
//  2               Bus Master Function Enable      R/W
//  1               Memory Space Enable             R/W
//  0               IO Space Enable                 R
//==============================================================================

wire    write_08 = req_in & rd_wr & (address_in == 32'h008);

assign reg08 =  reg08_r;

always @(posedge clk) 
    if (!rst)   reg08_r <=  64'h0; 
    else        reg08_r <=  write_08    ?   data_in :   reg08_r;  
    
//==============================================================================
//  REG 10 (DW 1)   Status / Command 
//==============================================================================
//  31 (15)         Detected Parity Error       R/W1C
//  30 (14)         Reserved
//  29 (13)         Master Abort Status         R/W1C
//  28 (12)         Received Target Abort       R/W1C
//  27 (11)         Signaled Target Abort       R/W1C
//  26-25 (10-9)    DEVSEL Timing Status        R
//  24 (8)          Received Parity Error       R/W1C
//  23 (7)          Fast Back to Back Capable   R
//  22-16 (6-0)     Not used
//------------------------------------------------------------------------------
//  15-7            Reserved                    -
//  6               Parity Error Detection Enable   R/W
//  5-3             Reserved
//  2               Bus Master Function Enable      R/W
//  1               Memory Space Enable             R/W
//  0               IO Space Enable                 R
//==============================================================================

wire    write_10 = req_in & rd_wr & (address_in == 32'h010);

assign reg10 =  reg10_r;

always @(posedge clk) 
    if (!rst)   reg10_r <=  64'h0; 
    else        reg10_r <=  write_10    ?   data_in :   reg10_r;
    
assign state = reg00[3:1];

endmodule   
