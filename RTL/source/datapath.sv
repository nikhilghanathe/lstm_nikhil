`timescale 1ns/1ps
/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    12/9/2017 
// Design Name:    reg_load
// Module Name:    reg_load 
// Project Name:   LSTM
// Target Devices: Stratix V
// Tool versions:  15.1.1
// Description: 	 datapath for LSTM layer - 1 layer
//
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "CustomTypes.sv"


module datapath #(parameter WIDTH=DATA_WIDTH) (clk, rst,CE, DramCh0, DramCh1, 
														//matMul ops
															load, ChangeTile, ComputeDone, ColSelB, BufferSelA, BufferSelB, BufferLoadSigA, ComputeStart, Result, ValidIn, ValidOut,onePassFifoDone,
														//elem-wise ops	
															fifoInSel, fifoWrSig,fifoSelLoad, fifoRdSig,RdSel, WrSel, matMulValid, stateBuffSel, gateMode, elemValid, changeMode, activationMode, gateCompValid, stateCompValid, stateCompDone, forwardDone,
													
														//backprop ports
														dnextCWrDone, cellAddrDone_b,  dnextCUpdateDone, computeDerivWrStart, derivWrDone,
														dnextCFill, dnextCUpdateSig, computeDerivativeValid
															
															);

									
//forward prop
	input												clk, rst,CE[1:0], BufferSelA, BufferSelB,ValidIn,fifoWrSig, fifoRdSig, fifoSelLoad;
	input [1:0]											BufferLoadSigA;
	input 												stateBuffSel;//select between prev an next cell states
	input [NUM_PARALLEL*WIDTH-1:0] 						DramCh0, DramCh1;//data from 2 channels of DRAM
	input 												load;//load signal
	input [2*TILE_SIZE-1:0]								ColSelB;
	input 												ComputeStart;
	input 												stateCompValid;
	output logic 										ChangeTile, ComputeDone,ValidOut,onePassFifoDone;
	output logic [WIDTH-1:0]							Result;



	//elementwise operations
	input [B-1:0]										RdSel, WrSel;
	input 												fifoInSel;
	output logic										matMulValid, changeMode, gateCompValid;
	input [1:0]											gateMode;
	input												elemValid;
	input	[2:0]										activationMode;
	output logic 										stateCompDone, forwardDone;




//babckprop
	output logic 										dnextCWrDone, cellAddrDone_b,  dnextCUpdateDone,computeDerivWrStart, derivWrDone;																		
	input 												dnextCFill, dnextCUpdateSig, computeDerivativeValid;


//signals
logic	[2*TILE_SIZE-1:0]		 					ColSelB_ff;		
		//operands for multiplications
logic [NUM_PARALLEL* WIDTH-1:0] 					MulOp1 		[1:0][(TILE_SIZE/NUM_PARALLEL)-1:0];
logic [WIDTH-1:0] 									MulOp2 		[1:0][TILE_SIZE-1:0][TILE_SIZE-1:0];
logic 												SelLoadB		[TILE_SIZE-1:0];
logic [WIDTH-1:0] 									BufferB 		[TILE_SIZE-1:0];//MATB buffer
logic 												CE_B			[1:0][(TILE_SIZE)-1 : 0];
logic [TILE_SIZE-1:0]								SelB			[2*TILE_SIZE-1:0];//for two buffers
logic 												valid_ff 	[5 * (TILE_SIZE/NUM_PARALLEL + $clog2(NUM_PARALLEL)+1 + 10 -1)  : 0];//valid delay line
logic 												SelInitB		[TILE_SIZE-1:0];
logic [TILE_SIZE-1:0]								SelInitVal;
logic [WIDTH-1:0]									matMulResult;


//accumulation block
logic [$clog2(B*TILE_SIZE)-1:0]				fifoCount,fifoCountDelay[4:0];
logic 												fifoRdEn[B-1:0], fifoWrEn[B-1:0], fifoWrEn_ff[B-1:0][5:0], fifoFull[B-1:0], fifoEmpty[B-1:0];
logic [WIDTH-1:0]									fifoOut[B-1:0], fifoIn[B-1:0];
logic 												matMulValid_t	;
logic [WIDTH-1:0]									matMulResultDelay [5:0], preAdd[B-1:0];

//elementwise computations
logic [WIDTH-1:0]									actOut[B-1:0];
logic													validAct[B-1:0][9+4:0];
logic [WIDTH-1:0]									activationDelay[4:0];





genvar 												gi, gj, gk;
integer i;





////////////////////////////////////Clock enable and Valid arrays///////////////////////////////////////////

//delay colB_sel by one clk cycle to compensate for selB_load
always@(posedge clk)begin
	ColSelB_ff <= ColSelB;
end


//CE chain for Matrix B
//CE for B matrix columns to start shifting
always@(posedge clk) begin
	CE_B[0][0]	<=	CE[0];
	CE_B[1][0]	<=	CE[1];
	for(i=0;i<TILE_SIZE/NUM_PARALLEL-1;i++)begin
		CE_B[0][i+1] <= CE_B[0][i]; 
		CE_B[1][i+1] <= CE_B[1][i]; 
	end
end


always@(posedge clk) begin
   valid_ff[0]	<=	ValidIn;
	for(i=0;i<( 5 * (TILE_SIZE/NUM_PARALLEL+($clog2(NUM_PARALLEL)+1)) + 1 ) -1;i++)begin
		valid_ff[i+1] <= valid_ff[i]; 
	end
end
assign ValidOut = valid_ff[5*($clog2(NUM_PARALLEL) + 1 +  (TILE_SIZE/NUM_PARALLEL - 1) ) + 5 - 1 -1];// extra -1 to accomdate delay of register load 



//**********************************B MATRIX*******************************
//*************************************************************************
//convert packed to unpacked
assign SelInitVal		=		{{TILE_SIZE-NUM_PARALLEL{1'b0}},{NUM_PARALLEL{1'b1}}};
generate 
begin: SelInit_block
	for(gi=0;gi<TILE_SIZE;gi++)begin: SelInit_i	
		always@*begin
			SelInitB[gi]  =	SelInitVal[gi];
		end
	end
end
endgenerate

//SELECT LINE SHIFT REG For laoding 16 values at a time 
	LFSR_User #(.WIDTH(1),
			.SIZE(TILE_SIZE),
			.SHIFT_SIZE(NUM_PARALLEL)) 
				U_SEL_SHIFT
			(.clk(clk),
			 .rst(rst),
			 .CE(1'b1),
			 .LE(load==1'b1 ?  {TILE_SIZE{1'b1}} : {TILE_SIZE{1'b0}}),
			 .din(SelInitB),
			 .dout(SelLoadB)//select lines connected to each B col. only 16 asserted at any particular clk cycle (ANDed with ColSelB)			
			);
	
	
	
generate 
begin: B_SEL_AND_ROW_SEL_BLOCK
	for(gi=0;gi< 2 * TILE_SIZE;gi++) begin: B_SEL_AND_ROW_SEL_i
		for(gj=0;gj<TILE_SIZE;gj++) begin: B_SEL_AND_ROW_SEL_j
			always@* begin
				SelB[gi][gj] = SelLoadB[gj] & ColSelB[gi];//select lines connected to each B col. only 16 asserted at any particular clk cycle (ANDed with ColSelB)	
			end
		end
	end
end
endgenerate


//buffer for loading B matrix
generate 
begin: B_buffer_block
	for(gi=0;gi<TILE_SIZE/NUM_PARALLEL;gi= gi+1) begin: B_buffer_i
		for(gj=0;gj<NUM_PARALLEL;gj++) begin: B_buffer_j
			assign BufferB[NUM_PARALLEL*gi + (gj)] [WIDTH-1:0]= DramCh1[(WIDTH*(gj+1))-1 : WIDTH*(gj)];
		end
	end

end
endgenerate






//Generate COL_B number of LFSR for rotating columns
generate 
begin: B_COL_BLOCK
	for(gi=0;gi<2;gi++)begin:BufferSet
		for(gj=0;gj<TILE_SIZE;gj++) begin: B_COL
			LFSR_User #(.WIDTH(WIDTH),
					 .SIZE(TILE_SIZE), 
					 .SHIFT_SIZE(1)) 		//shift by 1
					 U_B_COL
					(.clk(clk),
					 .rst(rst),
					 .CE((CE_B[gi][gj % NUM_PARALLEL] ) | (SelB[TILE_SIZE * gi + gj])),//two separate CE chains for B
					 .LE(SelB[TILE_SIZE * gi + gj]),
					 .din(BufferB),
					 .dout(MulOp2[gi][gj])		
					);
		end
	end
end
endgenerate







/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												matrix A buffer
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//SERIAL-in PARALLEL-out for MAT-A

generate
begin: SIPO_RAM_BLOCK
	for(gi=0;gi<2;gi++)begin: BufferSet
		LUTRAM_SIPO #(.WIDTH(NUM_PARALLEL*WIDTH),
					  .SIZE(TILE_SIZE/NUM_PARALLEL)) 
					  U_SIPO_MATA
						(.clk(clk),
						 .rst(rst),
						 .CE(BufferLoadSigA[gi]),
						 .SerialIn(DramCh0),
						 .ParallelOut(MulOp1[gi])
						 );
	end
end
endgenerate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 //matrix multiplication block
 
	MatrixMulPE MMUl	(
		.clk(clk),
		.rst(rst),
		.op1(BufferSelA ? MulOp1[1] : MulOp1[0]),
		.op2(BufferSelB ? MulOp2[1] : MulOp2[0]),
		.result(matMulResult)
		);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								Accumulation blocks
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

logic	[WIDTH-1:0]						activationIn, activationOutSig, activationOutTanh, activationInDelay;
logic 									activationElemValid[B-1:0];
logic 									elemWiseValid[5+9 +5 -1:0];
logic										fifoInSelDelay[5:0];
logic										fifoSelSig[B-1:0], fifoSelCE;
logic [1:0]								gateModeDelay[9+5+5 - 1:0];






//delay matMulResult to sync with preadder of FIFO
always@(posedge clk)begin
	matMulResultDelay[0]	<=	matMulResult;
	fifoInSelDelay[0]		<=	fifoInSel;
	for(int i=0;i<5;i++)begin
		matMulResultDelay[i+1]	<=	matMulResultDelay[i];
		fifoInSelDelay[i+1]		<=	fifoInSelDelay[i];
	end
end


//counter to keop track of which FIFO block has to be used to write matMul result
always@(posedge clk)begin
	if(rst==1'b1)begin
		fifoCount			<=	0;
	end
	else if(matMulValid_t	==1'b1)begin
		fifoCount			<=	fifoCount	+ 1'b1;
	end
	
//	//delay fifo count for wrEn
//	fifoCountDelay[0]	<=	fifoCount;
//	for(i=0;i<4;i++)begin
//		fifoCountDelay[i+1]	<=	fifoCountDelay[i];
//	end
end


//asserted when the matMul operation spits out valid results
assign matMulValid_t		=	valid_ff[5*($clog2(NUM_PARALLEL) + 1 +  (TILE_SIZE/NUM_PARALLEL - 1) ) + 5 - 1];
assign matMulValid		=	matMulValid_t;
assign onePassFifoDone	=	fifoCount==(B*TILE_SIZE)-1	?	1'b1 : 1'b0;
assign fifoSelCE			=	((fifoCount+1'b1) % TILE_SIZE) == 0;


//shiftreg for fifos
always@(posedge clk)begin
	if(rst==1'b1)begin
		for(i=0;i<B;i++)begin
			fifoSelSig[i]	<=	1'b0;
		end
	end
	else begin
		if(fifoSelLoad==1'b1)begin
			fifoSelSig[0]	<=	1'b1;//loadval
		end
		else if(fifoSelCE==1'b1)begin
			fifoSelSig[0]	<=	fifoSelSig[B-1];//circuar shift
		end
	
	//shift
		for(i=0;i<B-1;i++)begin
			if(fifoSelCE==1'b1)begin
				fifoSelSig[i+1]	<=	fifoSelSig[i];	
			end
		end
	end
end



//generate block which generates 'B' parallel FIFO and element-wise processing blocks
generate
begin: ElemWiseProcGenBlk
	for(gi=0;gi<B;gi++)begin:ElemWiseProcGenBlk_i
		
	//adder to add previously computed partial sums
	PE PreAdd(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(matMulResult),
				.Op2(fifoOut[gi]),
				.OpAct(32'h0),
				.mode(3'b010),//add
				.AddResult(preAdd[gi])
				);
				
	//WrEn for fifo when matMulResult is valid
		assign fifoWrEn[gi]					=	WrSel[gi];//(fifoWrSig &&  matMulValid_t && fifoSelSig[gi]) ? 1'b1 : 1'b0;
		always@(posedge clk)begin//delay by 5 clk cycle becasue Reading and writing is delayed due to latency of preadd
			fifoWrEn_ff[gi][0]		<=	fifoWrEn[gi]; 
			for(i=0;i<5;i++)begin
				fifoWrEn_ff[gi][i+1]	<=	fifoWrEn_ff[gi][i];
			end
		end
	//MUX to write inside FIFOs
		assign fifoIn[gi]						=	fifoInSel==1'b1	?	preAdd[gi]	:	matMulResultDelay[4];
		
	//'B' parallel FIFOs	
		FIFO_User U_ACCBLOCKS (
			.data  (fifoIn[gi]),  //  fifo_input.datain
			.wrreq (fifoWrEn[gi] && !fifoFull[gi]), //            .wrreq
			.rdreq (fifoRdEn [gi] && !fifoEmpty[gi]), //            .rdreq
			.clock (clk), //            .clk
			.q     (fifoOut[gi]),     // fifo_output.dataout
			//.usedw (<connected-to-usedw>), //            .usedw
			.full  (fifoFull[gi]),  //            .full
			.empty (fifoEmpty[gi])  //            .empty
		);
		
	//RdEn for fifos - same as WrEn
		assign fifoRdEn[gi]					=	RdSel[gi];//( fifoRdSig &&  matMulValid_t && fifoSelSig[gi]) ? 1'b1 : 1'b0;
	
	//valid signals for activation calc
		assign activationElemValid[gi]	=	fifoRdEn[gi] && elemValid;
		
		always@(posedge clk)begin
			validAct[gi][0]						<=	activationElemValid[gi];
			for(int i =0;i<4+9-1;i++)begin
				validAct[gi][i+1]						<=	validAct[gi][i];
			end
		end
	
	end
end
endgenerate





/********************************FORWARD PASS ELEM_WISE OPS****************************************/




//select the correct line for activation calc
always@(posedge clk)begin
	if(rst==1'b1)begin
		activationIn	<=	0;
	end
	else begin
		for(int i=0;i<B;i++)begin
			if(validAct[i][5] ==1'b1)begin
				activationIn	<=	preAdd[i];
			end
		end
	end
end

//
////activation calculation
//
//	PE ActivationCalc(
//			.clk(clk),
//			.rst(rst),
//			.CE(1'b1),
//			.Op1(32'h0),
//			.Op2(32'h0),
//			.OpAct(activationIn),
//			.mode(activationMode),//activation mode
//			.SigResult(activationOutSig),
//			.TanhResult(activationOutTanh)
//			);
//delay by 7 clk cycles and include activation later.
delayChain #(.WIDTH(DATA_WIDTH), .SIZE(7)) dummyDelay (.clk(clk), .din(activationIn), .dout(activationInDelay));
		
//delay regs to match the sigmoid and tanh latency from PE		
always@(posedge clk)begin
	elemWiseValid[0]		<=	elemValid;//valid signal for further elemWise ops
	gateModeDelay[0]		<=	gateMode;
	

	for(i=0;i<5 +5+ 9-1;i++)begin
		elemWiseValid[i+1]	<=	elemWiseValid[i];
		gateModeDelay[i+1]	<=	gateModeDelay[i];
	end
	

end




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//BRAM memories to store the intermediate matrices and model
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//declarations
logic [NUM_PARALLEL*WIDTH-1:0]					cellStateIn, cellState, hiddenState, cellStateB1, cellStateB2, hiddenStateIn, hiddenStateB1, hiddenStateB2, inputGateIn, forgetGateIn, outputGateIn, memUpdateGateIn;
logic [$clog2((B*H)/NUM_PARALLEL)-1:0]			hiddenStateRAddr, hiddenStateWAddr, cellStateWAddr, cellStateRAddr, cellAddr, cellAddrDelay[7+1+5+5+12+5-1:0], inputGateRAddr, forgetGateRAddr, outputGateRAddr, memUpdateGateRAddr, inputGateWAddr, forgetGateWAddr, outputGateWAddr, memUpdateGateWAddr ;
logic 											cellStateValid, stateCompValidDelay[7+1+5+5+12+5-1:0];
logic [$clog2((B*H)/NUM_PARALLEL)-1:0]			gateRamAddr;
logic											changeMode_t;
logic [WIDTH*NUM_PARALLEL-1:0]					inputVal, outputVal, forgetVal, memUpdateVal,inputVal_act, outputVal_act, forgetVal_act, memUpdateVal_act, inputVal_act_ff, outputVal_act_ff, forgetVal_act_ff, memUpdateVal_act_ff;
logic											gateCompvalid;
logic [WIDTH-1:0]								ixg[NUM_PARALLEL-1:0], fxc[NUM_PARALLEL-1:0], tanhOutput[NUM_PARALLEL-1:0];
logic [$clog2(NUM_PARALLEL)-1:0]				countEn;
logic 											ramEn, hiddenStateWen_1, hiddenStateWen_2, cellStateWen_1, cellStateWen_2, forgetGateWen, inputGateWen, outputGateWen, memUpdateGateWen;
logic [WIDTH-1:0]								elemInputDelayTanh[NUM_PARALLEL-1:0],elemInputDelaySig[NUM_PARALLEL-1:0], elemInputDelay[NUM_PARALLEL-1:0] ;
logic [WIDTH*NUM_PARALLEL-1: 0]					gateIn, outputValDelay[16:0];



always@(posedge clk)begin
	elemInputDelay[0]	<=	activationInDelay;
	for(i=0;i<NUM_PARALLEL-1;i++)begin
		elemInputDelay[i+1]	<=	elemInputDelay[i];
	end
end

//always@(posedge clk)begin
//	elemInputDelayTanh[0]	<=	activationOutTanh;
//	for(i=0;i<NUM_PARALLEL-1;i++)begin
//		elemInputDelayTanh[i+1]	<=	elemInputDelayTanh[i];
//	end
//end

generate
begin: gateInSigBufferBlock
	for(gi=0;gi<NUM_PARALLEL;gi++)begin:gateInSigBuff_i
		assign gateIn[WIDTH*(gi+1)-1 : WIDTH*gi]	=	elemInputDelay[gi];
	end 
end
endgenerate


//generate
//begin: gateInTanhBufferBlock
//	for(gi=0;gi<NUM_PARALLEL;gi++)begin:gateInTanhBuff_i
//		assign gateInTanh[WIDTH*(gi+1)-1 : WIDTH*gi]	=	elemInputDelay[gi-1];
//	end 
//end
//endgenerate



//input gate
	  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_inputGate (
		.clk(clk),
		.waddr(inputGateWAddr),
		.wdata(inputGateIn),
		.wen(inputGateWen),//input gate mode
		.raddr(inputGateRAddr),
		.rdata(inputVal)
		);
		
//forget gate
	  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_forgetGate (
		.clk(clk),
		.waddr(forgetGateWAddr),
		.wdata(forgetGateIn),
		.wen(forgetGateWen),
		.raddr(forgetGateRAddr),
		.rdata(forgetVal)
		);
		
//output gate
	  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_outputGate (
		.clk(clk),
		.waddr(outputGateWAddr),
		.wdata(outputGateIn),
		.wen(outputGateWen),
		.raddr(outputGateRAddr),
		.rdata(outputVal)
		);
		
//memoryUpdate gate
	  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_memUpdateGate (
		.clk(clk),
		.waddr(memUpdateGateWAddr),
		.wdata(memUpdateGateIn),
		.wen(memUpdateGateWen),
		.raddr(memUpdateGateRAddr),
		.rdata(memUpdateVal)
		);
		
		
		

//toggle between next and prev cell and hidden states		
/////////////////////////////////////////////////////
//When stateBuffSel ==1'b0 then you are reading from buff1 and writing into buff2.
//When stateBuffSel ==1'b1 then you are reading from buff2 and writing into buff1.
//////////////////////////////////////////////////////
//Cell & hidden State buff1
//cell state
	  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_cellState_1 (
		.clk(clk),
		.waddr(cellStateWAddr),
		.wdata(cellStateIn),
		.wen(cellStateWen_1),
		.raddr(cellStateRAddr),
		.rdata(cellStateB1)
		);
		
//hidden state
	  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_hiddenState_1 (
		.clk(clk),
		.waddr(hiddenStateWAddr),
		.wdata(hiddenStateIn),
		.wen(hiddenStateWen_1),//input gate mode
		.raddr(hiddenStateRAddr),
		.rdata(hiddenStateB1)
		);
		
		
//Cell & hidden State buff2
//cell state
	  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_cellState_2 (
		.clk(clk),
		.waddr(cellStateWAddr),
		.wdata(cellStateIn),
		.wen(cellStateWen_2),
		.raddr(cellStateRAddr),
		.rdata(cellStateB2)
		);
		
//hidden state
	  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_hiddenState_2 (
		.clk(clk),
		.waddr(hiddenStateWAddr),
		.wdata(hiddenStateIn),
		.wen(hiddenStateWen_2),//input gate mode
		.raddr(hiddenStateRAddr),
		.rdata(hiddenStateB2)
		);


	
assign cellState		=	stateBuffSel==1'b0 ? cellStateB1 	: cellStateB2;
assign hiddenState	=	stateBuffSel==1'b0 ? hiddenStateB1 	: hiddenStateB2;		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//counter to generate flag for storing 16 words at a time in RAM
always@(posedge clk)begin
	if(rst==1'b1)begin
		countEn	<=	0;
	end
	else begin
		if(elemWiseValid[13]==1'b1)begin
			countEn	<=	countEn + 1'b1;
		end
	end	
end
assign ramEn	=	countEn==NUM_PARALLEL-1 ? 1'b1 : 1'b0;


//assign gateCompValid	=	validAct[8];
//gate RAMs addr generator 
always@(posedge clk)begin
	if(rst==1'b1)begin
		gateRamAddr	<=	0;
	end
	else begin
		if(changeMode_t==1'b1)begin
			gateRamAddr	<=	0;
		end
		else if(ramEn==1'b1)begin
			gateRamAddr	<=	gateRamAddr	+ 1'b1;
		end
	end
end
assign changeMode_t	=	gateRamAddr==((B*H)/NUM_PARALLEL)-1 ? 1'b1 : 1'b0;
assign changeMode		=	changeMode_t;


//state calc counter for addr generation
always@(posedge clk)begin
	if(rst==1'b1)begin
		cellAddr	<=	0;
	end
	else begin
		if(stateCompDone==1'b1)begin
			cellAddr	<=	0;
		end
		else if(stateCompValid==1'b1)begin
			cellAddr	<=	cellAddr	+ 1'b1;
		end
	end
end


//cellState and hidden state addr generation
always@(posedge clk)begin
	//delay chain for CellState and hiddenState calc
	cellAddrDelay[0]			<=	cellAddr;
	stateCompValidDelay[0]	<=	stateCompValid;
	for(i=0;i<7+1+5+5+12+5-1;i++)begin
		cellAddrDelay[i+1]			<=	cellAddrDelay[i];
		stateCompValidDelay[i+1]	<=	stateCompValidDelay[i];
	end
end

assign stateCompDone			=	cellAddr==(B*H)/NUM_PARALLEL-1	?  1'b1 : 1'b0;


//addr signals for BRAMS
assign inputGateRAddr		=	stateCompValid==1'b1 				?	cellAddr 				: {$clog2((B*H)/NUM_PARALLEL){1'b0}};
assign forgetGateRAddr		=	stateCompValid==1'b1 				?	cellAddr 				: {$clog2((B*H)/NUM_PARALLEL){1'b0}};
assign outputGateRAddr		=	stateCompValid==1'b1 				?	cellAddr				 	: {$clog2((B*H)/NUM_PARALLEL){1'b0}};
assign memUpdateGateRAddr	=	stateCompValid==1'b1 				?	cellAddr 				: {$clog2((B*H)/NUM_PARALLEL){1'b0}};

assign inputGateWAddr		=	stateCompValidDelay[6]==1'b1 		?	cellAddrDelay[6]		: gateRamAddr;
assign forgetGateWAddr		=	stateCompValidDelay[6]==1'b1 		?	cellAddrDelay[6]		: gateRamAddr;
assign outputGateWAddr		=	stateCompValidDelay[6]==1'b1 		?	cellAddrDelay[6]		: gateRamAddr;
assign memUpdateGateWAddr	=	stateCompValidDelay[6]==1'b1 		?	cellAddrDelay[6]		: gateRamAddr;

assign inputGateWen			=	gateModeDelay[13]==2'b00 && elemWiseValid[13]==1'b1 && ramEn==1'b1 | stateCompValidDelay[6];
assign forgetGateWen			=	gateModeDelay[13]==2'b00 && elemWiseValid[13]==1'b1 && ramEn==1'b1 | stateCompValidDelay[6];
assign outputGateWen			=	gateModeDelay[13]==2'b00 && elemWiseValid[13]==1'b1 && ramEn==1'b1 | stateCompValidDelay[6];
assign memUpdateGateWen		=	gateModeDelay[13]==2'b00 && elemWiseValid[13]==1'b1 && ramEn==1'b1 | stateCompValidDelay[6];

assign inputGateIn			=	stateCompValidDelay[6]==1'b1		?	inputVal_act_ff			:	gateIn;
assign forgetGateIn			=	stateCompValidDelay[6]==1'b1		?	forgetVal_act_ff		:	gateIn;
assign outputGateIn			=	stateCompValidDelay[6]==1'b1		?	outputVal_act_ff		:	gateIn;
assign memUpdateGateIn		=	stateCompValidDelay[6]==1'b1		?	memUpdateVal_act_ff		:	gateIn;



assign cellStateRAddr		=	stateCompValidDelay[7]==1'b1 		?	cellAddr 						: {$clog2((B*H)/NUM_PARALLEL){1'b0}};
assign cellStateWAddr		=	stateCompValidDelay[9+7]==1'b1 		?	cellAddrDelay[9+7]  			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};
assign cellStateWen_1		=	stateBuffSel==1'b0 					?  1'b0 							: 	stateCompValidDelay[9];	
assign cellStateWen_2		=	stateBuffSel==1'b0 					? stateCompValidDelay[9]			:	1'b0 ;


assign hiddenStateRAddr		=	{$clog2((B*H)/NUM_PARALLEL){1'b0}};	
assign hiddenStateWAddr		=	stateCompValidDelay[21+7]==1'b1 	?	cellAddrDelay[21+7] 			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};
assign hiddenStateWen_1		=	stateBuffSel==1'b0 					?  1'b0 							: stateCompValidDelay[21+7];
assign hiddenStateWen_2		=	stateBuffSel==1'b0					?  stateCompValidDelay[21+7] 		: 1'b0;



//delay Sig output by one clk cycle to match delay of Tanh output
always@(posedge clk)begin
	inputVal_act_ff			<=	inputVal_act;
	outputVal_act_ff		<=	outputVal_act;
	forgetVal_act_ff		<=	forgetVal_act;
	
	
//	delay output val by 17 clkc cycles to sync
	outputValDelay[0]		<=	outputVal_act_ff;
	for(int i=0;i<17-1;i++)begin
		outputValDelay[i+1]	<=	outputValDelay[i];
	end
end



//state calculation block
generate 
begin:stateCalcBlock
	for(gi=0;gi<NUM_PARALLEL;gi++)begin:stateCalcBlock_i
	
		//activation calculation
		PE ActivationCalc_input(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(32'h0),
				.Op2(32'h0),
				.OpAct(inputVal[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.mode(3'b011),//activation mode = sig
				.SigResult(inputVal_act[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
		
		//activation calculation
		PE ActivationCalc_forget(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(32'h0),
				.Op2(32'h0),
				.OpAct(forgetVal[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.mode(3'b011),//activation mode = sig
				.SigResult(forgetVal_act[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
		//activation calculation
		PE ActivationCalc_output(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(32'h0),
				.Op2(32'h0),
				.OpAct(outputVal[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.mode(3'b011),//activation mode = sig
				.SigResult(outputVal_act[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
		//activation calculation
		PE ActivationCalc_memUpdate(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(32'h0),
				.Op2(32'h0),
				.OpAct(memUpdateVal[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.mode(3'b011),//activation mode = Tanh
				.SigResult(memUpdateVal_act[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
				
				
		
				
				
				
				
		//f * prev_C
		PE fVal_x_cVal(
					.clk(clk),
					.rst(rst),
					.CE(1'b1),
					.Op1(forgetVal_act_ff[WIDTH*(gi+1)-1 : WIDTH*gi]),
					.Op2(cellState[WIDTH*(gi+1)-1 : WIDTH*gi]),
					.OpAct(32'h0),
					.mode(3'b001),//mul mode
					.MulResult(fxc[gi])
					);

		//i * g
		PE iVal_x_gVal(
					.clk(clk),
					.rst(rst),
					.CE(1'b1),
					.Op1(inputVal_act_ff[WIDTH*(gi+1)-1 : WIDTH*gi]),
					.Op2(memUpdateVal_act[WIDTH*(gi+1)-1 : WIDTH*gi]),
					.OpAct(32'h0),
					.mode(3'b001),//mul mode
					.MulResult(ixg[gi])
					);
			
		//next_c = f * prev_c + i * g
		PE cellStatCalc(
					.clk(clk),
					.rst(rst),
					.CE(1'b1),
					.Op1(ixg[gi]),
					.Op2(fxc[gi]),
					.OpAct(32'h0),
					.mode(3'b010),//add mode
					.AddResult(cellStateIn[WIDTH*(gi+1)-1 : WIDTH*gi])
					);
						
		//tanh(next_c)
		PE outputTanh(
					.clk(clk),
					.rst(rst),
					.CE(1'b1),
					.Op1(32'h0),
					.Op2(32'h0),
					.OpAct(cellStateIn[WIDTH*(gi+1)-1 : WIDTH*gi]),
					.mode(3'b100),//tanh mode
					.TanhResult(tanhOutput[gi])
					);
					
		//next_h = o * tanh(next_c)
		PE hiddenStatCalc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(tanhOutput[gi]),
				.Op2(outputValDelay[16][WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(hiddenStateIn[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
							
	end
end
endgenerate
		
			
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/********************************FORWARD PASS ELEM_WISE OPS END HERE****************************************/


















/********************************BACKWARD PASS ELEM_WISE OPS****************************************/

logic [NUM_PARALLEL*WIDTH-1:0]				dnextC,dtanh_nextC, tanh_nextC, dnextCIn, dnextCUpdated, o_x_dnextH, dnextCCurr, dnextCDelay, o_x_dnextHDelay, prevC, nextC, dnextCurr, di, df, dprevC, dg, doVal, doDelay   ;
logic [$clog2(NUM_PARALLEL*WIDTH)-1:0]		cellAddr_b, cellAddrUpd_b, cellAddr_o_Delay_b, dnextCWAddr, dnextCRAddr,gateAddrUpd_b ;
logic [$clog2(NUM_PARALLEL*WIDTH)-1:0]		inputGateRAddr_b, forgetGateRAddr_b, outputGateRAddr_b, memUpdateGateRAddr_b, cellStateRAddr_b, hiddenStateRAddr_b;
logic [$clog2(NUM_PARALLEL*WIDTH)-1:0]		inputGateWAddr_b, forgetGateWAddr_b, outputGateWAddr_b, memUpdateGateWAddr_b, cellStateWAddr_b, hiddenStateWAddr_b;
logic										dnextCUpdateSigDelay, dprevCWen, cellAddrUpdDone_b, computeDerivativeValidDelay;




//counter for addr gen for elemwise ops in backprop
always@(posedge clk)begin
	if(rst==1'b1)begin
		cellAddr_b	<=	0;
	end
	else begin
		if(cellAddrDone_b==1'b1)begin
			cellAddr_b	<=	0;
		end
		else if(dnextCFill==1'b1	||	 dnextCUpdateSig==1'b1 )begin
			cellAddr_b	<=	cellAddr_b	+	1'b1;
		end
	end
	
end

assign cellAddrDone_b	=	cellAddr_b == (B*H)/NUM_PARALLEL -1 ? 1'b1 : 1'b0;

assign dnextCWrDone 		=  cellAddrDone_b;
assign dnextCUpdateDone		=  cellAddrUpdDone_b;



//counter for addr gen for elemwise ops in backprop
always@(posedge clk)begin
	if(rst==1'b1)begin
		cellAddrUpd_b	<=	0;
	end
	else begin
		if(cellAddrUpdDone_b==1'b1)begin
			cellAddrUpd_b	<=	0;
		end
		else if(dnextCUpdateSigDelay==1'b1)begin
			cellAddrUpd_b	<=	cellAddrUpd_b	+	1'b1;
		end
	end
	
end

assign cellAddrUpdDone_b	=	cellAddrUpd_b == (B*H)/NUM_PARALLEL -1 ? 1'b1 : 1'b0;


//fill and update dnextC
assign dnextCWAddr	=	dnextCFill==1'b1										?	cellAddr_b	:	cellAddrUpd_b;
assign dnextCWen 	=	dnextCFill | dnextCUpdateSigDelay;
assign dnextCRAddr	=	dnextCUpdateSig==1'b1 									?	cellAddr_b	:	{$clog2((B*H)/NUM_PARALLEL){1'b0}};
assign dnextCIn		= 	dnextCFill==1'b1										?	DramCh1		:	dnextCUpdated;

///////////////////////////////
// --  = signals taken care of
//////////////////////////////

//addr signals for BRAMS back prop 	//write sigs placeholder
assign inputGateRAddr_b			=	computeDerivativeValid==1'b1 						?	cellAddrUpd_b 			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//--
assign forgetGateRAddr_b		=	computeDerivativeValid==1'b1 						?	cellAddrUpd_b 			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//--
assign outputGateRAddr_b		=	dnextCUpdateSig==1'b1 					   			?	cellAddrUpd_b 			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//--
assign memUpdateGateRAddr_b		=	computeDerivativeValid==1'b1 						?	cellAddrUpd_b			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//--
//assign aValsWaddr				=	

assign inputGateWAddr_b			=	computeDerivativeValidDelay==1'b1 					?	gateAddrUpd_b 			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//--
assign forgetGateWAddr_b		=	computeDerivativeValidDelay==1'b1 					?	gateAddrUpd_b 			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//--
assign outputGateWAddr_b		=	dnextCUpdateSig==1'b1 			   					?	cellAddrUpd_b 			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//--
assign memUpdateGateWAddr_b		=	computeDerivativeValidDelay==1'b1 					?	gateAddrUpd_b			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//--

assign inputGateIn_b			=	di;//--
assign forgetGateIn_b			=	df;//--
assign outputGateIn_b			=	doDelay;//--
assign memUpdateGateIn_b		=	dg;//--




assign cellStateRAddr_b			=	dnextCUpdateSig==1'b1 									?	cellAddr_b 			: {$clog2((B*H)/NUM_PARALLEL){1'b0}};//
assign cellStateWAddr_b			=	{$clog2((B*H)/NUM_PARALLEL){1'b0}};

assign hiddenStateRAddr_b		=	{$clog2((B*H)/NUM_PARALLEL){1'b0}};	
assign hiddenStateWAddr_b		=	{$clog2((B*H)/NUM_PARALLEL){1'b0}};






//******delay chains***********

//validchain for  do and  dnextC 
delayChain #(.WIDTH(1),.SIZE(16)) dnextC_valid(.clk(clk),  .din(dnextCUpdateSig),  .dout(dnextCUpdateSigDelay));


//validchain for dnextC
delayChain #(.WIDTH(NUM_PARALLEL*WIDTH),.SIZE(6+5)) dnextC_delay(.clk(clk),  .din(dnextC),  .dout(dnextCDelay));

//delay mul op by 1 clk cycle to wait for dtanh op
delayChain #(.WIDTH(NUM_PARALLEL*WIDTH),.SIZE(1)) waitFordTanh(.clk(clk),  .din(o_x_dnextH),  .dout(o_x_dnextHDelay)); 

//delay dnextH by 7 clk cycle to wait or tanh(nextC) op
delayChain #(.WIDTH(NUM_PARALLEL*WIDTH),.SIZE(7)) waitForTanh(.clk(clk),  .din(DramCh1),  .dout(dnextHDelay)); 

//delay dnextH by 7 clk cycle to wait or tanh(nextC) op
delayChain #(.WIDTH(NUM_PARALLEL*WIDTH),.SIZE(4)) waitFordnextCUpd(.clk(clk),  .din(doVal),  .dout(doDelay)); 







//Memories required for bacward pass

//dnextC and dprevC RAM
  RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_dnextC (
	.clk(clk),
	.waddr(dnextCWAddr),
	.wdata(dnextCIn),
	.wen(dnextCFill | dprevCWen),
	.raddr(dnextCRAddr),
	.rdata(dnextC)
	);	


	RAM #(.WIDTH(NUM_PARALLEL*DATA_WIDTH),.SIZE((B*H)/NUM_PARALLEL)) U_RAM_aVals (
	.clk(clk),
	.waddr(aValsWaddr),
	.wdata(DramCh1),
	.wen(aValsWaddr),
	.raddr(aValsRaddr),
	.rdata(aVals)
	);





//	DramRdWr #(.SIZE((B*H)/NUM_PARALLEL) U_DRAM_READ (.clk(clk), .rst(rst), .go());

		
//Brackprop element-wise ops

generate
begin:backPropElem_calc_blk
	for(gi=0;gi<NUM_PARALLEL;gi++)begin:backPropElem_calc_blk_i
		
	
//**********************Backprop into step 5*********************		
	//do calc operations
		// tanh(nextC)
		PE tanh_nextC_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(32'h0),
				.Op2(32'h0),
				.OpAct(cellState[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.mode(3'b100),//tanh mode
				.TanhResult(tanh_nextC[WIDTH*(gi+1)-1 : WIDTH*gi])
				);

		PE do_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(tanh_nextC[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextHDelay[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(),
				.mode(3'b001),//mul mode
				.MulResult(doVal[WIDTH*(gi+1)-1 : WIDTH*gi])
				);



	//dnextC calc operations
		// o * dnext_h
		PE o_x_dnextH_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(DramCh1[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(outputVal[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(o_x_dnextH[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
		// dtanh(nextC)
		PE dtanh_nextC_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(32'h0),
				.Op2(32'h0),
				.OpAct(cellState[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.mode(3'b110),//dtanh mode
				.dTanhResult(dtanh_nextC[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
				
		// o * dnext_h * dtanh(nextC)
		PE o_x_dnextH_x_dtanhnextC_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(o_x_dnextHDelay[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dtanh_nextC[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(dnextCurr[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
		// dnext_c += o * (1 - np.tanh(next_c)**2) * dnext_h
		PE dtanh_nextCUpd(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(dnextCDelay[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCurr[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b010),//add mode
				.AddResult(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
				
			



	//******************	Backprop into step 4******************
//compute derivatives of gates
//    df = prev_c * dnext_c
//    dprev_c = f * dnext_c
//    di = g * dnext_c
//    dg = i * dnext_c

logic [$clog2((B*H)/NUM_PARALLEL)-1: 0]						gateAddr_b, gateAddrUpd_b;
logic 														gateAddrDone_b, gateAddrUpdDone_b, computeDerivativeValidDelay;

//******delay chains***********

//validchain for  do and  dnextC 
delayChain #(.WIDTH(1),.SIZE(5)) gateUpdValid(.clk(clk),  .din(computeDerivativeValid),  .dout(computeDerivativeValidDelay));


//counter for addr gen for calc derivative of gates read
assign gateAddr_b 			=	cellAddrUpd_b;
assign gateAddrDone_b 		=	cellAddrUpdDone_b;	


//counter for addr gen for calc derivative of gates write
always@(posedge clk)begin
	if(rst==1'b1)begin
		gateAddrUpd_b	<=	0;
	end
	else begin
		if(gateAddrUpdDone_b==1'b1)begin
			gateAddrUpd_b	<=	0;
		end
		else if(computeDerivativeValidDelay==1'b1)begin
			gateAddrUpd_b	<=	gateAddrUpd_b	+	1'b1;
		end
	end
	
end

assign gateAddrUpdDone_b	=	gateAddrUpdDone_b == (B*H)/NUM_PARALLEL -1 ? 1'b1 : 1'b0;
assign derivativeCompDone	=	gateAddrUpdDone_b;

	
		// df = prevC * dnextCUpdated
		PE df_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(prevC[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(df[WIDTH*(gi+1)-1 : WIDTH*gi])
				);		
				
		// dprevC = f  * dnextCUpdated
		PE dprevC_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(forgetVal[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(dprevC[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
		
		// di = g * dnextCUpdated
		PE di_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(memUpdateVal[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(df[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
				
		// dg = i * dnextCUpdated
		PE dg_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(inputVal[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(dg[WIDTH*(gi+1)-1 : WIDTH*gi])
				);
				
				
				
*******************Backprop into the gate 3 ******************************
   dag = dtanh(ag) * dg
   dao = dSig(ao) * do
   daf = dSig(af) * df
   dai = dSig(ai) * di
		
				
		// dag = dtanh(ag) * dg
		PE df_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(prevC[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(df[WIDTH*(gi+1)-1 : WIDTH*gi])
				);	
				
		// dao = dSig(ao) * do
		PE df_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(prevC[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(df[WIDTH*(gi+1)-1 : WIDTH*gi])
				);	
			
			
		// daf = dSig(af) * df
		PE df_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(prevC[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(df[WIDTH*(gi+1)-1 : WIDTH*gi])
				);	
			

		// dai = dSig(ai) * di
		PE df_calc(
				.clk(clk),
				.rst(rst),
				.CE(1'b1),
				.Op1(prevC[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.Op2(dnextCUpdated[WIDTH*(gi+1)-1 : WIDTH*gi]),
				.OpAct(32'h0),
				.mode(3'b001),//mul mode
				.MulResult(df[WIDTH*(gi+1)-1 : WIDTH*gi])
				);	
						
				
	end
end
endgenerate 
 
 
assign prevC = cellStateB1;
assign nextC = cellStateB2;
 




/********************************BACKWARD PASS ELEM_WISE OPS END HERE****************************************/


assign Result			=	matMulResult;
assign forwardDone	=	hiddenStateWAddr==((B*H)/NUM_PARALLEL)-1 ? 1'b1 : 1'b0;




endmodule