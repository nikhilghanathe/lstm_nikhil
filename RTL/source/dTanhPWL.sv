`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	MSRI	
// Engineer: 	NP Ghanathe	
// 
// Create Date: 1/18/2017 
// Design Name: dTanhPWL
// Module Name: dTanhPWL
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 	Compute first order derivative of Tanh using PWL method (Isosceles triangular Approximation)
// 
// Dependencies: All underlying modules
// 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
//							
//						PWL Implementation
//							tanh'(x)=
//										1 - (|x|)/2						,	0≤|x|≤2;
//										0			  					  	,	otherwise
// 	
//////////////////////////////////////////////////////////////////////////////////
`include "CustomTypes.sv"

module dTanhPWL #(parameter WIDTH=DATA_WIDTH)(clk,rst, x, CoeffMul1,CoeffMul2, CoeffAdd1);

input 							clk, rst;
input [WIDTH-1:0]				x;
output logic [WIDTH-1:0]	CoeffMul1,CoeffMul2, CoeffAdd1;


//Signals
logic [WIDTH-1:0]				xAbs;
logic 							signX,c0,c1,z0;


//sign bit
assign signX 	=	x[WIDTH-1];



//Abs value
assign xAbs =	{1'b0, x[WIDTH-2:0]};



//magnitude comaparator

	//y=1
	FPGreatEq u0 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h00000000),      // >=0     b.b
		.q      (c0)       //      q.q
	);
	
	FPLessEq u1 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40000000),      // <=2     b.b
		.q      (c1)       //      q.q
	);

	
	assign z0		=	c0	&	c1;

	
//							tanh'(x)=
//										1 - (|x|)/2						,	0≤|x|≤2;  => 1 - (xAbs)/2   
//										0			  					  	,	otherwise
	always@(posedge clk) begin
		if(z0==1'b1)begin
			CoeffMul1	<=	32'h3f800000;
			CoeffMul2	<=	32'h3f800000;
			CoeffAdd1	<=	{1'b1,x[30:25],x[24:23] - 2'b01,x[22:0]};//divide by two is equal to mantissa_new = mantissa_old-1
		end
		else begin
			CoeffMul1	<=	0;
			CoeffMul2	<=	0;
			CoeffAdd1	<=	0;
		end
	
	end




	
	
endmodule
