/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    20/9/2017 
// Design Name:    tb_matrix_mult_user_app
// Module Name:    test bench matrix mult
// Project Name:   MATRIX_MUL
// Target Devices: Stratix V
// Tool versions: 15.1.1
// Description: test bench to test the UserApp with LSTM datapath
//
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns/1ps
`include "CustomTypes.sv"


module tb_UserApp;

logic 													clk, go, go_b, rst;
logic [DRAM_BUS_WIDTH-1:0]							DramCh1, DramCh0;
 logic [31:0]											result;
 logic 													done;


 
 //signals
 logic									addrStart;
 
UserApp LSTM (
		.clk(clk),
		.rst(rst),
		.go(go),
		.go_b(go_b),
		.DramCh0(DramCh0),
		.DramCh1(DramCh1),
		.result(result),
		.done(done)
	);

	
	
initial begin
clk			=	0;
rst			=	1;
go				=	0;
go_b			=	0;
DramCh0		=	0;
DramCh1		=	0;

#100;
@(posedge clk);
rst			=	0;
@(posedge clk);

DramCh0 		=  512'h3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000;
DramCh1 		=  512'h3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000_3f800000;
go				=	1;
@(posedge clk);
go				=	0;






end

always begin
	#5 clk=1;
	#5 clk=0;
end




	
	
endmodule