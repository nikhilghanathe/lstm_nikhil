`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	MSRI	
// Engineer: 	NP Ghanathe	
// 
// Create Date: 1/18/2017 
// Design Name: SigmoidPLAN5
// Module Name: SigmoidPLAN5
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 	Compute sigmoid function using Piecewise linear approximation technique using 5 segments (PLAN-5)
// 
// Dependencies: All underlying modules
// 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
//							
//						PLAN Implementation																									Encoding					
//							Y= 1 								|	|x| >= 5 																0001															
//							Y= 0.03125* |x| + 0.84375 			|	2.375 =<|X| < 5		sign==1 => 0.15625	; sign==0	=>	0.84375			0010			
//							Y= O.125*|X|+O.625 					|	1 =<|X|<2.375 		sign==1 => 0.625 	; sign==0	=>	0.375			0100
//							Y= 0.25* |X| + 0.5 					|	O=<|X|< 1 			sign==1 => 0.5 		; sign==0	=>	0.5				1000
//							Y= 1-Y 								|	x<0 	
// 
//////////////////////////////////////////////////////////////////////////////////
`include "CustomTypes.sv"

module SigmoidPLAN5 #(parameter WIDTH= DATA_WIDTH)( clk, rst,x, CoeffAdd1,CoeffAdd2);

input 							clk,rst;
input [WIDTH-1:0]				x;
output logic [WIDTH-1:0]	CoeffAdd1, CoeffAdd2;


//Signals
logic [WIDTH-1:0]				MulOut,xAbs, CoeffMult, CoeffAdd1_net, CoeffAdd2_net, CoeffMult_net, CoeffAdd_net;
logic 							sign,c0,c1,c2,c3,c4,c5,z0,z1,z2,z3;
logic [WIDTH-1:0]				y_net1, y_net2;


//sign bit
assign sign 	=	x[WIDTH-1];


//Abs value
assign xAbs 	=	{1'b0, x[WIDTH-2:0]};



//magnitude comaparator

	//y=1
	FPGreatEq u0 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40a00000),      // >=5     b.b
		.q      (z0)       //      q.q
	);

	
	//Y= 0.03125* /X/ + 0.84375 
	
		FPGreatEq u1 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40180000),      // >=2.375     b.b
		.q      (c0)       //      q.q
		);
		
		
		FPLess u2 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40a00000),      //  <5    b.b
		.q      (c1)       //      q.q
	);

assign z1	=	c0 & c1;
	
	
	//Y=O.125*/X/+O.625
		FPGreatEq u3 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h3f800000),      // >=1     b.b
		.q      (c2)       //      q.q
		);
		
		
		FPLess u4 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h40180000),      //  <2.375    b.b
		.q      (c3)       //      q.q
	);
	
	assign z2	=	c2 & c3;
	
	//Y= 0.25* /X/ + 0.5
		FPGreatEq u5 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h00000000),      // >=0     b.b
		.q      (c4)       //      q.q
		);
		
		
		FPLess u6 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (xAbs),      //      a.a
		.b      (32'h3f800000),      //  <1    b.b
		.q      (c5)       //      q.q
	);
	
	assign z3	=	c4 & c5;	
	
	

	
	
//compute sigmoid output
		
	//Supply coeff inputs
	always@* begin
	
		CoeffAdd1_net		=	0;
		CoeffAdd2_net		=	0;
//							Y= 1 								|	|x| >= 5 																			0001															
//							Y= 0.03125* |x| + 0.84375 	|	2.375 =<|X| < 5	sign==1 => 0.15625	; sign==0	=>	0.84375		0010			
//							Y= O.125*|X|+O.625 			|	1 =<|X|<2.375 		sign==1 => 0.625 		; sign==0	=>	0.375			0100
//							Y= 0.25* |X| + 0.5 			|	O=<|X|< 1 			sign==1 => 0.5 		; sign==0	=>	0.5			1000
//							Y= 1-Y 							|	x<0 
		case({z3,z2,z1,z0})
		
			4'b0001: begin//							Y= 1 								|	|x| >= 5 
				CoeffAdd1_net		=	32'h3f800000;
				if(sign==1'b1)begin
					CoeffAdd2_net		=	32'hbf800000;
				end
				else begin
					CoeffAdd2_net		=	32'h0;
				end
			end
		
			4'b0010: begin//Y= 0.03125* |x| + 0.84375 	|	2.375 =<|X| < 5
				
				if(sign==1'b1)begin
					CoeffAdd2_net		=	32'h3e200000;
					CoeffAdd1_net		=	{1'b1,xAbs[30:26],xAbs[25 : 23] - 3'b101,xAbs[22:0]};
				end
				else begin
					CoeffAdd1_net		=	{1'b0,xAbs[30:26],xAbs[25 : 23] - 3'b101,xAbs[22:0]};
					CoeffAdd2_net		=	32'h3f580000;
				end
			end
			
			
			4'b0100: begin//Y= O.125*|X|+O.625 			|	1 =<|X|<2.375
				if(sign==1'b1)begin
					CoeffAdd2_net		=	32'h3f200000;
					CoeffAdd1_net		=	{1'b1,xAbs[30:26],xAbs[25 : 23] - 3'b101,xAbs[22:0]};
				end
				else	begin
					CoeffAdd1_net		=	{1'b0,xAbs[30:26],xAbs[25 : 23] - 3'b011,xAbs[22:0]};
					CoeffAdd2_net		=	32'h3ec00000;
				end
			end
			
			
			4'b1000			: begin//Y= 0.25* |X| + 0.5 			|	O=<|X|< 1 
				CoeffAdd1_net		=	xAbs[25 : 23] - 3'b010;
				if(sign==1'b1)begin
					CoeffAdd1_net		=	{1'b1,xAbs[30:26],xAbs[25 : 23] - 3'b010,xAbs[22:0]};
					CoeffAdd2_net		=	32'h3f000000;
					
				end
				else begin
					CoeffAdd1_net		=	{1'b0,xAbs[30:26],xAbs[25 : 23] - 3'b010,xAbs[22:0]};
					CoeffAdd2_net		=	32'h3f000000;
				end
			end
		
		endcase
		
	end
	
	
	//register inputs
	always@(posedge clk)begin
		CoeffAdd1	<=	CoeffAdd1_net;
		CoeffAdd2	<=	CoeffAdd2_net;
	end



	


endmodule
