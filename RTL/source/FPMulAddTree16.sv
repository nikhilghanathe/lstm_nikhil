/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    18/9/2017 
// Design Name:    signed_mult_acc
// Module Name:    Signed multiplication 
// Project Name:   MATRIX_MUL
// Target Devices: Stratix V
// Tool versions: 15.1.1
// Description: Signed multiplication of 16 inputs which outputs the accumulated result
//
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

`include "CustomTypes.sv"


module SignedMul #(parameter INPUT_SIZE=16, parameter WIDTH=12) (rst, clk, dinA, dinB , mult_acc);



input clk,rst;

input signed [WIDTH-1:0] dinA [INPUT_SIZE-1:0];
input signed [WIDTH-1:0] dinB [INPUT_SIZE-1:0];

output signed[2*WIDTH+3:0] mult_acc; //1 extra bit of addition overflow



logic signed [2*WIDTH:0] s_mult_acc1,s_mult_acc2,s_mult_acc3,s_mult_acc4,s_mult_acc5,s_mult_acc6,s_mult_acc7,s_mult_acc8; //1 extra bit of addition overflow
logic signed [2*WIDTH+1:0] s_acc1, s_acc2, s_acc3, s_acc4; //1 extra bit of addition overflow
logic signed [2*WIDTH+2:0] s_add1, s_add2; //1 extra bit of addition overflow
logic signed [2*WIDTH+3:0] s_mult_acc; //1 extra bit of addition overflow

//Multiply-acculmulate
always@(posedge clk or posedge rst) begin
	if(rst==1'b1) begin
		s_mult_acc1 <= {2*WIDTH+1{1'b0}};
		s_mult_acc2 <= {2*WIDTH+1{1'b0}};
		s_mult_acc3 <= {2*WIDTH+1{1'b0}};
		s_mult_acc4 <= {2*WIDTH+1{1'b0}};
		s_mult_acc5 <= {2*WIDTH+1{1'b0}};
		s_mult_acc6 <= {2*WIDTH+1{1'b0}};
		s_mult_acc7 <= {2*WIDTH+1{1'b0}};
		s_mult_acc8 <= {2*WIDTH+1{1'b0}};
		
		s_acc1 		<= {2*WIDTH+2{1'b0}};
		s_acc2 		<= {2*WIDTH+2{1'b0}};
		s_acc3 		<= {2*WIDTH+2{1'b0}};
		s_acc4 		<= {2*WIDTH+2{1'b0}};
		
		s_add1 		<= {2*WIDTH+3{1'b0}};
		s_add2 		<= {2*WIDTH+3{1'b0}};
		
		
		s_mult_acc  <= {2*WIDTH+4{1'b0}};
	end
	else begin //construct 16 input mult-add tree with pipelining
		s_mult_acc1 <= (dinA[0] * dinB[0]) + (dinA[1] * dinB[1]);
		s_mult_acc2 <= (dinA[2] * dinB[1]) + (dinA[3] * dinB[3]);
		s_acc1		<=  s_mult_acc1 + s_mult_acc2;
		
		s_mult_acc3 <= (dinA[4] * dinB[4]) + (dinA[5] * dinB[5]);
		s_mult_acc4 <= (dinA[6] * dinB[6]) + (dinA[7] * dinB[7]);
		s_acc2		<=  s_mult_acc3 + s_mult_acc4;
		
		s_mult_acc5 <= (dinA[8] * dinB[8]) + (dinA[9] * dinB[9]);
		s_mult_acc6 <= (dinA[10] * dinB[10]) + (dinA[11] * dinB[11]);
		s_acc3		<=  s_mult_acc5 + s_mult_acc6;
		
		s_mult_acc7 <= (dinA[12] * dinB[12]) + (dinA[13] * dinB[13]);
		s_mult_acc8 <= (dinA[14] * dinB[14]) + (dinA[15] * dinB[15]);
		s_acc4		<=  s_mult_acc7 + s_mult_acc8;
		
		s_add1 		<= s_acc1 + s_acc2;
		s_add2 		<= s_acc3 + s_acc4;
		
		s_mult_acc  <= s_add1 + s_add2;
		
		
	end

end


assign mult_acc = s_mult_acc;
endmodule