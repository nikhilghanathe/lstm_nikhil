`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	MSRI	
// Engineer: 	NP Ghanathe	
// 
// Create Date: 1/18/2017 
// Design Name: BoundeLeakyReLU
// Module Name: BoundedLeakyyReLU
// Project Name: LSTM
// Target Devices: Arria10
// Tool Versions: 
// Description: 	Compute leaky relu output which has it positive output bounded by a value ('1' here) and small negative slope which is given by alpha parameter
// 
// Dependencies: All underlying modules
// 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
//							
//						
// 
//////////////////////////////////////////////////////////////////////////////////
`include "CustomTypes.sv"

module BoundedLeakyReLU #(parameter WIDTH=DATA_WIDTH, parameter UPPER_BOUND=1, parameter ALPHA=32'h3e4ccccd/*0.2*/)(clk,rst, x,y,go,done);

input 							clk, rst, go;
input [WIDTH-1:0]				x;
output logic [WIDTH-1:0]	y;
output logic 					done;

//signals
logic								sign,z0;
logic [WIDTH-1:0]				MulOut;

assign sign 	=	x[WIDTH-1];

	FpMult u0 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (x),      //      a.a
		.b      (ALPHA),      // >=5     b.b
		.q      (MulOut)       //      q.q
	);
	
	
	
	
	FpGreat u1 (
		.clk    (clk),    //    clk.clk
		.areset (rst), // areset.reset
		.a      (x),      //      a.a
		.b      (32'h3f800000),      // >=5     b.b
		.q      (z0)       //      q.q
	);

	
assign y	=	sign==1'b1	?	MulOut	:	z0==1'b1	? 32'h3f800000 : x ;




endmodule