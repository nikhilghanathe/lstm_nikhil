/////////////////////////////////////////////////////////////////////////////////
// Company: MSRI
// Engineer: N P Ghanathe
// 
// Create Date:    17/9/2017 
// Design Name:    LUTRAM_SIPO
// Module Name:    LUTRAM_SIPO 
// Project Name:   LSTM
// Target Devices: Stratix V
// Tool versions:  15.1.1
// Description: 	LUT memory which takes serial values and addr and outputs all the contents of the RAM
// Dependencies: all underlying modules
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

module LUTRAM_SIPO #(parameter WIDTH=32, parameter SIZE=32)
					(clk,rst,CE,SerialIn,ParallelOut);

input 							clk,rst,CE,clear;
input [WIDTH-1:0] 			serialIn;
output logic [WIDTH-1:0] 	ParallelOut [SIZE-1:0];


//local paramamters
localparam ADDR_WIDTH = $clog2(SIZE);

//signals
logic [ADDR_WIDTH-1:0] addr;
	
//memory
logic [WIDTH-1:0] memory [SIZE-1:0];



//RAM
always@ (posedge clk) begin
	if(clear==1'b1)begin
		addr	<=	{ADDR_WIDTH{1'b0}};
	end
	else if(CE==1'b1) begin
		addr			 <= 	addr + 1'b1;
		memory[addr] <=	serial_in;
	end
end


assign ParallelOut=memory;

endmodule