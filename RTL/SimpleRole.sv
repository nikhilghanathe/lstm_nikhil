/*
Name: SimpleRole.sv
Description: DRAM loopback test. Writes data to DRAM and reads it back before looping back through PCIe.

Copyright (c) Microsoft Corporation
 
All rights reserved. 
 
MIT License
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the ""Software""), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
 

import ShellTypes::*;
import SL3Types::*;
`include "source/CustomTypes.sv"

typedef struct packed {
    //logic [31:0] size;
    logic        last;
    logic [15:0] slot;
    logic [3:0]  pad;
} JobContext;


module SimpleRole
(
    // User clock and reset
    input                               clk,
    input                               rst, 

    // Simplified Memory interface
    output MemReq                       mem_reqs        [1:0],
    input                               mem_req_grants  [1:0],
    input MemResp                       mem_resps       [1:0],
    output                              mem_resp_grants [1:0],

    // PCIe Slot DMA interface
    input PCIEPacket                    pcie_packet_in,
    output                              pcie_full_out,

    output PCIEPacket                   pcie_packet_out,
    input                               pcie_grant_in,

    // Soft register interface
    input SoftRegReq                    softreg_req,
    output SoftRegResp                  softreg_resp,
    
    // SerialLite III interface
    output SL3DataInterface             sl_tx_out           [3:0],
    input                               sl_tx_full_in       [3:0],
    output SL3OOBInterface              sl_tx_oob_out       [3:0],
    input                               sl_tx_oob_full_in   [3:0],

    input SL3DataInterface              sl_rx_in            [3:0],
    output                              sl_rx_grant_out     [3:0],
    input SL3OOBInterface               sl_rx_oob_in        [3:0],
    output                              sl_rx_oob_grant_out [3:0]
);
    
     reg  [63:0]          control_status_reg0,control_status_reg1,control_status_reg2;
     wire [2:0]             accel_state;
    // Disable SL3 links
    genvar i;
    generate
        for(i = 0; i < 4; i=i+1) begin: disableSL3
            assign sl_tx_out[i] = '{valid: 1'b0, data: 128'b0, last: 1'b0};
            assign sl_tx_oob_out[i] = '{valid: 1'b0, data: 15'b0};
            
            assign sl_rx_grant_out[i] = 1'b0;
            assign sl_rx_oob_grant_out[i] = 1'b0;
        end
    endgenerate
    
    // Set up DRAM interleaver
    DramInterleaverConfig dramConfig;
    MemReq  mem_interleaved_req;
    wire    mem_interleaved_req_grant;
    MemResp mem_interleaved_resp;
    reg     mem_interleaved_resp_grant;
    /*
    assign mem_reqs[0] = mem_interleaved_req;
    assign mem_interleaved_req_grant = mem_req_grants[0];
    assign mem_interleaved_resp = mem_resps[0];
    assign mem_resp_grants[0] = mem_interleaved_resp_grant;
    
    // Disable channel 1 for now
    assign mem_reqs[1] = '{valid: 1'b0, isWrite: 1'b0, addr: 64'b0, data: 512'b0};
    assign mem_resp_grants[1] = 1'b0;
    */
    
    DramInterleaver interleaver
    (
    // User clock and reset
    .clk(clk),
    .rst(rst),
    
    // Config
    .config_in(dramConfig),
    
    // Input interface
    .mem_req_in(mem_interleaved_req),
    .mem_req_grant_out(mem_interleaved_req_grant),
    .mem_resp_out(mem_interleaved_resp),
    .mem_resp_grant_in(mem_interleaved_resp_grant),
    
    // Output interfaces
    .mem_req_c0_out(mem_reqs[0]),
    .mem_req_grant_c0_in(mem_req_grants[0]),
    .mem_resp_c0_in(mem_resps[0]),
    .mem_resp_grant_c0_out(mem_resp_grants[0]),
    
    .mem_req_c1_out(mem_reqs[1]),
    .mem_req_grant_c1_in(mem_req_grants[1]),
    .mem_resp_c1_in(mem_resps[1]),
    .mem_resp_grant_c1_out(mem_resp_grants[1])
    );
    
//   soft_regs soft_registers
//
//   (
//   .clk (clk), 
//  .rst (~rst),
//   .address_in (softreg_req.addr),
//   .data_in (softreg_req.data),
//   .req_in (softreg_req.valid),
//   .rd_wr (softreg_req.isWrite),              //rd - 0, wr - 1
//
//   .state    (accel_state),
//   .read_data (softreg_resp.data), 
//   .cpl_val (softreg_resp.valid)
//
//   );

logic [63:0]                            slv_reg0, slv_reg1, slv_reg2, slv_reg3 ,slv_reg4, slv_reg5,slv_reg6,slv_reg7,slv_reg8,slv_reg9, slv_reg10, slv_reg11 ;






    //-----------------------------------------------------------------
    //PCIe Input requests
    //-----------------------------------------------------------------
    reg        [1:0]                    input_packets_cnt,output_packets_cnt;
    wire       [UMI_DATA_WIDTH-1:0]     request_data;
    reg        [PCIE_DATA_WIDTH-1:0]    pcie_data_out;
    reg        [PCIE_DATA_WIDTH-1:0]    pcie_in_data_r,pcie_in_data_rr,pcie_in_data_rrr;
    wire                                request_enq;
    
    logic[UMI_DATA_WIDTH-1:0]           request_in;
    wire                                request_full;
    logic[UMI_DATA_WIDTH-1:0]           request_out;
    wire                                request_empty;
    reg                                 request_deq;

    logic                               go, done, clear;


    
    //count the number of incoming packets
     always @(posedge clk)
        if (rst)        input_packets_cnt <= 2'b00;
        else            input_packets_cnt   <=  pcie_packet_in.last     ?   2'b00:
                                                pcie_packet_in.valid    ?   input_packets_cnt + 1'b1    : input_packets_cnt;
    //passthrough the data when valid==1                                            
     always @(posedge clk)
        if (rst)        pcie_in_data_r  <= {128{1'b0}};
        else            pcie_in_data_r  <=  (pcie_packet_in.valid)  ?   pcie_packet_in.data : pcie_in_data_r;
        
     //shift reg for 4 packets of 128 bits each   
     always @(posedge clk)
        {pcie_in_data_rrr,pcie_in_data_rr}  <= {pcie_in_data_rr,pcie_in_data_r};
     
     //data sent to dram
     assign request_data = {pcie_packet_in.data,pcie_in_data_r,pcie_in_data_rr,pcie_in_data_rrr};
     
     //enq signal into FIFO
    assign request_enq = (input_packets_cnt == 2'b11) && request_full==1'b0;

    //data-in for FIFO
    //assign request_in = '{valid: pcie_packet_in.valid, data: request_data, slot: pcie_packet_in.slot, pad: pcie_packet_in.pad, last: pcie_packet_in.last};
    assign request_in = request_data;
    //assign request_in = '{valid: pcie_packet_in.valid, data: request_data, slot: pcie_packet_in.slot, pad: pcie_packet_in.pad, last: request_enq};
    

    




logic [13:0] usedw, usedw_req;

    //pcie -> accumulated FIFO
    //4 packets pf 128bits each are packed and enqued in this FIFO. The output: request_out is sent to DRAM
    FIFO
    #(
        .WIDTH                  (UMI_DATA_WIDTH),
        .LOG_DEPTH              (13)
    )
    requests
    (
        .clock                  (clk),
        .reset_n                (~rst),
        .wrreq                  (request_enq),
        .data                   (request_in),
        .full                   (request_full),
        .q                      (request_out),
        .empty                  (request_empty),
        .rdreq                  (request_deq),
        .almost_full            (),
        .almost_empty           (),
        .usedw                  (usedw_req)
    );
    



    reg        jobRespQ_enq;
    JobContext jobRespQ_in;
    wire       jobRespQ_full;
    JobContext jobRespQ_out;
    wire       jobRespQ_empty;
    reg        jobRespQ_deq;
    logic      jobRespQ_almostfull;
    
    
    // Needs to be sized to be at least as large as a 64KB job to prevent deadlock
    // 64KB/(128/8) = 4K entries
    // I'm sizing it to store 2 jobs (2^13 = 8K)
    

    //enques the jobs from pcie that need to sent back on the pcie lines
    FIFO
    #(
        .WIDTH                  ($bits(JobContext)),
        .LOG_DEPTH              (14)
    )
    JobRespQ
    (
        .clock                  (clk),
        .reset_n                (~rst),
        .wrreq                  (jobRespQ_enq),
        .data                   (jobRespQ_in),
        .full                   (jobRespQ_full),
        .q                      (jobRespQ_out),
        .empty                  (jobRespQ_empty),
        .rdreq                  (jobRespQ_deq),
        .almost_full            (jobRespQ_almostfull),
        .almost_empty           (),
        .usedw                  (usedw)
    );
    
    assign jobRespQ_enq = pcie_packet_in.valid && !jobRespQ_full;
    assign jobRespQ_in = '{last: pcie_packet_in.last, slot: pcie_packet_in.slot, pad: pcie_packet_in.pad};
   //used to signal fpga cannot accept any more packets 
    assign pcie_full_out = jobRespQ_almostfull;
    


    // Stream from PCIe queue to DRAM
    
    typedef enum {
        INIT = 0,
        WRITE=1,
        COMPLETE_WRITE=2,
        READ=3,
        DONE = 4
    } FSMState;
    
    FSMState state;
    FSMState next_state;
    reg[63:0] writeAddr;
    reg[63:0] next_writeAddr;
    reg[63:0] readAddr;
    reg[63:0] next_readAddr;
    reg[31:0] jobSize;
    reg[31:0] next_jobSize;
   
    wire [511:0] test_wire;
     
    //FIFO for Data from DRAM
    wire dram_input_buffer0_full,dram_input_buffer0_empty,dram_input_buffer0_rden;
    wire [UMI_DATA_WIDTH-1:0] dram_input_buffer0_out;
    logic dram_input_buffer0_wen;
    logic [$clog2(JOBSIZE)-1:0] pcie_last_count, pcie_job_last_count, jobEnqCount, jobDeqCount, memCount;



     
    
    always@* begin
        next_state = state;
        next_writeAddr = writeAddr;
        next_readAddr = readAddr;
        next_jobSize = jobSize;
        
        mem_interleaved_req = '{valid: 0, isWrite: 1'b0, addr: 64'b0, data: 512'b0};
        
        request_deq = 1'b0;
        
        // jobRespQ_enq = 1'b0;
        // jobRespQ_in = '{last: 1'b0, slot: 16'b0, pad: 4'b0};

        done = 0;
        clear = 0;
        case(state)
            INIT:begin
                clear = 1;
                if(go==1'b1)begin
                    next_state = WRITE;
                end
            end

            WRITE: begin// write the data after packing of 4 packets to DRAM from 'requests' FIFO; Also enque the jobs from pcie
                
                if(!request_empty) begin
                    mem_interleaved_req = '{valid: 1'b1, isWrite: 1'b1, addr: writeAddr, data: request_out};
                    
                    if(mem_interleaved_req_grant==1'b1) begin
                        next_writeAddr = writeAddr + 64'd64;
                        next_jobSize = jobSize + 32'd1;
                        request_deq = 1;
                        
                        if(DEBUG==1) $display("In the WRITE state and writeAddr = %d and jobSize = %d\n", writeAddr, jobSize);
                        
                    end
                end

                if(pcie_packet_in.last==1'b1) begin
                    next_state = COMPLETE_WRITE;
                end
            end


            COMPLETE_WRITE:begin
                if(!request_empty) begin
                    mem_interleaved_req = '{valid: 1'b1, isWrite: 1'b1, addr: writeAddr, data: request_out};
                    if(mem_interleaved_req_grant==1'b1) begin
                        next_writeAddr = writeAddr + 64'd64;
                        next_jobSize = jobSize + 32'd1;
                        request_deq = 1;
                        next_state = READ;
                            
                        if(DEBUG==1) $display("In the COMPLETEWRITE state and writeAddr = %d and jobSize = %d\n", writeAddr, jobSize);
                    
                    end
                end
            end
            
            READ:begin
                mem_interleaved_req = '{valid: 1'b1, isWrite: 1'b0, addr: readAddr, data: 512'b0};
                
                if(mem_interleaved_req_grant==1'b1) begin
                    next_readAddr = readAddr + 64'd64;
                    next_jobSize = jobSize - 32'd1;

                    if(DEBUG==1) $display("In the READ state and readAddr = %d and jobSize = %d\n", readAddr, jobSize);
                    
                    if(pcie_last_count==JOBSIZE)begin
                        next_state = DONE;
                    end
                    else if(next_jobSize == 32'd0) begin
                        next_state = WRITE;
                    end
                end
            end

            DONE:begin
                done = 1;
                next_state = INIT;
            end
        
            
        endcase
    end
    



//Update next variables
    always@(posedge clk) begin
        if(rst) begin
            state <= WRITE;
            writeAddr <= 64'b0;
            readAddr <= 64'b0;
            jobSize <= 32'b0;
        end
        else begin
            state <= next_state;
            writeAddr <= next_writeAddr;
            readAddr <= next_readAddr;
            jobSize <= next_jobSize;
        end
    end
    


    // Wait for reads from DRAM to return and write out to PCIe
    always @(posedge clk)
        if (rst)    output_packets_cnt  <=  2'b00;
        else        output_packets_cnt  <=  pcie_grant_in   ?   output_packets_cnt + 1'b1   : output_packets_cnt;

    always @(*)
        case(output_packets_cnt)
                2'b00: pcie_data_out <= dram_input_buffer0_out[127:0];
                2'b01: pcie_data_out <= dram_input_buffer0_out[255:128];
                2'b10: pcie_data_out <= dram_input_buffer0_out[383:256];
                2'b11: pcie_data_out <= dram_input_buffer0_out[511:384];
                default: pcie_data_out <= dram_input_buffer0_out[127:0];
        endcase
     

     //unpack data from dram and send to pcie
    always@* begin
        jobRespQ_deq = 1'b0;
        
        pcie_packet_out = '{valid: 1'b0, data: 512'b0, slot: 16'b0, pad: 4'b0, last: 1'b0};

        if(dram_input_buffer0_empty==1'b0) begin
            pcie_packet_out = '{valid: 1'b1, data: pcie_data_out, slot: jobRespQ_out.slot, pad: jobRespQ_out.pad, last: jobRespQ_out.last};

            if(DEBUG==1) $display("in sending back down pcie_grant_in= %d  and dram_input_buffer0_empty=%d and dram_input_buffer0_full =%d and usedw_pcie =%d\n",pcie_grant_in, dram_input_buffer0_empty, dram_input_buffer0_full, usedw_pcie);
            
            if(pcie_grant_in==1'b1) begin
                jobRespQ_deq = 1'b1;
            end
        end
    end
    
     
logic [13:0] usedw_pcie;


assign dram_input_buffer0_rden = ~dram_input_buffer0_empty & (output_packets_cnt == 2'b11) ;
assign dram_input_buffer0_wen = mem_interleaved_resp.valid & !dram_input_buffer0_full;

//FIFO to store the data read form DRAM. DataOut is unpacked and srent to pcie
    FIFO
    #(
        .WIDTH                  (UMI_DATA_WIDTH),
        .LOG_DEPTH              (13)
    )
    dram_input_buffer0
    (
        .clock                  (clk),
        .reset_n                (~rst),
        .wrreq                  (dram_input_buffer0_wen),
        .data                   (mem_interleaved_resp.data),
        .full                   (dram_input_buffer0_full),
        .q                      (dram_input_buffer0_out),
        .empty                  (dram_input_buffer0_empty),
        .rdreq                  (dram_input_buffer0_rden),
        .almost_full            (),
        .almost_empty           (),
        .usedw                  (usedw_pcie)
    );

    assign mem_interleaved_resp_grant = ~dram_input_buffer0_full & mem_interleaved_resp.valid;




logic [63:0] packetInLoss, packetOutLoss, jobFullCount;
logic inLossClear, outLossClear;

//update pcie_lastcount
counter_user #(.SIZE(JOBSIZE)) U_LAST_COUNT (.clk(clk), .rst(clear), .en(pcie_packet_in.last), .dout(pcie_last_count)) ;

//debug counters
counter_user #(.SIZE(JOBSIZE)) U_JOB_LAST_COUNT (.clk(clk), .rst(clear), .en(jobRespQ_out.last), .dout(pcie_job_last_count)) ;
counter_user #(.SIZE(JOBSIZE)) U_JOB_ENQ_COUNT (.clk(clk), .rst(clear), .en(jobRespQ_enq), .dout(jobEnqCount)) ;
counter_user #(.SIZE(JOBSIZE)) U_JOB_DEQ_COUNT (.clk(clk), .rst(clear), .en(jobRespQ_deq), .dout(jobDeqCount)) ;
counter_user #(.SIZE(JOBSIZE)) U_MEM_GRANT_COUNT (.clk(clk), .rst(clear), .en(mem_interleaved_req_grant), .dout(memCount)) ;

counter_user #(.SIZE(JOBSIZE)) U_PACKIN_LOSS_COUNT (.clk(clk), .rst(clear | inLossClear), .en(pcie_packet_in.valid), .dout(packetInLoss)) ;
counter_user #(.SIZE(JOBSIZE)) U_PACKOUT_LOSS_COUNT (.clk(clk), .rst(clear | outLossClear), .en(jobRespQ_deq), .dout(packetOutLoss)) ;

assign inLossClear = packetInLoss==1024;
assign outLossClear = packetOutLoss==1024;

counter_user #(.SIZE(JOBSIZE)) U_JOBFULL_COUNT (.clk(clk), .rst(clear), .en(jobRespQ_almostfull), .dout(jobFullCount)) ;



//Softreg Interface
//softreg write SW -> FPGA
always@(posedge clk)begin
    if(rst==1'b1)begin
        slv_reg0 <= 0;
        //slv_reg1 <= 0;
        // slv_reg2 <= 0;
        // slv_reg3 <= 0;
        // slv_reg4 <= 0;
    end
    else if(softreg_req.valid && softreg_req.isWrite) begin
        case(softreg_req.addr)
            32'd0:begin
                slv_reg0 <= softreg_req.data;
            end

            // 32'd1:begin
            //     slv_reg1 <= softreg_req.data;
            // end

            // 32'd2:begin
            //     slv_reg2 <= softreg_req.data;
            // end

            // 32'd3:begin
            //     slv_reg3 <= softreg_req.data;
            // end

            // 32'd4:begin
            //     slv_reg4 <= softreg_req.data;
            // end

            default:begin
                slv_reg0 <= slv_reg0;
                //slv_reg1 <= slv_reg1;
                // slv_reg2 <= slv_reg2;
                // slv_reg3 <= slv_reg3;
                // slv_reg4 <= slv_reg4;
            end // default:

        endcase
    end
end

     
    // Softreg config
    always@* begin
//        softreg_resp = '{valid:1'b0, data: 64'b0};
        dramConfig = '{valid: 1'b0, mode: CHAN0_ONLY};
        softreg_resp = '{valid: 1'b0, data: 64'h0};
       // Read softreg values
       if(softreg_req.valid && !softreg_req.isWrite) begin
           case(softreg_req.addr)
                32'd0:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg0};
                end

                32'd1:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg1};
                end

                32'd2:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg2};
                end

                32'd3:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg3};
                end

                32'd4:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg4};
                end

                32'd5:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg5};
                end

                32'd6:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg6};
                end

                32'd7:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg7};
                end

                32'd8:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg8};
                end

                32'd9:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg9};
                end

                32'd10:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg10};
                end
                
                32'd11:begin
                    softreg_resp = '{valid: 1'b1, data: slv_reg11};
                end    

               default: softreg_resp = '{valid: 1'b1, data: 64'd0};
           endcase
       end
        
        // Control FSM state
        if(softreg_req.valid && softreg_req.isWrite) begin
            case(softreg_req.addr)
                32'd600: dramConfig = '{valid: 1'b1, mode: CHAN0_ONLY};
                32'd700: dramConfig = '{valid: 1'b1, mode: CHAN1_ONLY};
                32'd800: dramConfig = '{valid: 1'b1, mode: INTERLEAVED};
                default: dramConfig = '{valid: 1'b0, mode: CHAN0_ONLY};
            endcase
        end
    end
    

//assign go and done signals to/from soft regs
    assign go =slv_reg0[0];
    
    //fpga softreg write process
    always@(posedge clk)begin
        if(rst)begin
            slv_reg1 <= 0;
            slv_reg2 <= 0;
            slv_reg3 <= 0;
            slv_reg4 <= 0;
            slv_reg5 <= 0;
            slv_reg6 <= 0;
            slv_reg7 <= 0;
            slv_reg8 <= 0;
            slv_reg9 <= 0;
            slv_reg10 <= 0;
            slv_reg11 <= 0;
        end
        else begin
            if(go | done)
                slv_reg1 <= done;

            slv_reg2 <= pcie_last_count;

            slv_reg3 <= jobSize;

            slv_reg4 <= pcie_job_last_count;

            slv_reg5 <= jobEnqCount;

            slv_reg6 <= jobDeqCount; 

            slv_reg7 <= memCount;

            slv_reg8 <= writeAddr;

            slv_reg9 <= readAddr;

            slv_reg10 <= packetInLoss;

            slv_reg11 <= jobFullCount;

            //slv_reg12 <= jobFullCount;
        
        end
    end


     //macc module for testing purposes
    
reg [63:0]      macc_output_acc;
wire[63:0]      macc_output;
wire            macc_output_val;
wire            macc_enable = 1'b1; 
wire            macc_clear = 1'b0;
//macc #(
//    .OP_0_WIDTH  (32),//`PRECISION_OP,
//    .OP_1_WIDTH  (32),//`PRECISION_OP,
//    .ACC_WIDTH   (64),//`PRECISION_ACC,
//    .OUT_WIDTH  (64),//`PRECISION_OP,
//    .TYPE ("FIXED_POINT"),
//    .FRAC_BITS (16),//`PRECISION_FRAC,
//    .INT_BITS (15)//`PRECISION_OP - 1 - `PRECISION_FRAC
//) macc_test (
//  .clk (clk),
//  .reset (rst),
//  .enable (macc_enable),
//  .clear (macc_clear),
//  .op_code (3'b010),
//  .op_0 (dram_input_buffer0_out[31:0]),
//  .op_1 (dram_input_buffer0_out[63:32]),
//  .op_add (macc_output_acc),
//  .out (macc_output),
//  .out_val (macc_output_val)
//);
   //debug block
always@(posedge clk)begin
    if(DEBUG==1)begin
        if(pcie_grant_in==1)
            $display("grantedma\n");
        if(jobRespQ_out.last==1'b1)$display("joblast\n");
        if(jobRespQ_enq==1'b1)$display("jobenq\n");
        if(pcie_packet_in.last==1'b1)$display("pcielast\n");
        if(request_enq==1'b1) $display("requestenq\n");
        if(request_deq==1'b1) $display("requestdeq\n");
        if(pcie_packet_in.valid==1'b1)begin
            $display("herema\n");
            $display("pcie_packet_in.slot = %d and pcie_packet_in.data = %d\n",pcie_packet_in.slot, pcie_packet_in.data);
        end

        $display("pcie_packet_in.data = %x\n",pcie_packet_in.data);
        $display("pcie_in_data_r = %x\n",pcie_in_data_r);
        $display("pcie_in_data_rr = %x\n",pcie_in_data_rr);
        $display("pcie_in_data_rrr = %x\n",pcie_in_data_rrr);

        $display("request_data = %x\n",request_data);

        $display("input cnt  = %x and request_enq = %x and request_in = %x \n",input_packets_cnt, request_enq, request_in );

        $display("in sending back special pcie_grant_in= %d  and dram_input_buffer0_empty=%d and dram_input_buffer0_full =%d and usedw_pcie =%d and mem_interleaved_resp.valid=%d and mem_interleaved_resp_grant=%d and dram_input_buffer0_wen=%d  and dram_input_buffer0_rden=%d\n",pcie_grant_in, dram_input_buffer0_empty, dram_input_buffer0_full, usedw_pcie, mem_interleaved_resp.valid, mem_interleaved_resp_grant,dram_input_buffer0_wen,dram_input_buffer0_rden);        

        $display("go = %d and done = %d\n",go,done);        
    end
    if(state==INIT)
        $display("go = %d and done = %d\n and state = %d",go,done, state);        

    if(state==DONE)
        $display("go = %d and done = %d\n and state = %d",go,done, state);        

end   


endmodule

