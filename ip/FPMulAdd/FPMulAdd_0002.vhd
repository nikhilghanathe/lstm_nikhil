-- ------------------------------------------------------------------------- 
-- Altera DSP Builder Advanced Flow Tools Release Version 15.1
-- Quartus Prime development tool and MATLAB/Simulink Interface
-- 
-- Legal Notice: Copyright 2015 Altera Corporation.  All rights reserved.
-- Your use of  Altera  Corporation's design tools,  logic functions and other
-- software and tools,  and its AMPP  partner logic functions, and  any output
-- files  any of the  foregoing  device programming or simulation files),  and
-- any associated  documentation or information are expressly subject  to  the
-- terms and conditions  of the Altera Program License Subscription Agreement,
-- Altera  MegaCore  Function  License  Agreement, or other applicable license
-- agreement,  including,  without limitation,  that your use  is for the sole
-- purpose of  programming  logic  devices  manufactured by Altera and sold by
-- Altera or its authorized  distributors.  Please  refer  to  the  applicable
-- agreement for further details.
-- ---------------------------------------------------------------------------

-- VHDL created from FPMulAdd_0002
-- VHDL created on Thu Dec 06 05:10:55 2018


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.MATH_REAL.all;
use std.TextIO.all;
use work.dspba_library_package.all;

LIBRARY altera_mf;
USE altera_mf.altera_mf_components.all;
LIBRARY altera_lnsim;
USE altera_lnsim.altera_lnsim_components.altera_syncram;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity FPMulAdd_0002 is
    port (
        a : in std_logic_vector(31 downto 0);  -- float32_m23
        b : in std_logic_vector(31 downto 0);  -- float32_m23
        c : in std_logic_vector(31 downto 0);  -- float32_m23
        q : out std_logic_vector(31 downto 0);  -- float32_m23
        clk : in std_logic;
        areset : in std_logic
    );
end FPMulAdd_0002;

architecture normal of FPMulAdd_0002 is

    attribute altera_attribute : string;
    attribute altera_attribute of normal : architecture is "-name PHYSICAL_SYNTHESIS_REGISTER_DUPLICATION ON; -name AUTO_SHIFT_REGISTER_RECOGNITION OFF; -name MESSAGE_DISABLE 10036; -name MESSAGE_DISABLE 10037; -name MESSAGE_DISABLE 14130; -name MESSAGE_DISABLE 14320; -name MESSAGE_DISABLE 15400; -name MESSAGE_DISABLE 14130; -name MESSAGE_DISABLE 10036; -name MESSAGE_DISABLE 12020; -name MESSAGE_DISABLE 12030; -name MESSAGE_DISABLE 12010; -name MESSAGE_DISABLE 12110; -name MESSAGE_DISABLE 14320; -name MESSAGE_DISABLE 13410; -name MESSAGE_DISABLE 113007";
    
    signal GND_q : STD_LOGIC_VECTOR (0 downto 0);
    signal VCC_q : STD_LOGIC_VECTOR (0 downto 0);
    signal cstAllOWE_uid13_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (7 downto 0);
    signal cstZeroWF_uid14_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (22 downto 0);
    signal cstAllZWE_uid15_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_x_uid18_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_x_uid18_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_x_uid18_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal excZ_x_uid18_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expXIsMax_uid19_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal expXIsMax_uid19_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal expXIsMax_uid19_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal expXIsMax_uid19_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid20_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (22 downto 0);
    signal fracXIsZero_uid20_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal fracXIsZero_uid20_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid20_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excZ_y_uid32_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_y_uid32_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_y_uid32_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal excZ_y_uid32_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expXIsMax_uid33_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal expXIsMax_uid33_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal expXIsMax_uid33_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal expXIsMax_uid33_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid34_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (22 downto 0);
    signal fracXIsZero_uid34_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal fracXIsZero_uid34_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid34_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expSum_uid47_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (8 downto 0);
    signal expSum_uid47_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (8 downto 0);
    signal expSum_uid47_p_uid6_fpMultAddTest_o : STD_LOGIC_VECTOR (8 downto 0);
    signal expSum_uid47_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (8 downto 0);
    signal biasInc_uid48_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (9 downto 0);
    signal signR_uid51_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal signR_uid51_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal signR_uid51_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal signR_uid51_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal roundBitDetectionConstant_uid66_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (2 downto 0);
    signal roundBit_uid68_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal roundBit_uid68_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal roundBit_uid68_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal oneFracRPostExc2_uid95_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (22 downto 0);
    signal signRPostExc_uid105_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal signRPostExc_uid105_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal signRPostExc_uid105_p_uid6_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal signRPostExc_uid105_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid127_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (22 downto 0);
    signal fracXIsZero_uid127_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal fracXIsZero_uid127_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid127_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_aSig_uid130_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_aSig_uid130_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_aSig_uid130_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_aSig_uid130_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_bSig_uid143_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_bSig_uid143_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_bSig_uid143_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_bSig_uid143_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_bSig_uid144_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_bSig_uid144_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_bSig_uid144_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_bSig_uid144_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_bSig_uid147_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_bSig_uid147_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_bSig_uid147_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_bSig_uid147_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal cWFP2_uid163_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (4 downto 0);
    signal padConst_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (24 downto 0);
    signal zocst_uid178_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (1 downto 0);
    signal cAmA_uid188_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (4 downto 0);
    signal oneCST_uid192_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (7 downto 0);
    signal cRBit_uid201_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (4 downto 0);
    signal roundBit_uid203_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal roundBit_uid203_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal roundBit_uid203_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal wEP2AllOwE_uid207_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (9 downto 0);
    signal wEP2AllZ_uid214_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (9 downto 0);
    signal regInputs_uid221_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal regInputs_uid221_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal regInputs_uid221_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal regInputs_uid221_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signRInfRZRReg_uid240_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal signRInfRZRReg_uid240_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal signRInfRZRReg_uid240_r_uid7_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal signRInfRZRReg_uid240_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal signRInfRZRReg_uid240_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signRPostExc_uid242_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal signRPostExc_uid242_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal signRPostExc_uid242_r_uid7_fpMultAddTest_q_i : STD_LOGIC_VECTOR (0 downto 0);
    signal signRPostExc_uid242_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal zs_uid256_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (15 downto 0);
    signal mO_uid259_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (3 downto 0);
    signal zs_uid270_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (3 downto 0);
    signal zs_uid276_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (1 downto 0);
    signal wIntCst_uid289_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (5 downto 0);
    signal rightShiftStage0Idx2Pad32_uid295_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (31 downto 0);
    signal rightShiftStage0Idx3Pad48_uid298_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (47 downto 0);
    signal rightShiftStage1Idx3Pad12_uid309_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (11 downto 0);
    signal rightShiftStage2Idx3Pad3_uid320_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (2 downto 0);
    signal zeroOutCst_uid324_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal leftShiftStage0Idx3Pad24_uid335_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (23 downto 0);
    signal leftShiftStage1Idx3Pad6_uid346_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (5 downto 0);
    type prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_a_type is array(0 to 0) of UNSIGNED(23 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_a0 : prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_a_type;
    attribute preserve : boolean;
    attribute preserve of prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_a0 : signal is true;
    type prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_c_type is array(0 to 0) of UNSIGNED(23 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_c0 : prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_c_type;
    attribute preserve of prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_c0 : signal is true;
    type prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_p_type is array(0 to 0) of UNSIGNED(47 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_p : prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_p_type;
    type prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_u_type is array(0 to 0) of UNSIGNED(47 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_u : prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_u_type;
    type prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_w_type is array(0 to 0) of UNSIGNED(47 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_w : prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_w_type;
    type prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_x_type is array(0 to 0) of UNSIGNED(47 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_x : prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_x_type;
    type prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_y_type is array(0 to 0) of UNSIGNED(47 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_y : prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_y_type;
    type prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_s_type is array(0 to 0) of UNSIGNED(47 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_s : prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_s_type;
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_qq : STD_LOGIC_VECTOR (47 downto 0);
    signal prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_q : STD_LOGIC_VECTOR (47 downto 0);
    signal redist0_q : STD_LOGIC_VECTOR (1 downto 0);
    signal redist1_q : STD_LOGIC_VECTOR (1 downto 0);
    signal redist2_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist3_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist4_q : STD_LOGIC_VECTOR (11 downto 0);
    signal redist5_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist6_q : STD_LOGIC_VECTOR (2 downto 0);
    signal redist7_q : STD_LOGIC_VECTOR (7 downto 0);
    signal redist8_q : STD_LOGIC_VECTOR (22 downto 0);
    signal redist9_q : STD_LOGIC_VECTOR (33 downto 0);
    signal redist10_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist11_q : STD_LOGIC_VECTOR (27 downto 0);
    signal redist12_q : STD_LOGIC_VECTOR (26 downto 0);
    signal redist13_q : STD_LOGIC_VECTOR (25 downto 0);
    signal redist14_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist15_q : STD_LOGIC_VECTOR (22 downto 0);
    signal redist16_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist17_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist18_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist19_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist20_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist21_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist22_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist23_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist24_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist25_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist26_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist27_q : STD_LOGIC_VECTOR (22 downto 0);
    signal redist28_q : STD_LOGIC_VECTOR (7 downto 0);
    signal redist29_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist30_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist31_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist32_q : STD_LOGIC_VECTOR (22 downto 0);
    signal redist33_q : STD_LOGIC_VECTOR (7 downto 0);
    signal redist34_q : STD_LOGIC_VECTOR (7 downto 0);
    signal redist35_q : STD_LOGIC_VECTOR (2 downto 0);
    signal redist36_q : STD_LOGIC_VECTOR (7 downto 0);
    signal redist37_q : STD_LOGIC_VECTOR (22 downto 0);
    signal redist38_q : STD_LOGIC_VECTOR (34 downto 0);
    signal redist39_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist40_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist41_q : STD_LOGIC_VECTOR (8 downto 0);
    signal redist42_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist43_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist44_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist45_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist46_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist47_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_mem_reset0 : std_logic;
    signal redist48_mem_ia : STD_LOGIC_VECTOR (31 downto 0);
    signal redist48_mem_aa : STD_LOGIC_VECTOR (1 downto 0);
    signal redist48_mem_ab : STD_LOGIC_VECTOR (1 downto 0);
    signal redist48_mem_iq : STD_LOGIC_VECTOR (31 downto 0);
    signal redist48_mem_q : STD_LOGIC_VECTOR (31 downto 0);
    signal redist48_rdcnt_q : STD_LOGIC_VECTOR (1 downto 0);
    signal redist48_rdcnt_i : UNSIGNED (1 downto 0);
    attribute preserve of redist48_rdcnt_i : signal is true;
    signal redist48_rdcnt_eq : std_logic;
    attribute preserve of redist48_rdcnt_eq : signal is true;
    signal redist48_wraddr_q : STD_LOGIC_VECTOR (1 downto 0);
    signal redist48_mem_top_q : STD_LOGIC_VECTOR (2 downto 0);
    signal redist48_cmpReg_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_sticky_ena_q : STD_LOGIC_VECTOR (0 downto 0);
    attribute preserve of redist48_sticky_ena_q : signal is true;
    signal roundBitAndNormalizationOp_uid71_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (25 downto 0);
    signal fracBAddOp_uid182_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (26 downto 0);
    signal fracXIsNotZero_uid21_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsNotZero_uid21_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_x_uid22_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_x_uid22_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_x_uid22_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_x_uid23_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_x_uid23_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_x_uid23_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid24_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid24_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid25_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid25_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_x_uid26_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_x_uid26_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_x_uid26_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsNotZero_uid35_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsNotZero_uid35_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_y_uid36_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_y_uid36_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_y_uid36_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_y_uid37_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_y_uid37_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_y_uid37_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid38_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid38_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid39_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid39_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_y_uid40_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_y_uid40_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_y_uid40_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expSumMBias_uid49_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (11 downto 0);
    signal expSumMBias_uid49_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (11 downto 0);
    signal expSumMBias_uid49_p_uid6_fpMultAddTest_o : STD_LOGIC_VECTOR (11 downto 0);
    signal expSumMBias_uid49_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (10 downto 0);
    signal expFracRPostRounding_uid72_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (36 downto 0);
    signal expFracRPostRounding_uid72_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (36 downto 0);
    signal expFracRPostRounding_uid72_p_uid6_fpMultAddTest_o : STD_LOGIC_VECTOR (36 downto 0);
    signal expFracRPostRounding_uid72_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (35 downto 0);
    signal excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excXZAndExcYR_uid80_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excXZAndExcYR_uid80_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excXZAndExcYR_uid80_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excYZAndExcXR_uid81_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excYZAndExcXR_uid81_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excYZAndExcXR_uid81_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excXIAndExcYI_uid84_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excXIAndExcYI_uid84_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excXIAndExcYI_uid84_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excXRAndExcYI_uid85_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excXRAndExcYI_uid85_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excXRAndExcYI_uid85_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excYRAndExcXI_uid86_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excYRAndExcXI_uid86_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excYRAndExcXI_uid86_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excYZAndExcXI_uid89_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excYZAndExcXI_uid89_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excYZAndExcXI_uid89_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excXZAndExcYI_uid90_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excXZAndExcYI_uid90_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excXZAndExcYI_uid90_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal ZeroTimesInf_uid91_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal ZeroTimesInf_uid91_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal ZeroTimesInf_uid91_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid92_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid92_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid92_p_uid6_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid92_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excREnc_uid94_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (1 downto 0);
    signal fracRPostExc_uid98_p_uid6_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal fracRPostExc_uid98_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (22 downto 0);
    signal expRPostExc_uid103_p_uid6_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal expRPostExc_uid103_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (7 downto 0);
    signal invExcRNaN_uid104_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invExcRNaN_uid104_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expXIsMax_uid126_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal expXIsMax_uid126_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal expXIsMax_uid126_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsNotZero_uid128_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsNotZero_uid128_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_aSig_uid129_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_aSig_uid129_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_aSig_uid129_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid131_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid131_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid132_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid132_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_aSig_uid133_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_aSig_uid133_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_aSig_uid133_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expXIsMax_uid140_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal expXIsMax_uid140_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal expXIsMax_uid140_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid141_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (22 downto 0);
    signal fracXIsZero_uid141_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal fracXIsZero_uid141_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsNotZero_uid142_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsNotZero_uid142_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid145_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid145_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid146_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid146_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal effSub_uid154_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal effSub_uid154_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal effSub_uid154_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracBz_uid158_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal fracBz_uid158_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (22 downto 0);
    signal expAmExpB_uid162_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (8 downto 0);
    signal expAmExpB_uid162_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (8 downto 0);
    signal expAmExpB_uid162_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (8 downto 0);
    signal expAmExpB_uid162_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (8 downto 0);
    signal shiftedOut_uid165_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (11 downto 0);
    signal shiftedOut_uid165_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (11 downto 0);
    signal shiftedOut_uid165_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (11 downto 0);
    signal shiftedOut_uid165_r_uid7_fpMultAddTest_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal shiftedOut_uid165_r_uid7_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal iShiftedOut_uid169_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal iShiftedOut_uid169_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (22 downto 0);
    signal cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invCmpEQ_stickyBits_cZwF_uid174_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invCmpEQ_stickyBits_cZwF_uid174_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invSticky_uid175_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invSticky_uid175_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal effSubInvSticky_uid176_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal effSubInvSticky_uid176_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal effSubInvSticky_uid176_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (26 downto 0);
    signal fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (26 downto 0);
    signal fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (26 downto 0);
    signal expInc_uid193_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (8 downto 0);
    signal expInc_uid193_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (8 downto 0);
    signal expInc_uid193_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (8 downto 0);
    signal expInc_uid193_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (8 downto 0);
    signal rndExpFrac_uid206_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (34 downto 0);
    signal rndExpFrac_uid206_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (34 downto 0);
    signal rndExpFrac_uid206_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (34 downto 0);
    signal rndExpFrac_uid206_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (34 downto 0);
    signal excRNaN2_uid227_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN2_uid227_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN2_uid227_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excAIBISub_uid228_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excAIBISub_uid228_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excAIBISub_uid228_r_uid7_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal excAIBISub_uid228_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid229_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid229_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid229_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excREnc_uid231_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (1 downto 0);
    signal sigBBInf_uid234_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal sigBBInf_uid234_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal sigBBInf_uid234_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal sigAAInf_uid235_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal sigAAInf_uid235_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal sigAAInf_uid235_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signRInf_uid236_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal signRInf_uid236_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal signRInf_uid236_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_d : STD_LOGIC_VECTOR (0 downto 0);
    signal excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excBZARSigA_uid238_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excBZARSigA_uid238_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excBZARSigA_uid238_r_uid7_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal excBZARSigA_uid238_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signRZero_uid239_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal signRZero_uid239_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal signRZero_uid239_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invExcRNaN_uid241_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invExcRNaN_uid241_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracRPostExc_uid246_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal fracRPostExc_uid246_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (22 downto 0);
    signal expRPostExc_uid250_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal expRPostExc_uid250_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (7 downto 0);
    signal vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (1 downto 0);
    signal vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (1 downto 0);
    signal shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (11 downto 0);
    signal shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (11 downto 0);
    signal shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (11 downto 0);
    signal shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_n : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_cmp_a : STD_LOGIC_VECTOR (2 downto 0);
    signal redist48_cmp_b : STD_LOGIC_VECTOR (2 downto 0);
    signal redist48_cmp_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_notEnable_a : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_notEnable_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_nor_a : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_nor_b : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_nor_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_enaAnd_a : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_enaAnd_b : STD_LOGIC_VECTOR (0 downto 0);
    signal redist48_enaAnd_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expX_uid9_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal expX_uid9_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal signX_uid11_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal signX_uid11_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal frac_x_uid17_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal frac_x_uid17_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal expY_uid10_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal expY_uid10_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal signY_uid12_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal signY_uid12_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal frac_y_uid31_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal frac_y_uid31_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal R_uid106_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (31 downto 0);
    signal fracAAddOp_uid179_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (26 downto 0);
    signal R_uid251_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (31 downto 0);
    signal leftShiftStage0Idx2_uid334_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal normalizeBit_uid52_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (47 downto 0);
    signal normalizeBit_uid52_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal fracRPostNormHigh_uid54_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (46 downto 0);
    signal fracRPostNormHigh_uid54_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (23 downto 0);
    signal fracRPostNormLow_uid55_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (45 downto 0);
    signal fracRPostNormLow_uid55_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (23 downto 0);
    signal stickyRange_uid57_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (21 downto 0);
    signal stickyRange_uid57_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (21 downto 0);
    signal extraStickyBitOfProd_uid58_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (22 downto 0);
    signal extraStickyBitOfProd_uid58_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal leftShiftStage0Idx1Rng8_uid330_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (19 downto 0);
    signal leftShiftStage0Idx1Rng8_uid330_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (19 downto 0);
    signal leftShiftStage0Idx3Rng24_uid336_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (3 downto 0);
    signal leftShiftStage0Idx3Rng24_uid336_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (3 downto 0);
    signal fracGRS_uid186_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal expFracY_uid109_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal expFracY_uid109_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (30 downto 0);
    signal sigY_uid111_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal sigY_uid111_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal fracY_uid112_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal fracY_uid112_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal expY_uid113_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal expY_uid113_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal fracRPreExc_uid73_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (23 downto 0);
    signal fracRPreExc_uid73_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal expRPreExcExt_uid74_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (35 downto 0);
    signal expRPreExcExt_uid74_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (11 downto 0);
    signal oFracB_uid161_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (23 downto 0);
    signal rightShiftStageSel5Dto4_uid300_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (5 downto 0);
    signal rightShiftStageSel5Dto4_uid300_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal rightShiftStageSel3Dto2_uid311_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (3 downto 0);
    signal rightShiftStageSel3Dto2_uid311_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal rightShiftStageSel1Dto0_uid322_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (1 downto 0);
    signal rightShiftStageSel1Dto0_uid322_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal fracAddResult_uid184_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (27 downto 0);
    signal fracAddResult_uid184_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (27 downto 0);
    signal fracAddResult_uid184_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (27 downto 0);
    signal fracAddResult_uid184_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal rndExp_uid208_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (33 downto 0);
    signal rndExp_uid208_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (9 downto 0);
    signal rndExpFracOvfBits_uid211_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (33 downto 0);
    signal rndExpFracOvfBits_uid211_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal rUdfExtraBit_uid217_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (33 downto 0);
    signal rUdfExtraBit_uid217_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal fracRPreExc_uid219_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (23 downto 0);
    signal fracRPreExc_uid219_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal expRPreExc_uid220_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal expRPreExc_uid220_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal rVStage_uid283_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (1 downto 0);
    signal rVStage_uid283_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal ofracX_uid43_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (23 downto 0);
    signal ofracY_uid46_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (23 downto 0);
    signal expFracX_uid108_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal expFracX_uid108_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (30 downto 0);
    signal fracRPostNorm_uid56_p_uid6_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal fracRPostNorm_uid56_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (23 downto 0);
    signal extraStickyBit_uid59_p_uid6_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal extraStickyBit_uid59_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal stickyExtendedRange_uid60_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (22 downto 0);
    signal leftShiftStage0Idx1_uid331_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal leftShiftStage0Idx3_uid337_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (27 downto 0);
    signal rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (15 downto 0);
    signal vStage_uid260_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (11 downto 0);
    signal vStage_uid260_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (11 downto 0);
    signal xGTEy_uid110_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (33 downto 0);
    signal xGTEy_uid110_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (33 downto 0);
    signal xGTEy_uid110_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (33 downto 0);
    signal xGTEy_uid110_r_uid7_fpMultAddTest_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal xGTEy_uid110_r_uid7_fpMultAddTest_n : STD_LOGIC_VECTOR (0 downto 0);
    signal ypn_uid114_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (31 downto 0);
    signal expRPreExc_uid75_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (7 downto 0);
    signal expRPreExc_uid75_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal expUdf_uid76_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (14 downto 0);
    signal expUdf_uid76_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (14 downto 0);
    signal expUdf_uid76_p_uid6_fpMultAddTest_o : STD_LOGIC_VECTOR (14 downto 0);
    signal expUdf_uid76_p_uid6_fpMultAddTest_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal expUdf_uid76_p_uid6_fpMultAddTest_n : STD_LOGIC_VECTOR (0 downto 0);
    signal expOvf_uid78_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (14 downto 0);
    signal expOvf_uid78_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (14 downto 0);
    signal expOvf_uid78_p_uid6_fpMultAddTest_o : STD_LOGIC_VECTOR (14 downto 0);
    signal expOvf_uid78_p_uid6_fpMultAddTest_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal expOvf_uid78_p_uid6_fpMultAddTest_n : STD_LOGIC_VECTOR (0 downto 0);
    signal rightPaddedIn_uid167_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal rangeFracAddResultMwfp3Dto0_uid185_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (26 downto 0);
    signal rangeFracAddResultMwfp3Dto0_uid185_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (26 downto 0);
    signal rOvfEQMax_uid209_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (9 downto 0);
    signal rOvfEQMax_uid209_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (9 downto 0);
    signal rOvfEQMax_uid209_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal rUdfEQMin_uid216_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (9 downto 0);
    signal rUdfEQMin_uid216_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (9 downto 0);
    signal rUdfEQMin_uid216_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal rOvfExtraBits_uid212_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (4 downto 0);
    signal rOvfExtraBits_uid212_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (4 downto 0);
    signal rOvfExtraBits_uid212_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (4 downto 0);
    signal rOvfExtraBits_uid212_r_uid7_fpMultAddTest_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal rOvfExtraBits_uid212_r_uid7_fpMultAddTest_n : STD_LOGIC_VECTOR (0 downto 0);
    signal rUdf_uid218_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal rUdf_uid218_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal rUdf_uid218_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracRPostNorm1dto0_uid64_p_uid6_fpMultAddTest_in : STD_LOGIC_VECTOR (1 downto 0);
    signal fracRPostNorm1dto0_uid64_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal expFracPreRound_uid69_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (34 downto 0);
    signal stickyRangeComparator_uid62_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (22 downto 0);
    signal stickyRangeComparator_uid62_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal stickyRangeComparator_uid62_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (15 downto 0);
    signal vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (15 downto 0);
    signal vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal cStage_uid261_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (15 downto 0);
    signal aSig_uid118_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal aSig_uid118_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (31 downto 0);
    signal bSig_uid119_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal bSig_uid119_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (31 downto 0);
    signal excZC3_uid82_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excZC3_uid82_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excZC3_uid82_p_uid6_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal excZC3_uid82_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal rightShiftStage0Idx1Rng16_uid291_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage0Idx1Rng16_uid291_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (32 downto 0);
    signal rightShiftStage0Idx2Rng32_uid294_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage0Idx2Rng32_uid294_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (16 downto 0);
    signal rightShiftStage0Idx3Rng48_uid297_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage0Idx3Rng48_uid297_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal rOvf_uid213_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal rOvf_uid213_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal rOvf_uid213_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excRZeroVInC_uid222_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (4 downto 0);
    signal r_uid285_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (4 downto 0);
    signal sticky_uid63_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal sticky_uid63_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (15 downto 0);
    signal exp_aSig_uid123_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (30 downto 0);
    signal exp_aSig_uid123_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal frac_aSig_uid124_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (22 downto 0);
    signal frac_aSig_uid124_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal sigA_uid152_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal sigA_uid152_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal exp_bSig_uid137_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (30 downto 0);
    signal exp_bSig_uid137_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal frac_bSig_uid138_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (22 downto 0);
    signal frac_bSig_uid138_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal sigB_uid153_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (31 downto 0);
    signal sigB_uid153_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excRZero_uid83_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excRZero_uid83_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excRZero_uid83_p_uid6_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal excRZero_uid83_p_uid6_fpMultAddTest_d : STD_LOGIC_VECTOR (0 downto 0);
    signal excRZero_uid83_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excRInf_uid88_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal excRInf_uid88_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal excRInf_uid88_p_uid6_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal excRInf_uid88_p_uid6_fpMultAddTest_d : STD_LOGIC_VECTOR (0 downto 0);
    signal excRInf_uid88_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal rightShiftStage0Idx1_uid293_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage0Idx2_uid296_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage0Idx3_uid299_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal rInfOvf_uid224_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal rInfOvf_uid224_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal rInfOvf_uid224_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excRZero_uid223_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal aMinusA_uid189_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (4 downto 0);
    signal aMinusA_uid189_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (4 downto 0);
    signal aMinusA_uid189_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expPostNorm_uid194_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (9 downto 0);
    signal expPostNorm_uid194_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (9 downto 0);
    signal expPostNorm_uid194_r_uid7_fpMultAddTest_o : STD_LOGIC_VECTOR (9 downto 0);
    signal expPostNorm_uid194_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (9 downto 0);
    signal leftShiftStageSel4Dto3_uid338_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (4 downto 0);
    signal leftShiftStageSel4Dto3_uid338_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStageSel2Dto1_uid349_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (2 downto 0);
    signal leftShiftStageSel2Dto1_uid349_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStageSel0Dto0_uid354_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (0 downto 0);
    signal leftShiftStageSel0Dto0_uid354_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal lrs_uid65_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (2 downto 0);
    signal rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (15 downto 0);
    signal rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal vStage_uid267_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (7 downto 0);
    signal vStage_uid267_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal concExc_uid93_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (2 downto 0);
    signal rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal excRInfVInC_uid225_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (5 downto 0);
    signal invAMinusA_uid232_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal invAMinusA_uid232_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_a : STD_LOGIC_VECTOR (2 downto 0);
    signal roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_b : STD_LOGIC_VECTOR (2 downto 0);
    signal roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (7 downto 0);
    signal vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (7 downto 0);
    signal vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (7 downto 0);
    signal rightShiftStage1Idx1Rng4_uid302_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage1Idx1Rng4_uid302_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (44 downto 0);
    signal rightShiftStage1Idx2Rng8_uid305_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage1Idx2Rng8_uid305_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (40 downto 0);
    signal rightShiftStage1Idx3Rng12_uid308_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage1Idx3Rng12_uid308_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (36 downto 0);
    signal excRInf_uid226_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signRReg_uid233_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (0 downto 0);
    signal signRReg_uid233_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal signRReg_uid233_r_uid7_fpMultAddTest_c : STD_LOGIC_VECTOR (0 downto 0);
    signal signRReg_uid233_r_uid7_fpMultAddTest_d : STD_LOGIC_VECTOR (0 downto 0);
    signal signRReg_uid233_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal leftShiftStage1Idx1Rng2_uid341_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (25 downto 0);
    signal leftShiftStage1Idx1Rng2_uid341_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (25 downto 0);
    signal leftShiftStage1Idx2Rng4_uid344_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (23 downto 0);
    signal leftShiftStage1Idx2Rng4_uid344_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (23 downto 0);
    signal leftShiftStage1Idx3Rng6_uid347_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (21 downto 0);
    signal leftShiftStage1Idx3Rng6_uid347_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (21 downto 0);
    signal rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (7 downto 0);
    signal rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (3 downto 0);
    signal vStage_uid273_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (3 downto 0);
    signal vStage_uid273_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (3 downto 0);
    signal rightShiftStage1Idx1_uid304_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage1Idx2_uid307_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage1Idx3_uid310_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal concExc_uid230_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (2 downto 0);
    signal leftShiftStage1Idx1_uid342_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal leftShiftStage1Idx2_uid345_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal leftShiftStage1Idx3_uid348_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (3 downto 0);
    signal vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (3 downto 0);
    signal vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (3 downto 0);
    signal rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal rVStage_uid277_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (3 downto 0);
    signal rVStage_uid277_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal vStage_uid279_lzCountVal_uid187_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (1 downto 0);
    signal vStage_uid279_lzCountVal_uid187_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (1 downto 0);
    signal rightShiftStage2Idx1Rng1_uid313_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage2Idx1Rng1_uid313_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (47 downto 0);
    signal rightShiftStage2Idx2Rng2_uid316_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage2Idx2Rng2_uid316_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (46 downto 0);
    signal rightShiftStage2Idx3Rng3_uid319_alignmentShifter_uid166_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage2Idx3Rng3_uid319_alignmentShifter_uid166_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (45 downto 0);
    signal leftShiftStage2Idx1Rng1_uid352_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (26 downto 0);
    signal leftShiftStage2Idx1Rng1_uid352_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (26 downto 0);
    signal rightShiftStage2Idx1_uid315_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage2Idx2_uid318_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal rightShiftStage2Idx3_uid321_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal leftShiftStage2Idx1_uid353_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (1 downto 0);
    signal rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (27 downto 0);
    signal r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_s : STD_LOGIC_VECTOR (0 downto 0);
    signal r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal fracPostNorm_uid191_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (27 downto 0);
    signal fracPostNorm_uid191_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (26 downto 0);
    signal Sticky0_uid195_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (0 downto 0);
    signal Sticky0_uid195_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal Sticky1_uid196_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (1 downto 0);
    signal Sticky1_uid196_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal Round_uid197_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (2 downto 0);
    signal Round_uid197_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal Guard_uid198_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (3 downto 0);
    signal Guard_uid198_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal LSB_uid199_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (4 downto 0);
    signal LSB_uid199_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (0 downto 0);
    signal alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (48 downto 0);
    signal alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (48 downto 0);
    signal alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (48 downto 0);
    signal fracPostNormRndRange_uid204_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (25 downto 0);
    signal fracPostNormRndRange_uid204_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (23 downto 0);
    signal rndBitCond_uid200_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (4 downto 0);
    signal stickyBits_uid171_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (22 downto 0);
    signal stickyBits_uid171_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (22 downto 0);
    signal alignFracBOpRange_uid180_r_uid7_fpMultAddTest_in : STD_LOGIC_VECTOR (48 downto 0);
    signal alignFracBOpRange_uid180_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (25 downto 0);
    signal expFracR_uid205_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (33 downto 0);
    signal rBi_uid202_r_uid7_fpMultAddTest_a : STD_LOGIC_VECTOR (4 downto 0);
    signal rBi_uid202_r_uid7_fpMultAddTest_b : STD_LOGIC_VECTOR (4 downto 0);
    signal rBi_uid202_r_uid7_fpMultAddTest_q : STD_LOGIC_VECTOR (0 downto 0);

begin


    -- cAmA_uid188_r_uid7_fpMultAddTest(CONSTANT,187)
    cAmA_uid188_r_uid7_fpMultAddTest_q <= "11100";

    -- zs_uid256_lzCountVal_uid187_r_uid7_fpMultAddTest(CONSTANT,255)
    zs_uid256_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "0000000000000000";

    -- redist48_notEnable(LOGICAL,412)
    redist48_notEnable_a <= VCC_q;
    redist48_notEnable_q <= not (redist48_notEnable_a);

    -- redist48_nor(LOGICAL,413)
    redist48_nor_a <= redist48_notEnable_q;
    redist48_nor_b <= redist48_sticky_ena_q;
    redist48_nor_q <= not (redist48_nor_a or redist48_nor_b);

    -- redist48_mem_top(CONSTANT,409)
    redist48_mem_top_q <= "010";

    -- redist48_cmp(LOGICAL,410)
    redist48_cmp_a <= redist48_mem_top_q;
    redist48_cmp_b <= STD_LOGIC_VECTOR("0" & redist48_rdcnt_q);
    redist48_cmp_q <= "1" WHEN redist48_cmp_a = redist48_cmp_b ELSE "0";

    -- redist48_cmpReg(REG,411)
    redist48_cmpReg: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            redist48_cmpReg_q <= "0";
        ELSIF (clk'EVENT AND clk = '1') THEN
            redist48_cmpReg_q <= STD_LOGIC_VECTOR(redist48_cmp_q);
        END IF;
    END PROCESS;

    -- redist48_sticky_ena(REG,414)
    redist48_sticky_ena: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            redist48_sticky_ena_q <= "0";
        ELSIF (clk'EVENT AND clk = '1') THEN
            IF (redist48_nor_q = "1") THEN
                redist48_sticky_ena_q <= STD_LOGIC_VECTOR(redist48_cmpReg_q);
            END IF;
        END IF;
    END PROCESS;

    -- redist48_enaAnd(LOGICAL,415)
    redist48_enaAnd_a <= redist48_sticky_ena_q;
    redist48_enaAnd_b <= VCC_q;
    redist48_enaAnd_q <= redist48_enaAnd_a and redist48_enaAnd_b;

    -- redist48_rdcnt(COUNTER,407)
    -- every=1, low=0, high=2, step=1, init=1
    redist48_rdcnt: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            redist48_rdcnt_i <= TO_UNSIGNED(1, 2);
            redist48_rdcnt_eq <= '0';
        ELSIF (clk'EVENT AND clk = '1') THEN
            IF (redist48_rdcnt_i = TO_UNSIGNED(1, 2)) THEN
                redist48_rdcnt_eq <= '1';
            ELSE
                redist48_rdcnt_eq <= '0';
            END IF;
            IF (redist48_rdcnt_eq = '1') THEN
                redist48_rdcnt_i <= redist48_rdcnt_i - 2;
            ELSE
                redist48_rdcnt_i <= redist48_rdcnt_i + 1;
            END IF;
        END IF;
    END PROCESS;
    redist48_rdcnt_q <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR(RESIZE(redist48_rdcnt_i, 2)));

    -- xIn(GPIN,3)@0

    -- redist48_wraddr(REG,408)
    redist48_wraddr: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            redist48_wraddr_q <= "00";
        ELSIF (clk'EVENT AND clk = '1') THEN
            redist48_wraddr_q <= STD_LOGIC_VECTOR(redist48_rdcnt_q);
        END IF;
    END PROCESS;

    -- redist48_mem(DUALMEM,406)
    redist48_mem_ia <= STD_LOGIC_VECTOR(c);
    redist48_mem_aa <= redist48_wraddr_q;
    redist48_mem_ab <= redist48_rdcnt_q;
    redist48_mem_reset0 <= areset;
    redist48_mem_dmem : altera_syncram
    GENERIC MAP (
        ram_block_type => "MLAB",
        operation_mode => "DUAL_PORT",
        width_a => 32,
        widthad_a => 2,
        numwords_a => 3,
        width_b => 32,
        widthad_b => 2,
        numwords_b => 3,
        lpm_type => "altera_syncram",
        width_byteena_a => 1,
        address_reg_b => "CLOCK0",
        indata_reg_b => "CLOCK0",
        rdcontrol_reg_b => "CLOCK0",
        byteena_reg_b => "CLOCK0",
        outdata_reg_b => "CLOCK1",
        outdata_aclr_b => "CLEAR1",
        clock_enable_input_a => "NORMAL",
        clock_enable_input_b => "NORMAL",
        clock_enable_output_b => "NORMAL",
        read_during_write_mode_mixed_ports => "DONT_CARE",
        power_up_uninitialized => "TRUE",
        intended_device_family => "Stratix V"
    )
    PORT MAP (
        clocken1 => redist48_enaAnd_q(0),
        clocken0 => VCC_q(0),
        clock0 => clk,
        aclr1 => redist48_mem_reset0,
        clock1 => clk,
        address_a => redist48_mem_aa,
        data_a => redist48_mem_ia,
        wren_a => VCC_q(0),
        address_b => redist48_mem_ab,
        q_b => redist48_mem_iq
    );
    redist48_mem_q <= redist48_mem_iq(31 downto 0);

    -- sigY_uid111_r_uid7_fpMultAddTest(BITSELECT,110)@4
    sigY_uid111_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(redist48_mem_q);
    sigY_uid111_r_uid7_fpMultAddTest_b <= sigY_uid111_r_uid7_fpMultAddTest_in(31 downto 31);

    -- expY_uid113_r_uid7_fpMultAddTest(BITSELECT,112)@4
    expY_uid113_r_uid7_fpMultAddTest_in <= redist48_mem_q;
    expY_uid113_r_uid7_fpMultAddTest_b <= expY_uid113_r_uid7_fpMultAddTest_in(30 downto 23);

    -- fracY_uid112_r_uid7_fpMultAddTest(BITSELECT,111)@4
    fracY_uid112_r_uid7_fpMultAddTest_in <= redist48_mem_q;
    fracY_uid112_r_uid7_fpMultAddTest_b <= fracY_uid112_r_uid7_fpMultAddTest_in(22 downto 0);

    -- ypn_uid114_r_uid7_fpMultAddTest(BITJOIN,113)@4
    ypn_uid114_r_uid7_fpMultAddTest_q <= sigY_uid111_r_uid7_fpMultAddTest_b & expY_uid113_r_uid7_fpMultAddTest_b & fracY_uid112_r_uid7_fpMultAddTest_b;

    -- frac_x_uid17_p_uid6_fpMultAddTest(BITSELECT,16)@0
    frac_x_uid17_p_uid6_fpMultAddTest_in <= a;
    frac_x_uid17_p_uid6_fpMultAddTest_b <= frac_x_uid17_p_uid6_fpMultAddTest_in(22 downto 0);

    -- cstZeroWF_uid14_p_uid6_fpMultAddTest(CONSTANT,13)
    cstZeroWF_uid14_p_uid6_fpMultAddTest_q <= "00000000000000000000000";

    -- fracXIsZero_uid20_p_uid6_fpMultAddTest(LOGICAL,19)@0
    fracXIsZero_uid20_p_uid6_fpMultAddTest_a <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
    fracXIsZero_uid20_p_uid6_fpMultAddTest_b <= frac_x_uid17_p_uid6_fpMultAddTest_b;
    fracXIsZero_uid20_p_uid6_fpMultAddTest_q_i <= "1" WHEN fracXIsZero_uid20_p_uid6_fpMultAddTest_a = fracXIsZero_uid20_p_uid6_fpMultAddTest_b ELSE "0";
    fracXIsZero_uid20_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracXIsZero_uid20_p_uid6_fpMultAddTest_q_i, xout => fracXIsZero_uid20_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist45(DELAY,402)
    redist45 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracXIsZero_uid20_p_uid6_fpMultAddTest_q, xout => redist45_q, clk => clk, aclr => areset );

    -- cstAllOWE_uid13_p_uid6_fpMultAddTest(CONSTANT,12)
    cstAllOWE_uid13_p_uid6_fpMultAddTest_q <= "11111111";

    -- expX_uid9_p_uid6_fpMultAddTest(BITSELECT,8)@0
    expX_uid9_p_uid6_fpMultAddTest_in <= a;
    expX_uid9_p_uid6_fpMultAddTest_b <= expX_uid9_p_uid6_fpMultAddTest_in(30 downto 23);

    -- expXIsMax_uid19_p_uid6_fpMultAddTest(LOGICAL,18)@0
    expXIsMax_uid19_p_uid6_fpMultAddTest_a <= expX_uid9_p_uid6_fpMultAddTest_b;
    expXIsMax_uid19_p_uid6_fpMultAddTest_b <= cstAllOWE_uid13_p_uid6_fpMultAddTest_q;
    expXIsMax_uid19_p_uid6_fpMultAddTest_q_i <= "1" WHEN expXIsMax_uid19_p_uid6_fpMultAddTest_a = expXIsMax_uid19_p_uid6_fpMultAddTest_b ELSE "0";
    expXIsMax_uid19_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => expXIsMax_uid19_p_uid6_fpMultAddTest_q_i, xout => expXIsMax_uid19_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist46(DELAY,403)
    redist46 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => expXIsMax_uid19_p_uid6_fpMultAddTest_q, xout => redist46_q, clk => clk, aclr => areset );

    -- excI_x_uid22_p_uid6_fpMultAddTest(LOGICAL,21)@3
    excI_x_uid22_p_uid6_fpMultAddTest_a <= redist46_q;
    excI_x_uid22_p_uid6_fpMultAddTest_b <= redist45_q;
    excI_x_uid22_p_uid6_fpMultAddTest_q <= excI_x_uid22_p_uid6_fpMultAddTest_a and excI_x_uid22_p_uid6_fpMultAddTest_b;

    -- cstAllZWE_uid15_p_uid6_fpMultAddTest(CONSTANT,14)
    cstAllZWE_uid15_p_uid6_fpMultAddTest_q <= "00000000";

    -- expY_uid10_p_uid6_fpMultAddTest(BITSELECT,9)@0
    expY_uid10_p_uid6_fpMultAddTest_in <= b;
    expY_uid10_p_uid6_fpMultAddTest_b <= expY_uid10_p_uid6_fpMultAddTest_in(30 downto 23);

    -- excZ_y_uid32_p_uid6_fpMultAddTest(LOGICAL,31)@0
    excZ_y_uid32_p_uid6_fpMultAddTest_a <= expY_uid10_p_uid6_fpMultAddTest_b;
    excZ_y_uid32_p_uid6_fpMultAddTest_b <= cstAllZWE_uid15_p_uid6_fpMultAddTest_q;
    excZ_y_uid32_p_uid6_fpMultAddTest_q_i <= "1" WHEN excZ_y_uid32_p_uid6_fpMultAddTest_a = excZ_y_uid32_p_uid6_fpMultAddTest_b ELSE "0";
    excZ_y_uid32_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excZ_y_uid32_p_uid6_fpMultAddTest_q_i, xout => excZ_y_uid32_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist44(DELAY,401)
    redist44 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => excZ_y_uid32_p_uid6_fpMultAddTest_q, xout => redist44_q, clk => clk, aclr => areset );

    -- excYZAndExcXI_uid89_p_uid6_fpMultAddTest(LOGICAL,88)@3
    excYZAndExcXI_uid89_p_uid6_fpMultAddTest_a <= redist44_q;
    excYZAndExcXI_uid89_p_uid6_fpMultAddTest_b <= excI_x_uid22_p_uid6_fpMultAddTest_q;
    excYZAndExcXI_uid89_p_uid6_fpMultAddTest_q <= excYZAndExcXI_uid89_p_uid6_fpMultAddTest_a and excYZAndExcXI_uid89_p_uid6_fpMultAddTest_b;

    -- frac_y_uid31_p_uid6_fpMultAddTest(BITSELECT,30)@0
    frac_y_uid31_p_uid6_fpMultAddTest_in <= b;
    frac_y_uid31_p_uid6_fpMultAddTest_b <= frac_y_uid31_p_uid6_fpMultAddTest_in(22 downto 0);

    -- fracXIsZero_uid34_p_uid6_fpMultAddTest(LOGICAL,33)@0
    fracXIsZero_uid34_p_uid6_fpMultAddTest_a <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
    fracXIsZero_uid34_p_uid6_fpMultAddTest_b <= frac_y_uid31_p_uid6_fpMultAddTest_b;
    fracXIsZero_uid34_p_uid6_fpMultAddTest_q_i <= "1" WHEN fracXIsZero_uid34_p_uid6_fpMultAddTest_a = fracXIsZero_uid34_p_uid6_fpMultAddTest_b ELSE "0";
    fracXIsZero_uid34_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracXIsZero_uid34_p_uid6_fpMultAddTest_q_i, xout => fracXIsZero_uid34_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist42(DELAY,399)
    redist42 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracXIsZero_uid34_p_uid6_fpMultAddTest_q, xout => redist42_q, clk => clk, aclr => areset );

    -- expXIsMax_uid33_p_uid6_fpMultAddTest(LOGICAL,32)@0
    expXIsMax_uid33_p_uid6_fpMultAddTest_a <= expY_uid10_p_uid6_fpMultAddTest_b;
    expXIsMax_uid33_p_uid6_fpMultAddTest_b <= cstAllOWE_uid13_p_uid6_fpMultAddTest_q;
    expXIsMax_uid33_p_uid6_fpMultAddTest_q_i <= "1" WHEN expXIsMax_uid33_p_uid6_fpMultAddTest_a = expXIsMax_uid33_p_uid6_fpMultAddTest_b ELSE "0";
    expXIsMax_uid33_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => expXIsMax_uid33_p_uid6_fpMultAddTest_q_i, xout => expXIsMax_uid33_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist43(DELAY,400)
    redist43 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => expXIsMax_uid33_p_uid6_fpMultAddTest_q, xout => redist43_q, clk => clk, aclr => areset );

    -- excI_y_uid36_p_uid6_fpMultAddTest(LOGICAL,35)@3
    excI_y_uid36_p_uid6_fpMultAddTest_a <= redist43_q;
    excI_y_uid36_p_uid6_fpMultAddTest_b <= redist42_q;
    excI_y_uid36_p_uid6_fpMultAddTest_q <= excI_y_uid36_p_uid6_fpMultAddTest_a and excI_y_uid36_p_uid6_fpMultAddTest_b;

    -- excZ_x_uid18_p_uid6_fpMultAddTest(LOGICAL,17)@0
    excZ_x_uid18_p_uid6_fpMultAddTest_a <= expX_uid9_p_uid6_fpMultAddTest_b;
    excZ_x_uid18_p_uid6_fpMultAddTest_b <= cstAllZWE_uid15_p_uid6_fpMultAddTest_q;
    excZ_x_uid18_p_uid6_fpMultAddTest_q_i <= "1" WHEN excZ_x_uid18_p_uid6_fpMultAddTest_a = excZ_x_uid18_p_uid6_fpMultAddTest_b ELSE "0";
    excZ_x_uid18_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excZ_x_uid18_p_uid6_fpMultAddTest_q_i, xout => excZ_x_uid18_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist47(DELAY,404)
    redist47 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => excZ_x_uid18_p_uid6_fpMultAddTest_q, xout => redist47_q, clk => clk, aclr => areset );

    -- excXZAndExcYI_uid90_p_uid6_fpMultAddTest(LOGICAL,89)@3
    excXZAndExcYI_uid90_p_uid6_fpMultAddTest_a <= redist47_q;
    excXZAndExcYI_uid90_p_uid6_fpMultAddTest_b <= excI_y_uid36_p_uid6_fpMultAddTest_q;
    excXZAndExcYI_uid90_p_uid6_fpMultAddTest_q <= excXZAndExcYI_uid90_p_uid6_fpMultAddTest_a and excXZAndExcYI_uid90_p_uid6_fpMultAddTest_b;

    -- ZeroTimesInf_uid91_p_uid6_fpMultAddTest(LOGICAL,90)@3
    ZeroTimesInf_uid91_p_uid6_fpMultAddTest_a <= excXZAndExcYI_uid90_p_uid6_fpMultAddTest_q;
    ZeroTimesInf_uid91_p_uid6_fpMultAddTest_b <= excYZAndExcXI_uid89_p_uid6_fpMultAddTest_q;
    ZeroTimesInf_uid91_p_uid6_fpMultAddTest_q <= ZeroTimesInf_uid91_p_uid6_fpMultAddTest_a or ZeroTimesInf_uid91_p_uid6_fpMultAddTest_b;

    -- fracXIsNotZero_uid35_p_uid6_fpMultAddTest(LOGICAL,34)@3
    fracXIsNotZero_uid35_p_uid6_fpMultAddTest_a <= redist42_q;
    fracXIsNotZero_uid35_p_uid6_fpMultAddTest_q <= not (fracXIsNotZero_uid35_p_uid6_fpMultAddTest_a);

    -- excN_y_uid37_p_uid6_fpMultAddTest(LOGICAL,36)@3
    excN_y_uid37_p_uid6_fpMultAddTest_a <= redist43_q;
    excN_y_uid37_p_uid6_fpMultAddTest_b <= fracXIsNotZero_uid35_p_uid6_fpMultAddTest_q;
    excN_y_uid37_p_uid6_fpMultAddTest_q <= excN_y_uid37_p_uid6_fpMultAddTest_a and excN_y_uid37_p_uid6_fpMultAddTest_b;

    -- fracXIsNotZero_uid21_p_uid6_fpMultAddTest(LOGICAL,20)@3
    fracXIsNotZero_uid21_p_uid6_fpMultAddTest_a <= redist45_q;
    fracXIsNotZero_uid21_p_uid6_fpMultAddTest_q <= not (fracXIsNotZero_uid21_p_uid6_fpMultAddTest_a);

    -- excN_x_uid23_p_uid6_fpMultAddTest(LOGICAL,22)@3
    excN_x_uid23_p_uid6_fpMultAddTest_a <= redist46_q;
    excN_x_uid23_p_uid6_fpMultAddTest_b <= fracXIsNotZero_uid21_p_uid6_fpMultAddTest_q;
    excN_x_uid23_p_uid6_fpMultAddTest_q <= excN_x_uid23_p_uid6_fpMultAddTest_a and excN_x_uid23_p_uid6_fpMultAddTest_b;

    -- excRNaN_uid92_p_uid6_fpMultAddTest(LOGICAL,91)@3
    excRNaN_uid92_p_uid6_fpMultAddTest_a <= excN_x_uid23_p_uid6_fpMultAddTest_q;
    excRNaN_uid92_p_uid6_fpMultAddTest_b <= excN_y_uid37_p_uid6_fpMultAddTest_q;
    excRNaN_uid92_p_uid6_fpMultAddTest_c <= ZeroTimesInf_uid91_p_uid6_fpMultAddTest_q;
    excRNaN_uid92_p_uid6_fpMultAddTest_q <= excRNaN_uid92_p_uid6_fpMultAddTest_a or excRNaN_uid92_p_uid6_fpMultAddTest_b or excRNaN_uid92_p_uid6_fpMultAddTest_c;

    -- invExcRNaN_uid104_p_uid6_fpMultAddTest(LOGICAL,103)@3
    invExcRNaN_uid104_p_uid6_fpMultAddTest_a <= excRNaN_uid92_p_uid6_fpMultAddTest_q;
    invExcRNaN_uid104_p_uid6_fpMultAddTest_q <= not (invExcRNaN_uid104_p_uid6_fpMultAddTest_a);

    -- signY_uid12_p_uid6_fpMultAddTest(BITSELECT,11)@0
    signY_uid12_p_uid6_fpMultAddTest_in <= STD_LOGIC_VECTOR(b);
    signY_uid12_p_uid6_fpMultAddTest_b <= signY_uid12_p_uid6_fpMultAddTest_in(31 downto 31);

    -- signX_uid11_p_uid6_fpMultAddTest(BITSELECT,10)@0
    signX_uid11_p_uid6_fpMultAddTest_in <= STD_LOGIC_VECTOR(a);
    signX_uid11_p_uid6_fpMultAddTest_b <= signX_uid11_p_uid6_fpMultAddTest_in(31 downto 31);

    -- signR_uid51_p_uid6_fpMultAddTest(LOGICAL,50)@0
    signR_uid51_p_uid6_fpMultAddTest_a <= signX_uid11_p_uid6_fpMultAddTest_b;
    signR_uid51_p_uid6_fpMultAddTest_b <= signY_uid12_p_uid6_fpMultAddTest_b;
    signR_uid51_p_uid6_fpMultAddTest_q_i <= signR_uid51_p_uid6_fpMultAddTest_a xor signR_uid51_p_uid6_fpMultAddTest_b;
    signR_uid51_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => signR_uid51_p_uid6_fpMultAddTest_q_i, xout => signR_uid51_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist40(DELAY,397)
    redist40 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => signR_uid51_p_uid6_fpMultAddTest_q, xout => redist40_q, clk => clk, aclr => areset );

    -- signRPostExc_uid105_p_uid6_fpMultAddTest(LOGICAL,104)@3
    signRPostExc_uid105_p_uid6_fpMultAddTest_a <= redist40_q;
    signRPostExc_uid105_p_uid6_fpMultAddTest_b <= invExcRNaN_uid104_p_uid6_fpMultAddTest_q;
    signRPostExc_uid105_p_uid6_fpMultAddTest_q_i <= signRPostExc_uid105_p_uid6_fpMultAddTest_a and signRPostExc_uid105_p_uid6_fpMultAddTest_b;
    signRPostExc_uid105_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => signRPostExc_uid105_p_uid6_fpMultAddTest_q_i, xout => signRPostExc_uid105_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- GND(CONSTANT,0)
    GND_q <= "0";

    -- ofracY_uid46_p_uid6_fpMultAddTest(BITJOIN,45)@0
    ofracY_uid46_p_uid6_fpMultAddTest_q <= VCC_q & frac_y_uid31_p_uid6_fpMultAddTest_b;

    -- ofracX_uid43_p_uid6_fpMultAddTest(BITJOIN,42)@0
    ofracX_uid43_p_uid6_fpMultAddTest_q <= VCC_q & frac_x_uid17_p_uid6_fpMultAddTest_b;

    -- prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma(CHAINMULTADD,356)@0
    prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_p(0) <= prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_a0(0) * prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_c0(0);
    prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_u(0) <= RESIZE(prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_p(0),48);
    prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_w(0) <= prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_u(0);
    prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_x(0) <= prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_w(0);
    prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_y(0) <= prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_x(0);
    prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_chainmultadd: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_a0 <= (others => (others => '0'));
            prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_c0 <= (others => (others => '0'));
            prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_s <= (others => (others => '0'));
        ELSIF (clk'EVENT AND clk = '1') THEN
            prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_a0(0) <= RESIZE(UNSIGNED(ofracX_uid43_p_uid6_fpMultAddTest_q),24);
            prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_c0(0) <= RESIZE(UNSIGNED(ofracY_uid46_p_uid6_fpMultAddTest_q),24);
            prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_s(0) <= prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_y(0);
        END IF;
    END PROCESS;
    prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_delay : dspba_delay
    GENERIC MAP ( width => 48, depth => 0, reset_kind => "ASYNC" )
    PORT MAP ( xin => STD_LOGIC_VECTOR(prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_s(0)(47 downto 0)), xout => prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_qq, clk => clk, aclr => areset );
    prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_q <= STD_LOGIC_VECTOR(prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_qq(47 downto 0));

    -- normalizeBit_uid52_p_uid6_fpMultAddTest(BITSELECT,51)@2
    normalizeBit_uid52_p_uid6_fpMultAddTest_in <= STD_LOGIC_VECTOR(prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_q);
    normalizeBit_uid52_p_uid6_fpMultAddTest_b <= normalizeBit_uid52_p_uid6_fpMultAddTest_in(47 downto 47);

    -- redist39(DELAY,396)
    redist39 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => normalizeBit_uid52_p_uid6_fpMultAddTest_b, xout => redist39_q, clk => clk, aclr => areset );

    -- roundBitDetectionConstant_uid66_p_uid6_fpMultAddTest(CONSTANT,65)
    roundBitDetectionConstant_uid66_p_uid6_fpMultAddTest_q <= "010";

    -- fracRPostNormHigh_uid54_p_uid6_fpMultAddTest(BITSELECT,53)@2
    fracRPostNormHigh_uid54_p_uid6_fpMultAddTest_in <= prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_q(46 downto 0);
    fracRPostNormHigh_uid54_p_uid6_fpMultAddTest_b <= fracRPostNormHigh_uid54_p_uid6_fpMultAddTest_in(46 downto 23);

    -- fracRPostNormLow_uid55_p_uid6_fpMultAddTest(BITSELECT,54)@2
    fracRPostNormLow_uid55_p_uid6_fpMultAddTest_in <= prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_q(45 downto 0);
    fracRPostNormLow_uid55_p_uid6_fpMultAddTest_b <= fracRPostNormLow_uid55_p_uid6_fpMultAddTest_in(45 downto 22);

    -- fracRPostNorm_uid56_p_uid6_fpMultAddTest(MUX,55)@2
    fracRPostNorm_uid56_p_uid6_fpMultAddTest_s <= normalizeBit_uid52_p_uid6_fpMultAddTest_b;
    fracRPostNorm_uid56_p_uid6_fpMultAddTest: PROCESS (fracRPostNorm_uid56_p_uid6_fpMultAddTest_s, fracRPostNormLow_uid55_p_uid6_fpMultAddTest_b, fracRPostNormHigh_uid54_p_uid6_fpMultAddTest_b)
    BEGIN
        CASE (fracRPostNorm_uid56_p_uid6_fpMultAddTest_s) IS
            WHEN "0" => fracRPostNorm_uid56_p_uid6_fpMultAddTest_q <= fracRPostNormLow_uid55_p_uid6_fpMultAddTest_b;
            WHEN "1" => fracRPostNorm_uid56_p_uid6_fpMultAddTest_q <= fracRPostNormHigh_uid54_p_uid6_fpMultAddTest_b;
            WHEN OTHERS => fracRPostNorm_uid56_p_uid6_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- fracRPostNorm1dto0_uid64_p_uid6_fpMultAddTest(BITSELECT,63)@2
    fracRPostNorm1dto0_uid64_p_uid6_fpMultAddTest_in <= fracRPostNorm_uid56_p_uid6_fpMultAddTest_q(1 downto 0);
    fracRPostNorm1dto0_uid64_p_uid6_fpMultAddTest_b <= fracRPostNorm1dto0_uid64_p_uid6_fpMultAddTest_in(1 downto 0);

    -- extraStickyBitOfProd_uid58_p_uid6_fpMultAddTest(BITSELECT,57)@2
    extraStickyBitOfProd_uid58_p_uid6_fpMultAddTest_in <= STD_LOGIC_VECTOR(prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_q(22 downto 0));
    extraStickyBitOfProd_uid58_p_uid6_fpMultAddTest_b <= extraStickyBitOfProd_uid58_p_uid6_fpMultAddTest_in(22 downto 22);

    -- extraStickyBit_uid59_p_uid6_fpMultAddTest(MUX,58)@2
    extraStickyBit_uid59_p_uid6_fpMultAddTest_s <= normalizeBit_uid52_p_uid6_fpMultAddTest_b;
    extraStickyBit_uid59_p_uid6_fpMultAddTest: PROCESS (extraStickyBit_uid59_p_uid6_fpMultAddTest_s, GND_q, extraStickyBitOfProd_uid58_p_uid6_fpMultAddTest_b)
    BEGIN
        CASE (extraStickyBit_uid59_p_uid6_fpMultAddTest_s) IS
            WHEN "0" => extraStickyBit_uid59_p_uid6_fpMultAddTest_q <= GND_q;
            WHEN "1" => extraStickyBit_uid59_p_uid6_fpMultAddTest_q <= extraStickyBitOfProd_uid58_p_uid6_fpMultAddTest_b;
            WHEN OTHERS => extraStickyBit_uid59_p_uid6_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- stickyRange_uid57_p_uid6_fpMultAddTest(BITSELECT,56)@2
    stickyRange_uid57_p_uid6_fpMultAddTest_in <= prodXY_uid253_prod_uid50_p_uid6_fpMultAddTest_cma_q(21 downto 0);
    stickyRange_uid57_p_uid6_fpMultAddTest_b <= stickyRange_uid57_p_uid6_fpMultAddTest_in(21 downto 0);

    -- stickyExtendedRange_uid60_p_uid6_fpMultAddTest(BITJOIN,59)@2
    stickyExtendedRange_uid60_p_uid6_fpMultAddTest_q <= extraStickyBit_uid59_p_uid6_fpMultAddTest_q & stickyRange_uid57_p_uid6_fpMultAddTest_b;

    -- stickyRangeComparator_uid62_p_uid6_fpMultAddTest(LOGICAL,61)@2
    stickyRangeComparator_uid62_p_uid6_fpMultAddTest_a <= stickyExtendedRange_uid60_p_uid6_fpMultAddTest_q;
    stickyRangeComparator_uid62_p_uid6_fpMultAddTest_b <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
    stickyRangeComparator_uid62_p_uid6_fpMultAddTest_q <= "1" WHEN stickyRangeComparator_uid62_p_uid6_fpMultAddTest_a = stickyRangeComparator_uid62_p_uid6_fpMultAddTest_b ELSE "0";

    -- sticky_uid63_p_uid6_fpMultAddTest(LOGICAL,62)@2
    sticky_uid63_p_uid6_fpMultAddTest_a <= stickyRangeComparator_uid62_p_uid6_fpMultAddTest_q;
    sticky_uid63_p_uid6_fpMultAddTest_q <= not (sticky_uid63_p_uid6_fpMultAddTest_a);

    -- lrs_uid65_p_uid6_fpMultAddTest(BITJOIN,64)@2
    lrs_uid65_p_uid6_fpMultAddTest_q <= fracRPostNorm1dto0_uid64_p_uid6_fpMultAddTest_b & sticky_uid63_p_uid6_fpMultAddTest_q;

    -- roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest(LOGICAL,66)@2
    roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_a <= lrs_uid65_p_uid6_fpMultAddTest_q;
    roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_b <= roundBitDetectionConstant_uid66_p_uid6_fpMultAddTest_q;
    roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_q <= "1" WHEN roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_a = roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_b ELSE "0";

    -- roundBit_uid68_p_uid6_fpMultAddTest(LOGICAL,67)@2
    roundBit_uid68_p_uid6_fpMultAddTest_a <= roundBitDetectionPattern_uid67_p_uid6_fpMultAddTest_q;
    roundBit_uid68_p_uid6_fpMultAddTest_q_i <= not (roundBit_uid68_p_uid6_fpMultAddTest_a);
    roundBit_uid68_p_uid6_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => roundBit_uid68_p_uid6_fpMultAddTest_q_i, xout => roundBit_uid68_p_uid6_fpMultAddTest_q, clk => clk, aclr => areset );

    -- roundBitAndNormalizationOp_uid71_p_uid6_fpMultAddTest(BITJOIN,70)@3
    roundBitAndNormalizationOp_uid71_p_uid6_fpMultAddTest_q <= GND_q & redist39_q & cstZeroWF_uid14_p_uid6_fpMultAddTest_q & roundBit_uid68_p_uid6_fpMultAddTest_q;

    -- biasInc_uid48_p_uid6_fpMultAddTest(CONSTANT,47)
    biasInc_uid48_p_uid6_fpMultAddTest_q <= "0001111111";

    -- expSum_uid47_p_uid6_fpMultAddTest(ADD,46)@0
    expSum_uid47_p_uid6_fpMultAddTest_a <= STD_LOGIC_VECTOR("0" & expX_uid9_p_uid6_fpMultAddTest_b);
    expSum_uid47_p_uid6_fpMultAddTest_b <= STD_LOGIC_VECTOR("0" & expY_uid10_p_uid6_fpMultAddTest_b);
    expSum_uid47_p_uid6_fpMultAddTest: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            expSum_uid47_p_uid6_fpMultAddTest_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            expSum_uid47_p_uid6_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(expSum_uid47_p_uid6_fpMultAddTest_a) + UNSIGNED(expSum_uid47_p_uid6_fpMultAddTest_b));
        END IF;
    END PROCESS;
    expSum_uid47_p_uid6_fpMultAddTest_q <= expSum_uid47_p_uid6_fpMultAddTest_o(8 downto 0);

    -- redist41(DELAY,398)
    redist41 : dspba_delay
    GENERIC MAP ( width => 9, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => expSum_uid47_p_uid6_fpMultAddTest_q, xout => redist41_q, clk => clk, aclr => areset );

    -- expSumMBias_uid49_p_uid6_fpMultAddTest(SUB,48)@2
    expSumMBias_uid49_p_uid6_fpMultAddTest_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR("0" & "00" & redist41_q));
    expSumMBias_uid49_p_uid6_fpMultAddTest_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((11 downto 10 => biasInc_uid48_p_uid6_fpMultAddTest_q(9)) & biasInc_uid48_p_uid6_fpMultAddTest_q));
    expSumMBias_uid49_p_uid6_fpMultAddTest_o <= STD_LOGIC_VECTOR(SIGNED(expSumMBias_uid49_p_uid6_fpMultAddTest_a) - SIGNED(expSumMBias_uid49_p_uid6_fpMultAddTest_b));
    expSumMBias_uid49_p_uid6_fpMultAddTest_q <= expSumMBias_uid49_p_uid6_fpMultAddTest_o(10 downto 0);

    -- expFracPreRound_uid69_p_uid6_fpMultAddTest(BITJOIN,68)@2
    expFracPreRound_uid69_p_uid6_fpMultAddTest_q <= expSumMBias_uid49_p_uid6_fpMultAddTest_q & fracRPostNorm_uid56_p_uid6_fpMultAddTest_q;

    -- redist38(DELAY,395)
    redist38 : dspba_delay
    GENERIC MAP ( width => 35, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => expFracPreRound_uid69_p_uid6_fpMultAddTest_q, xout => redist38_q, clk => clk, aclr => areset );

    -- expFracRPostRounding_uid72_p_uid6_fpMultAddTest(ADD,71)@3
    expFracRPostRounding_uid72_p_uid6_fpMultAddTest_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((36 downto 35 => redist38_q(34)) & redist38_q));
    expFracRPostRounding_uid72_p_uid6_fpMultAddTest_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR("0" & "0000000000" & roundBitAndNormalizationOp_uid71_p_uid6_fpMultAddTest_q));
    expFracRPostRounding_uid72_p_uid6_fpMultAddTest_o <= STD_LOGIC_VECTOR(SIGNED(expFracRPostRounding_uid72_p_uid6_fpMultAddTest_a) + SIGNED(expFracRPostRounding_uid72_p_uid6_fpMultAddTest_b));
    expFracRPostRounding_uid72_p_uid6_fpMultAddTest_q <= expFracRPostRounding_uid72_p_uid6_fpMultAddTest_o(35 downto 0);

    -- expRPreExcExt_uid74_p_uid6_fpMultAddTest(BITSELECT,73)@3
    expRPreExcExt_uid74_p_uid6_fpMultAddTest_in <= STD_LOGIC_VECTOR(expFracRPostRounding_uid72_p_uid6_fpMultAddTest_q);
    expRPreExcExt_uid74_p_uid6_fpMultAddTest_b <= expRPreExcExt_uid74_p_uid6_fpMultAddTest_in(35 downto 24);

    -- expRPreExc_uid75_p_uid6_fpMultAddTest(BITSELECT,74)@3
    expRPreExc_uid75_p_uid6_fpMultAddTest_in <= expRPreExcExt_uid74_p_uid6_fpMultAddTest_b(7 downto 0);
    expRPreExc_uid75_p_uid6_fpMultAddTest_b <= expRPreExc_uid75_p_uid6_fpMultAddTest_in(7 downto 0);

    -- redist36(DELAY,393)
    redist36 : dspba_delay
    GENERIC MAP ( width => 8, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => expRPreExc_uid75_p_uid6_fpMultAddTest_b, xout => redist36_q, clk => clk, aclr => areset );

    -- expOvf_uid78_p_uid6_fpMultAddTest(COMPARE,77)@3
    expOvf_uid78_p_uid6_fpMultAddTest_cin <= GND_q;
    expOvf_uid78_p_uid6_fpMultAddTest_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((13 downto 12 => expRPreExcExt_uid74_p_uid6_fpMultAddTest_b(11)) & expRPreExcExt_uid74_p_uid6_fpMultAddTest_b) & '0');
    expOvf_uid78_p_uid6_fpMultAddTest_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR("0" & "00000" & cstAllOWE_uid13_p_uid6_fpMultAddTest_q) & expOvf_uid78_p_uid6_fpMultAddTest_cin(0));
    expOvf_uid78_p_uid6_fpMultAddTest_o <= STD_LOGIC_VECTOR(SIGNED(expOvf_uid78_p_uid6_fpMultAddTest_a) - SIGNED(expOvf_uid78_p_uid6_fpMultAddTest_b));
    expOvf_uid78_p_uid6_fpMultAddTest_n(0) <= not (expOvf_uid78_p_uid6_fpMultAddTest_o(14));

    -- invExpXIsMax_uid38_p_uid6_fpMultAddTest(LOGICAL,37)@3
    invExpXIsMax_uid38_p_uid6_fpMultAddTest_a <= redist43_q;
    invExpXIsMax_uid38_p_uid6_fpMultAddTest_q <= not (invExpXIsMax_uid38_p_uid6_fpMultAddTest_a);

    -- InvExpXIsZero_uid39_p_uid6_fpMultAddTest(LOGICAL,38)@3
    InvExpXIsZero_uid39_p_uid6_fpMultAddTest_a <= redist44_q;
    InvExpXIsZero_uid39_p_uid6_fpMultAddTest_q <= not (InvExpXIsZero_uid39_p_uid6_fpMultAddTest_a);

    -- excR_y_uid40_p_uid6_fpMultAddTest(LOGICAL,39)@3
    excR_y_uid40_p_uid6_fpMultAddTest_a <= InvExpXIsZero_uid39_p_uid6_fpMultAddTest_q;
    excR_y_uid40_p_uid6_fpMultAddTest_b <= invExpXIsMax_uid38_p_uid6_fpMultAddTest_q;
    excR_y_uid40_p_uid6_fpMultAddTest_q <= excR_y_uid40_p_uid6_fpMultAddTest_a and excR_y_uid40_p_uid6_fpMultAddTest_b;

    -- invExpXIsMax_uid24_p_uid6_fpMultAddTest(LOGICAL,23)@3
    invExpXIsMax_uid24_p_uid6_fpMultAddTest_a <= redist46_q;
    invExpXIsMax_uid24_p_uid6_fpMultAddTest_q <= not (invExpXIsMax_uid24_p_uid6_fpMultAddTest_a);

    -- InvExpXIsZero_uid25_p_uid6_fpMultAddTest(LOGICAL,24)@3
    InvExpXIsZero_uid25_p_uid6_fpMultAddTest_a <= redist47_q;
    InvExpXIsZero_uid25_p_uid6_fpMultAddTest_q <= not (InvExpXIsZero_uid25_p_uid6_fpMultAddTest_a);

    -- excR_x_uid26_p_uid6_fpMultAddTest(LOGICAL,25)@3
    excR_x_uid26_p_uid6_fpMultAddTest_a <= InvExpXIsZero_uid25_p_uid6_fpMultAddTest_q;
    excR_x_uid26_p_uid6_fpMultAddTest_b <= invExpXIsMax_uid24_p_uid6_fpMultAddTest_q;
    excR_x_uid26_p_uid6_fpMultAddTest_q <= excR_x_uid26_p_uid6_fpMultAddTest_a and excR_x_uid26_p_uid6_fpMultAddTest_b;

    -- ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest(LOGICAL,86)@3
    ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_a <= excR_x_uid26_p_uid6_fpMultAddTest_q;
    ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_b <= excR_y_uid40_p_uid6_fpMultAddTest_q;
    ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_c <= expOvf_uid78_p_uid6_fpMultAddTest_n;
    ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_q <= ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_a and ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_b and ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_c;

    -- excYRAndExcXI_uid86_p_uid6_fpMultAddTest(LOGICAL,85)@3
    excYRAndExcXI_uid86_p_uid6_fpMultAddTest_a <= excR_y_uid40_p_uid6_fpMultAddTest_q;
    excYRAndExcXI_uid86_p_uid6_fpMultAddTest_b <= excI_x_uid22_p_uid6_fpMultAddTest_q;
    excYRAndExcXI_uid86_p_uid6_fpMultAddTest_q <= excYRAndExcXI_uid86_p_uid6_fpMultAddTest_a and excYRAndExcXI_uid86_p_uid6_fpMultAddTest_b;

    -- excXRAndExcYI_uid85_p_uid6_fpMultAddTest(LOGICAL,84)@3
    excXRAndExcYI_uid85_p_uid6_fpMultAddTest_a <= excR_x_uid26_p_uid6_fpMultAddTest_q;
    excXRAndExcYI_uid85_p_uid6_fpMultAddTest_b <= excI_y_uid36_p_uid6_fpMultAddTest_q;
    excXRAndExcYI_uid85_p_uid6_fpMultAddTest_q <= excXRAndExcYI_uid85_p_uid6_fpMultAddTest_a and excXRAndExcYI_uid85_p_uid6_fpMultAddTest_b;

    -- excXIAndExcYI_uid84_p_uid6_fpMultAddTest(LOGICAL,83)@3
    excXIAndExcYI_uid84_p_uid6_fpMultAddTest_a <= excI_x_uid22_p_uid6_fpMultAddTest_q;
    excXIAndExcYI_uid84_p_uid6_fpMultAddTest_b <= excI_y_uid36_p_uid6_fpMultAddTest_q;
    excXIAndExcYI_uid84_p_uid6_fpMultAddTest_q <= excXIAndExcYI_uid84_p_uid6_fpMultAddTest_a and excXIAndExcYI_uid84_p_uid6_fpMultAddTest_b;

    -- excRInf_uid88_p_uid6_fpMultAddTest(LOGICAL,87)@3
    excRInf_uid88_p_uid6_fpMultAddTest_a <= excXIAndExcYI_uid84_p_uid6_fpMultAddTest_q;
    excRInf_uid88_p_uid6_fpMultAddTest_b <= excXRAndExcYI_uid85_p_uid6_fpMultAddTest_q;
    excRInf_uid88_p_uid6_fpMultAddTest_c <= excYRAndExcXI_uid86_p_uid6_fpMultAddTest_q;
    excRInf_uid88_p_uid6_fpMultAddTest_d <= ExcROvfAndInReg_uid87_p_uid6_fpMultAddTest_q;
    excRInf_uid88_p_uid6_fpMultAddTest_q <= excRInf_uid88_p_uid6_fpMultAddTest_a or excRInf_uid88_p_uid6_fpMultAddTest_b or excRInf_uid88_p_uid6_fpMultAddTest_c or excRInf_uid88_p_uid6_fpMultAddTest_d;

    -- expUdf_uid76_p_uid6_fpMultAddTest(COMPARE,75)@3
    expUdf_uid76_p_uid6_fpMultAddTest_cin <= GND_q;
    expUdf_uid76_p_uid6_fpMultAddTest_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR("0" & "000000000000" & GND_q) & '0');
    expUdf_uid76_p_uid6_fpMultAddTest_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((13 downto 12 => expRPreExcExt_uid74_p_uid6_fpMultAddTest_b(11)) & expRPreExcExt_uid74_p_uid6_fpMultAddTest_b) & expUdf_uid76_p_uid6_fpMultAddTest_cin(0));
    expUdf_uid76_p_uid6_fpMultAddTest_o <= STD_LOGIC_VECTOR(SIGNED(expUdf_uid76_p_uid6_fpMultAddTest_a) - SIGNED(expUdf_uid76_p_uid6_fpMultAddTest_b));
    expUdf_uid76_p_uid6_fpMultAddTest_n(0) <= not (expUdf_uid76_p_uid6_fpMultAddTest_o(14));

    -- excZC3_uid82_p_uid6_fpMultAddTest(LOGICAL,81)@3
    excZC3_uid82_p_uid6_fpMultAddTest_a <= excR_x_uid26_p_uid6_fpMultAddTest_q;
    excZC3_uid82_p_uid6_fpMultAddTest_b <= excR_y_uid40_p_uid6_fpMultAddTest_q;
    excZC3_uid82_p_uid6_fpMultAddTest_c <= expUdf_uid76_p_uid6_fpMultAddTest_n;
    excZC3_uid82_p_uid6_fpMultAddTest_q <= excZC3_uid82_p_uid6_fpMultAddTest_a and excZC3_uid82_p_uid6_fpMultAddTest_b and excZC3_uid82_p_uid6_fpMultAddTest_c;

    -- excYZAndExcXR_uid81_p_uid6_fpMultAddTest(LOGICAL,80)@3
    excYZAndExcXR_uid81_p_uid6_fpMultAddTest_a <= redist44_q;
    excYZAndExcXR_uid81_p_uid6_fpMultAddTest_b <= excR_x_uid26_p_uid6_fpMultAddTest_q;
    excYZAndExcXR_uid81_p_uid6_fpMultAddTest_q <= excYZAndExcXR_uid81_p_uid6_fpMultAddTest_a and excYZAndExcXR_uid81_p_uid6_fpMultAddTest_b;

    -- excXZAndExcYR_uid80_p_uid6_fpMultAddTest(LOGICAL,79)@3
    excXZAndExcYR_uid80_p_uid6_fpMultAddTest_a <= redist47_q;
    excXZAndExcYR_uid80_p_uid6_fpMultAddTest_b <= excR_y_uid40_p_uid6_fpMultAddTest_q;
    excXZAndExcYR_uid80_p_uid6_fpMultAddTest_q <= excXZAndExcYR_uid80_p_uid6_fpMultAddTest_a and excXZAndExcYR_uid80_p_uid6_fpMultAddTest_b;

    -- excXZAndExcYZ_uid79_p_uid6_fpMultAddTest(LOGICAL,78)@3
    excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_a <= redist47_q;
    excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_b <= redist44_q;
    excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_q <= excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_a and excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_b;

    -- excRZero_uid83_p_uid6_fpMultAddTest(LOGICAL,82)@3
    excRZero_uid83_p_uid6_fpMultAddTest_a <= excXZAndExcYZ_uid79_p_uid6_fpMultAddTest_q;
    excRZero_uid83_p_uid6_fpMultAddTest_b <= excXZAndExcYR_uid80_p_uid6_fpMultAddTest_q;
    excRZero_uid83_p_uid6_fpMultAddTest_c <= excYZAndExcXR_uid81_p_uid6_fpMultAddTest_q;
    excRZero_uid83_p_uid6_fpMultAddTest_d <= excZC3_uid82_p_uid6_fpMultAddTest_q;
    excRZero_uid83_p_uid6_fpMultAddTest_q <= excRZero_uid83_p_uid6_fpMultAddTest_a or excRZero_uid83_p_uid6_fpMultAddTest_b or excRZero_uid83_p_uid6_fpMultAddTest_c or excRZero_uid83_p_uid6_fpMultAddTest_d;

    -- concExc_uid93_p_uid6_fpMultAddTest(BITJOIN,92)@3
    concExc_uid93_p_uid6_fpMultAddTest_q <= excRNaN_uid92_p_uid6_fpMultAddTest_q & excRInf_uid88_p_uid6_fpMultAddTest_q & excRZero_uid83_p_uid6_fpMultAddTest_q;

    -- redist35(DELAY,392)
    redist35 : dspba_delay
    GENERIC MAP ( width => 3, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => concExc_uid93_p_uid6_fpMultAddTest_q, xout => redist35_q, clk => clk, aclr => areset );

    -- excREnc_uid94_p_uid6_fpMultAddTest(LOOKUP,93)@4
    excREnc_uid94_p_uid6_fpMultAddTest: PROCESS (redist35_q)
    BEGIN
        -- Begin reserved scope level
        CASE (redist35_q) IS
            WHEN "000" => excREnc_uid94_p_uid6_fpMultAddTest_q <= "01";
            WHEN "001" => excREnc_uid94_p_uid6_fpMultAddTest_q <= "00";
            WHEN "010" => excREnc_uid94_p_uid6_fpMultAddTest_q <= "10";
            WHEN "011" => excREnc_uid94_p_uid6_fpMultAddTest_q <= "00";
            WHEN "100" => excREnc_uid94_p_uid6_fpMultAddTest_q <= "11";
            WHEN "101" => excREnc_uid94_p_uid6_fpMultAddTest_q <= "00";
            WHEN "110" => excREnc_uid94_p_uid6_fpMultAddTest_q <= "00";
            WHEN "111" => excREnc_uid94_p_uid6_fpMultAddTest_q <= "00";
            WHEN OTHERS => -- unreachable
                           excREnc_uid94_p_uid6_fpMultAddTest_q <= (others => '-');
        END CASE;
        -- End reserved scope level
    END PROCESS;

    -- expRPostExc_uid103_p_uid6_fpMultAddTest(MUX,102)@4
    expRPostExc_uid103_p_uid6_fpMultAddTest_s <= excREnc_uid94_p_uid6_fpMultAddTest_q;
    expRPostExc_uid103_p_uid6_fpMultAddTest: PROCESS (expRPostExc_uid103_p_uid6_fpMultAddTest_s, cstAllZWE_uid15_p_uid6_fpMultAddTest_q, redist36_q, cstAllOWE_uid13_p_uid6_fpMultAddTest_q)
    BEGIN
        CASE (expRPostExc_uid103_p_uid6_fpMultAddTest_s) IS
            WHEN "00" => expRPostExc_uid103_p_uid6_fpMultAddTest_q <= cstAllZWE_uid15_p_uid6_fpMultAddTest_q;
            WHEN "01" => expRPostExc_uid103_p_uid6_fpMultAddTest_q <= redist36_q;
            WHEN "10" => expRPostExc_uid103_p_uid6_fpMultAddTest_q <= cstAllOWE_uid13_p_uid6_fpMultAddTest_q;
            WHEN "11" => expRPostExc_uid103_p_uid6_fpMultAddTest_q <= cstAllOWE_uid13_p_uid6_fpMultAddTest_q;
            WHEN OTHERS => expRPostExc_uid103_p_uid6_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- oneFracRPostExc2_uid95_p_uid6_fpMultAddTest(CONSTANT,94)
    oneFracRPostExc2_uid95_p_uid6_fpMultAddTest_q <= "00000000000000000000001";

    -- fracRPreExc_uid73_p_uid6_fpMultAddTest(BITSELECT,72)@3
    fracRPreExc_uid73_p_uid6_fpMultAddTest_in <= expFracRPostRounding_uid72_p_uid6_fpMultAddTest_q(23 downto 0);
    fracRPreExc_uid73_p_uid6_fpMultAddTest_b <= fracRPreExc_uid73_p_uid6_fpMultAddTest_in(23 downto 1);

    -- redist37(DELAY,394)
    redist37 : dspba_delay
    GENERIC MAP ( width => 23, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracRPreExc_uid73_p_uid6_fpMultAddTest_b, xout => redist37_q, clk => clk, aclr => areset );

    -- fracRPostExc_uid98_p_uid6_fpMultAddTest(MUX,97)@4
    fracRPostExc_uid98_p_uid6_fpMultAddTest_s <= excREnc_uid94_p_uid6_fpMultAddTest_q;
    fracRPostExc_uid98_p_uid6_fpMultAddTest: PROCESS (fracRPostExc_uid98_p_uid6_fpMultAddTest_s, cstZeroWF_uid14_p_uid6_fpMultAddTest_q, redist37_q, oneFracRPostExc2_uid95_p_uid6_fpMultAddTest_q)
    BEGIN
        CASE (fracRPostExc_uid98_p_uid6_fpMultAddTest_s) IS
            WHEN "00" => fracRPostExc_uid98_p_uid6_fpMultAddTest_q <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
            WHEN "01" => fracRPostExc_uid98_p_uid6_fpMultAddTest_q <= redist37_q;
            WHEN "10" => fracRPostExc_uid98_p_uid6_fpMultAddTest_q <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
            WHEN "11" => fracRPostExc_uid98_p_uid6_fpMultAddTest_q <= oneFracRPostExc2_uid95_p_uid6_fpMultAddTest_q;
            WHEN OTHERS => fracRPostExc_uid98_p_uid6_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- R_uid106_p_uid6_fpMultAddTest(BITJOIN,105)@4
    R_uid106_p_uid6_fpMultAddTest_q <= signRPostExc_uid105_p_uid6_fpMultAddTest_q & expRPostExc_uid103_p_uid6_fpMultAddTest_q & fracRPostExc_uid98_p_uid6_fpMultAddTest_q;

    -- expFracY_uid109_r_uid7_fpMultAddTest(BITSELECT,108)@4
    expFracY_uid109_r_uid7_fpMultAddTest_in <= redist48_mem_q;
    expFracY_uid109_r_uid7_fpMultAddTest_b <= expFracY_uid109_r_uid7_fpMultAddTest_in(30 downto 0);

    -- expFracX_uid108_r_uid7_fpMultAddTest(BITSELECT,107)@4
    expFracX_uid108_r_uid7_fpMultAddTest_in <= R_uid106_p_uid6_fpMultAddTest_q;
    expFracX_uid108_r_uid7_fpMultAddTest_b <= expFracX_uid108_r_uid7_fpMultAddTest_in(30 downto 0);

    -- xGTEy_uid110_r_uid7_fpMultAddTest(COMPARE,109)@4
    xGTEy_uid110_r_uid7_fpMultAddTest_cin <= GND_q;
    xGTEy_uid110_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("00" & expFracX_uid108_r_uid7_fpMultAddTest_b) & '0';
    xGTEy_uid110_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("00" & expFracY_uid109_r_uid7_fpMultAddTest_b) & xGTEy_uid110_r_uid7_fpMultAddTest_cin(0);
    xGTEy_uid110_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(xGTEy_uid110_r_uid7_fpMultAddTest_a) - UNSIGNED(xGTEy_uid110_r_uid7_fpMultAddTest_b));
    xGTEy_uid110_r_uid7_fpMultAddTest_n(0) <= not (xGTEy_uid110_r_uid7_fpMultAddTest_o(33));

    -- bSig_uid119_r_uid7_fpMultAddTest(MUX,118)@4
    bSig_uid119_r_uid7_fpMultAddTest_s <= xGTEy_uid110_r_uid7_fpMultAddTest_n;
    bSig_uid119_r_uid7_fpMultAddTest: PROCESS (bSig_uid119_r_uid7_fpMultAddTest_s, R_uid106_p_uid6_fpMultAddTest_q, ypn_uid114_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (bSig_uid119_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => bSig_uid119_r_uid7_fpMultAddTest_q <= R_uid106_p_uid6_fpMultAddTest_q;
            WHEN "1" => bSig_uid119_r_uid7_fpMultAddTest_q <= ypn_uid114_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => bSig_uid119_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- sigB_uid153_r_uid7_fpMultAddTest(BITSELECT,152)@4
    sigB_uid153_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(bSig_uid119_r_uid7_fpMultAddTest_q);
    sigB_uid153_r_uid7_fpMultAddTest_b <= sigB_uid153_r_uid7_fpMultAddTest_in(31 downto 31);

    -- redist17(DELAY,374)
    redist17 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => sigB_uid153_r_uid7_fpMultAddTest_b, xout => redist17_q, clk => clk, aclr => areset );

    -- aSig_uid118_r_uid7_fpMultAddTest(MUX,117)@4
    aSig_uid118_r_uid7_fpMultAddTest_s <= xGTEy_uid110_r_uid7_fpMultAddTest_n;
    aSig_uid118_r_uid7_fpMultAddTest: PROCESS (aSig_uid118_r_uid7_fpMultAddTest_s, ypn_uid114_r_uid7_fpMultAddTest_q, R_uid106_p_uid6_fpMultAddTest_q)
    BEGIN
        CASE (aSig_uid118_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => aSig_uid118_r_uid7_fpMultAddTest_q <= ypn_uid114_r_uid7_fpMultAddTest_q;
            WHEN "1" => aSig_uid118_r_uid7_fpMultAddTest_q <= R_uid106_p_uid6_fpMultAddTest_q;
            WHEN OTHERS => aSig_uid118_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- sigA_uid152_r_uid7_fpMultAddTest(BITSELECT,151)@4
    sigA_uid152_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(aSig_uid118_r_uid7_fpMultAddTest_q);
    sigA_uid152_r_uid7_fpMultAddTest_b <= sigA_uid152_r_uid7_fpMultAddTest_in(31 downto 31);

    -- redist19(DELAY,376)
    redist19 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => sigA_uid152_r_uid7_fpMultAddTest_b, xout => redist19_q, clk => clk, aclr => areset );

    -- effSub_uid154_r_uid7_fpMultAddTest(LOGICAL,153)@6
    effSub_uid154_r_uid7_fpMultAddTest_a <= redist19_q;
    effSub_uid154_r_uid7_fpMultAddTest_b <= redist17_q;
    effSub_uid154_r_uid7_fpMultAddTest_q <= effSub_uid154_r_uid7_fpMultAddTest_a xor effSub_uid154_r_uid7_fpMultAddTest_b;

    -- exp_bSig_uid137_r_uid7_fpMultAddTest(BITSELECT,136)@4
    exp_bSig_uid137_r_uid7_fpMultAddTest_in <= bSig_uid119_r_uid7_fpMultAddTest_q(30 downto 0);
    exp_bSig_uid137_r_uid7_fpMultAddTest_b <= exp_bSig_uid137_r_uid7_fpMultAddTest_in(30 downto 23);

    -- redist28(DELAY,385)
    redist28 : dspba_delay
    GENERIC MAP ( width => 8, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => exp_bSig_uid137_r_uid7_fpMultAddTest_b, xout => redist28_q, clk => clk, aclr => areset );

    -- exp_aSig_uid123_r_uid7_fpMultAddTest(BITSELECT,122)@4
    exp_aSig_uid123_r_uid7_fpMultAddTest_in <= aSig_uid118_r_uid7_fpMultAddTest_q(30 downto 0);
    exp_aSig_uid123_r_uid7_fpMultAddTest_b <= exp_aSig_uid123_r_uid7_fpMultAddTest_in(30 downto 23);

    -- redist33(DELAY,390)
    redist33 : dspba_delay
    GENERIC MAP ( width => 8, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => exp_aSig_uid123_r_uid7_fpMultAddTest_b, xout => redist33_q, clk => clk, aclr => areset );

    -- expAmExpB_uid162_r_uid7_fpMultAddTest(SUB,161)@5
    expAmExpB_uid162_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("0" & redist33_q);
    expAmExpB_uid162_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("0" & redist28_q);
    expAmExpB_uid162_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(expAmExpB_uid162_r_uid7_fpMultAddTest_a) - UNSIGNED(expAmExpB_uid162_r_uid7_fpMultAddTest_b));
    expAmExpB_uid162_r_uid7_fpMultAddTest_q <= expAmExpB_uid162_r_uid7_fpMultAddTest_o(8 downto 0);

    -- cWFP2_uid163_r_uid7_fpMultAddTest(CONSTANT,162)
    cWFP2_uid163_r_uid7_fpMultAddTest_q <= "11001";

    -- shiftedOut_uid165_r_uid7_fpMultAddTest(COMPARE,164)@5
    shiftedOut_uid165_r_uid7_fpMultAddTest_cin <= GND_q;
    shiftedOut_uid165_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("000000" & cWFP2_uid163_r_uid7_fpMultAddTest_q) & '0';
    shiftedOut_uid165_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("00" & expAmExpB_uid162_r_uid7_fpMultAddTest_q) & shiftedOut_uid165_r_uid7_fpMultAddTest_cin(0);
    shiftedOut_uid165_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(shiftedOut_uid165_r_uid7_fpMultAddTest_a) - UNSIGNED(shiftedOut_uid165_r_uid7_fpMultAddTest_b));
    shiftedOut_uid165_r_uid7_fpMultAddTest_c(0) <= shiftedOut_uid165_r_uid7_fpMultAddTest_o(11);

    -- iShiftedOut_uid169_r_uid7_fpMultAddTest(LOGICAL,168)@5
    iShiftedOut_uid169_r_uid7_fpMultAddTest_a <= shiftedOut_uid165_r_uid7_fpMultAddTest_c;
    iShiftedOut_uid169_r_uid7_fpMultAddTest_q <= not (iShiftedOut_uid169_r_uid7_fpMultAddTest_a);

    -- zeroOutCst_uid324_alignmentShifter_uid166_r_uid7_fpMultAddTest(CONSTANT,323)
    zeroOutCst_uid324_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= "0000000000000000000000000000000000000000000000000";

    -- rightShiftStage2Idx3Pad3_uid320_alignmentShifter_uid166_r_uid7_fpMultAddTest(CONSTANT,319)
    rightShiftStage2Idx3Pad3_uid320_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= "000";

    -- rightShiftStage2Idx3Rng3_uid319_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,318)@5
    rightShiftStage2Idx3Rng3_uid319_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
    rightShiftStage2Idx3Rng3_uid319_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage2Idx3Rng3_uid319_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 3);

    -- rightShiftStage2Idx3_uid321_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,320)@5
    rightShiftStage2Idx3_uid321_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage2Idx3Pad3_uid320_alignmentShifter_uid166_r_uid7_fpMultAddTest_q & rightShiftStage2Idx3Rng3_uid319_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- zs_uid276_lzCountVal_uid187_r_uid7_fpMultAddTest(CONSTANT,275)
    zs_uid276_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "00";

    -- rightShiftStage2Idx2Rng2_uid316_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,315)@5
    rightShiftStage2Idx2Rng2_uid316_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
    rightShiftStage2Idx2Rng2_uid316_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage2Idx2Rng2_uid316_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 2);

    -- rightShiftStage2Idx2_uid318_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,317)@5
    rightShiftStage2Idx2_uid318_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= zs_uid276_lzCountVal_uid187_r_uid7_fpMultAddTest_q & rightShiftStage2Idx2Rng2_uid316_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- rightShiftStage2Idx1Rng1_uid313_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,312)@5
    rightShiftStage2Idx1Rng1_uid313_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
    rightShiftStage2Idx1Rng1_uid313_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage2Idx1Rng1_uid313_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 1);

    -- rightShiftStage2Idx1_uid315_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,314)@5
    rightShiftStage2Idx1_uid315_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= GND_q & rightShiftStage2Idx1Rng1_uid313_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- rightShiftStage1Idx3Pad12_uid309_alignmentShifter_uid166_r_uid7_fpMultAddTest(CONSTANT,308)
    rightShiftStage1Idx3Pad12_uid309_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= "000000000000";

    -- rightShiftStage1Idx3Rng12_uid308_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,307)@5
    rightShiftStage1Idx3Rng12_uid308_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
    rightShiftStage1Idx3Rng12_uid308_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage1Idx3Rng12_uid308_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 12);

    -- rightShiftStage1Idx3_uid310_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,309)@5
    rightShiftStage1Idx3_uid310_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage1Idx3Pad12_uid309_alignmentShifter_uid166_r_uid7_fpMultAddTest_q & rightShiftStage1Idx3Rng12_uid308_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- rightShiftStage1Idx2Rng8_uid305_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,304)@5
    rightShiftStage1Idx2Rng8_uid305_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
    rightShiftStage1Idx2Rng8_uid305_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage1Idx2Rng8_uid305_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 8);

    -- rightShiftStage1Idx2_uid307_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,306)@5
    rightShiftStage1Idx2_uid307_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= cstAllZWE_uid15_p_uid6_fpMultAddTest_q & rightShiftStage1Idx2Rng8_uid305_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- zs_uid270_lzCountVal_uid187_r_uid7_fpMultAddTest(CONSTANT,269)
    zs_uid270_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "0000";

    -- rightShiftStage1Idx1Rng4_uid302_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,301)@5
    rightShiftStage1Idx1Rng4_uid302_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
    rightShiftStage1Idx1Rng4_uid302_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage1Idx1Rng4_uid302_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 4);

    -- rightShiftStage1Idx1_uid304_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,303)@5
    rightShiftStage1Idx1_uid304_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= zs_uid270_lzCountVal_uid187_r_uid7_fpMultAddTest_q & rightShiftStage1Idx1Rng4_uid302_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- rightShiftStage0Idx3Pad48_uid298_alignmentShifter_uid166_r_uid7_fpMultAddTest(CONSTANT,297)
    rightShiftStage0Idx3Pad48_uid298_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= "000000000000000000000000000000000000000000000000";

    -- rightShiftStage0Idx3Rng48_uid297_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,296)@5
    rightShiftStage0Idx3Rng48_uid297_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightPaddedIn_uid167_r_uid7_fpMultAddTest_q;
    rightShiftStage0Idx3Rng48_uid297_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage0Idx3Rng48_uid297_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 48);

    -- rightShiftStage0Idx3_uid299_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,298)@5
    rightShiftStage0Idx3_uid299_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage0Idx3Pad48_uid298_alignmentShifter_uid166_r_uid7_fpMultAddTest_q & rightShiftStage0Idx3Rng48_uid297_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- rightShiftStage0Idx2Pad32_uid295_alignmentShifter_uid166_r_uid7_fpMultAddTest(CONSTANT,294)
    rightShiftStage0Idx2Pad32_uid295_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= "00000000000000000000000000000000";

    -- rightShiftStage0Idx2Rng32_uid294_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,293)@5
    rightShiftStage0Idx2Rng32_uid294_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightPaddedIn_uid167_r_uid7_fpMultAddTest_q;
    rightShiftStage0Idx2Rng32_uid294_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage0Idx2Rng32_uid294_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 32);

    -- rightShiftStage0Idx2_uid296_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,295)@5
    rightShiftStage0Idx2_uid296_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage0Idx2Pad32_uid295_alignmentShifter_uid166_r_uid7_fpMultAddTest_q & rightShiftStage0Idx2Rng32_uid294_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- rightShiftStage0Idx1Rng16_uid291_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,290)@5
    rightShiftStage0Idx1Rng16_uid291_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= rightPaddedIn_uid167_r_uid7_fpMultAddTest_q;
    rightShiftStage0Idx1Rng16_uid291_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStage0Idx1Rng16_uid291_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(48 downto 16);

    -- rightShiftStage0Idx1_uid293_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITJOIN,292)@5
    rightShiftStage0Idx1_uid293_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= zs_uid256_lzCountVal_uid187_r_uid7_fpMultAddTest_q & rightShiftStage0Idx1Rng16_uid291_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;

    -- excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest(LOGICAL,138)@5
    excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_a <= redist28_q;
    excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_b <= cstAllZWE_uid15_p_uid6_fpMultAddTest_q;
    excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_q <= "1" WHEN excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_a = excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_b ELSE "0";

    -- InvExpXIsZero_uid146_r_uid7_fpMultAddTest(LOGICAL,145)@5
    InvExpXIsZero_uid146_r_uid7_fpMultAddTest_a <= excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_q;
    InvExpXIsZero_uid146_r_uid7_fpMultAddTest_q <= not (InvExpXIsZero_uid146_r_uid7_fpMultAddTest_a);

    -- frac_bSig_uid138_r_uid7_fpMultAddTest(BITSELECT,137)@4
    frac_bSig_uid138_r_uid7_fpMultAddTest_in <= bSig_uid119_r_uid7_fpMultAddTest_q(22 downto 0);
    frac_bSig_uid138_r_uid7_fpMultAddTest_b <= frac_bSig_uid138_r_uid7_fpMultAddTest_in(22 downto 0);

    -- redist27(DELAY,384)
    redist27 : dspba_delay
    GENERIC MAP ( width => 23, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => frac_bSig_uid138_r_uid7_fpMultAddTest_b, xout => redist27_q, clk => clk, aclr => areset );

    -- fracBz_uid158_r_uid7_fpMultAddTest(MUX,157)@5
    fracBz_uid158_r_uid7_fpMultAddTest_s <= excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_q;
    fracBz_uid158_r_uid7_fpMultAddTest: PROCESS (fracBz_uid158_r_uid7_fpMultAddTest_s, redist27_q, cstZeroWF_uid14_p_uid6_fpMultAddTest_q)
    BEGIN
        CASE (fracBz_uid158_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => fracBz_uid158_r_uid7_fpMultAddTest_q <= redist27_q;
            WHEN "1" => fracBz_uid158_r_uid7_fpMultAddTest_q <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
            WHEN OTHERS => fracBz_uid158_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- oFracB_uid161_r_uid7_fpMultAddTest(BITJOIN,160)@5
    oFracB_uid161_r_uid7_fpMultAddTest_q <= InvExpXIsZero_uid146_r_uid7_fpMultAddTest_q & fracBz_uid158_r_uid7_fpMultAddTest_q;

    -- padConst_uid166_r_uid7_fpMultAddTest(CONSTANT,165)
    padConst_uid166_r_uid7_fpMultAddTest_q <= "0000000000000000000000000";

    -- rightPaddedIn_uid167_r_uid7_fpMultAddTest(BITJOIN,166)@5
    rightPaddedIn_uid167_r_uid7_fpMultAddTest_q <= oFracB_uid161_r_uid7_fpMultAddTest_q & padConst_uid166_r_uid7_fpMultAddTest_q;

    -- rightShiftStageSel5Dto4_uid300_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,299)@5
    rightShiftStageSel5Dto4_uid300_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= expAmExpB_uid162_r_uid7_fpMultAddTest_q(5 downto 0);
    rightShiftStageSel5Dto4_uid300_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStageSel5Dto4_uid300_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(5 downto 4);

    -- rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest(MUX,300)@5
    rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_s <= rightShiftStageSel5Dto4_uid300_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;
    rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest: PROCESS (rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_s, rightPaddedIn_uid167_r_uid7_fpMultAddTest_q, rightShiftStage0Idx1_uid293_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, rightShiftStage0Idx2_uid296_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, rightShiftStage0Idx3_uid299_alignmentShifter_uid166_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_s) IS
            WHEN "00" => rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightPaddedIn_uid167_r_uid7_fpMultAddTest_q;
            WHEN "01" => rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage0Idx1_uid293_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "10" => rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage0Idx2_uid296_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "11" => rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage0Idx3_uid299_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rightShiftStageSel3Dto2_uid311_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,310)@5
    rightShiftStageSel3Dto2_uid311_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= expAmExpB_uid162_r_uid7_fpMultAddTest_q(3 downto 0);
    rightShiftStageSel3Dto2_uid311_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStageSel3Dto2_uid311_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(3 downto 2);

    -- rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest(MUX,311)@5
    rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_s <= rightShiftStageSel3Dto2_uid311_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;
    rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest: PROCESS (rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_s, rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, rightShiftStage1Idx1_uid304_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, rightShiftStage1Idx2_uid307_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, rightShiftStage1Idx3_uid310_alignmentShifter_uid166_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_s) IS
            WHEN "00" => rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage0_uid301_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "01" => rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage1Idx1_uid304_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "10" => rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage1Idx2_uid307_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "11" => rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage1Idx3_uid310_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rightShiftStageSel1Dto0_uid322_alignmentShifter_uid166_r_uid7_fpMultAddTest(BITSELECT,321)@5
    rightShiftStageSel1Dto0_uid322_alignmentShifter_uid166_r_uid7_fpMultAddTest_in <= expAmExpB_uid162_r_uid7_fpMultAddTest_q(1 downto 0);
    rightShiftStageSel1Dto0_uid322_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= rightShiftStageSel1Dto0_uid322_alignmentShifter_uid166_r_uid7_fpMultAddTest_in(1 downto 0);

    -- rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest(MUX,322)@5
    rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_s <= rightShiftStageSel1Dto0_uid322_alignmentShifter_uid166_r_uid7_fpMultAddTest_b;
    rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest: PROCESS (rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_s, rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, rightShiftStage2Idx1_uid315_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, rightShiftStage2Idx2_uid318_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, rightShiftStage2Idx3_uid321_alignmentShifter_uid166_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_s) IS
            WHEN "00" => rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage1_uid312_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "01" => rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage2Idx1_uid315_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "10" => rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage2Idx2_uid318_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "11" => rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage2Idx3_uid321_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- wIntCst_uid289_alignmentShifter_uid166_r_uid7_fpMultAddTest(CONSTANT,288)
    wIntCst_uid289_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= "110001";

    -- shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest(COMPARE,289)@5
    shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_cin <= GND_q;
    shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("00" & expAmExpB_uid162_r_uid7_fpMultAddTest_q) & '0';
    shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("00000" & wIntCst_uid289_alignmentShifter_uid166_r_uid7_fpMultAddTest_q) & shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_cin(0);
    shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_a) - UNSIGNED(shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_b));
    shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_n(0) <= not (shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_o(11));

    -- r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest(MUX,324)@5
    r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_s <= shiftedOut_uid290_alignmentShifter_uid166_r_uid7_fpMultAddTest_n;
    r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest: PROCESS (r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_s, rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_q, zeroOutCst_uid324_alignmentShifter_uid166_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= rightShiftStage2_uid323_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN "1" => r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= zeroOutCst_uid324_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest(LOGICAL,169)@5
    alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_a <= r_uid325_alignmentShifter_uid166_r_uid7_fpMultAddTest_q;
    alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR((48 downto 1 => iShiftedOut_uid169_r_uid7_fpMultAddTest_q(0)) & iShiftedOut_uid169_r_uid7_fpMultAddTest_q);
    alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_q <= alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_a and alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_b;

    -- alignFracBOpRange_uid180_r_uid7_fpMultAddTest(BITSELECT,179)@5
    alignFracBOpRange_uid180_r_uid7_fpMultAddTest_in <= alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_q;
    alignFracBOpRange_uid180_r_uid7_fpMultAddTest_b <= alignFracBOpRange_uid180_r_uid7_fpMultAddTest_in(48 downto 23);

    -- redist13(DELAY,370)
    redist13 : dspba_delay
    GENERIC MAP ( width => 26, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => alignFracBOpRange_uid180_r_uid7_fpMultAddTest_b, xout => redist13_q, clk => clk, aclr => areset );

    -- fracBAddOp_uid182_r_uid7_fpMultAddTest(BITJOIN,181)@6
    fracBAddOp_uid182_r_uid7_fpMultAddTest_q <= GND_q & redist13_q;

    -- fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest(LOGICAL,182)@6
    fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_a <= fracBAddOp_uid182_r_uid7_fpMultAddTest_q;
    fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR((26 downto 1 => effSub_uid154_r_uid7_fpMultAddTest_q(0)) & effSub_uid154_r_uid7_fpMultAddTest_q);
    fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_q <= fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_a xor fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_b;

    -- zocst_uid178_r_uid7_fpMultAddTest(CONSTANT,177)
    zocst_uid178_r_uid7_fpMultAddTest_q <= "01";

    -- frac_aSig_uid124_r_uid7_fpMultAddTest(BITSELECT,123)@4
    frac_aSig_uid124_r_uid7_fpMultAddTest_in <= aSig_uid118_r_uid7_fpMultAddTest_q(22 downto 0);
    frac_aSig_uid124_r_uid7_fpMultAddTest_b <= frac_aSig_uid124_r_uid7_fpMultAddTest_in(22 downto 0);

    -- redist32(DELAY,389)
    redist32 : dspba_delay
    GENERIC MAP ( width => 23, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => frac_aSig_uid124_r_uid7_fpMultAddTest_b, xout => redist32_q, clk => clk, aclr => areset );

    -- stickyBits_uid171_r_uid7_fpMultAddTest(BITSELECT,170)@5
    stickyBits_uid171_r_uid7_fpMultAddTest_in <= alignFracBPostShiftOut_uid170_r_uid7_fpMultAddTest_q(22 downto 0);
    stickyBits_uid171_r_uid7_fpMultAddTest_b <= stickyBits_uid171_r_uid7_fpMultAddTest_in(22 downto 0);

    -- redist15(DELAY,372)
    redist15 : dspba_delay
    GENERIC MAP ( width => 23, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => stickyBits_uid171_r_uid7_fpMultAddTest_b, xout => redist15_q, clk => clk, aclr => areset );

    -- cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest(LOGICAL,172)@6
    cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_a <= redist15_q;
    cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_b <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
    cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_q <= "1" WHEN cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_a = cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_b ELSE "0";

    -- invCmpEQ_stickyBits_cZwF_uid174_r_uid7_fpMultAddTest(LOGICAL,173)@6
    invCmpEQ_stickyBits_cZwF_uid174_r_uid7_fpMultAddTest_a <= cmpEQ_stickyBits_cZwF_uid173_r_uid7_fpMultAddTest_q;
    invCmpEQ_stickyBits_cZwF_uid174_r_uid7_fpMultAddTest_q <= not (invCmpEQ_stickyBits_cZwF_uid174_r_uid7_fpMultAddTest_a);

    -- invSticky_uid175_r_uid7_fpMultAddTest(LOGICAL,174)@6
    invSticky_uid175_r_uid7_fpMultAddTest_a <= invCmpEQ_stickyBits_cZwF_uid174_r_uid7_fpMultAddTest_q;
    invSticky_uid175_r_uid7_fpMultAddTest_q <= not (invSticky_uid175_r_uid7_fpMultAddTest_a);

    -- effSubInvSticky_uid176_r_uid7_fpMultAddTest(LOGICAL,175)@6
    effSubInvSticky_uid176_r_uid7_fpMultAddTest_a <= effSub_uid154_r_uid7_fpMultAddTest_q;
    effSubInvSticky_uid176_r_uid7_fpMultAddTest_b <= invSticky_uid175_r_uid7_fpMultAddTest_q;
    effSubInvSticky_uid176_r_uid7_fpMultAddTest_q <= effSubInvSticky_uid176_r_uid7_fpMultAddTest_a and effSubInvSticky_uid176_r_uid7_fpMultAddTest_b;

    -- fracAAddOp_uid179_r_uid7_fpMultAddTest(BITJOIN,178)@6
    fracAAddOp_uid179_r_uid7_fpMultAddTest_q <= zocst_uid178_r_uid7_fpMultAddTest_q & redist32_q & GND_q & effSubInvSticky_uid176_r_uid7_fpMultAddTest_q;

    -- fracAddResult_uid184_r_uid7_fpMultAddTest(ADD,183)@6
    fracAddResult_uid184_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("0" & fracAAddOp_uid179_r_uid7_fpMultAddTest_q);
    fracAddResult_uid184_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("0" & fracBAddOpPostXor_uid183_r_uid7_fpMultAddTest_q);
    fracAddResult_uid184_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(fracAddResult_uid184_r_uid7_fpMultAddTest_a) + UNSIGNED(fracAddResult_uid184_r_uid7_fpMultAddTest_b));
    fracAddResult_uid184_r_uid7_fpMultAddTest_q <= fracAddResult_uid184_r_uid7_fpMultAddTest_o(27 downto 0);

    -- rangeFracAddResultMwfp3Dto0_uid185_r_uid7_fpMultAddTest(BITSELECT,184)@6
    rangeFracAddResultMwfp3Dto0_uid185_r_uid7_fpMultAddTest_in <= fracAddResult_uid184_r_uid7_fpMultAddTest_q(26 downto 0);
    rangeFracAddResultMwfp3Dto0_uid185_r_uid7_fpMultAddTest_b <= rangeFracAddResultMwfp3Dto0_uid185_r_uid7_fpMultAddTest_in(26 downto 0);

    -- redist12(DELAY,369)
    redist12 : dspba_delay
    GENERIC MAP ( width => 27, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => rangeFracAddResultMwfp3Dto0_uid185_r_uid7_fpMultAddTest_b, xout => redist12_q, clk => clk, aclr => areset );

    -- redist14(DELAY,371)
    redist14 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => invCmpEQ_stickyBits_cZwF_uid174_r_uid7_fpMultAddTest_q, xout => redist14_q, clk => clk, aclr => areset );

    -- fracGRS_uid186_r_uid7_fpMultAddTest(BITJOIN,185)@7
    fracGRS_uid186_r_uid7_fpMultAddTest_q <= redist12_q & redist14_q;

    -- rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,256)@7
    rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= fracGRS_uid186_r_uid7_fpMultAddTest_q;
    rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest_in(27 downto 12);

    -- vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest(LOGICAL,257)@7
    vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_a <= rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
    vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= zs_uid256_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "1" WHEN vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_a = vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_b ELSE "0";

    -- redist5(DELAY,362)
    redist5 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_q, xout => redist5_q, clk => clk, aclr => areset );

    -- vStage_uid260_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,259)@7
    vStage_uid260_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= fracGRS_uid186_r_uid7_fpMultAddTest_q(11 downto 0);
    vStage_uid260_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= vStage_uid260_lzCountVal_uid187_r_uid7_fpMultAddTest_in(11 downto 0);

    -- mO_uid259_lzCountVal_uid187_r_uid7_fpMultAddTest(CONSTANT,258)
    mO_uid259_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "1111";

    -- cStage_uid261_lzCountVal_uid187_r_uid7_fpMultAddTest(BITJOIN,260)@7
    cStage_uid261_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= vStage_uid260_lzCountVal_uid187_r_uid7_fpMultAddTest_b & mO_uid259_lzCountVal_uid187_r_uid7_fpMultAddTest_q;

    -- vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest(MUX,262)@7
    vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_s <= vCount_uid258_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest: PROCESS (vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_s, rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest_b, cStage_uid261_lzCountVal_uid187_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= rVStage_uid257_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
            WHEN "1" => vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= cStage_uid261_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,264)@7
    rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest_in(15 downto 8);

    -- vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest(LOGICAL,265)@7
    vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_a <= rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
    vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= cstAllZWE_uid15_p_uid6_fpMultAddTest_q;
    vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "1" WHEN vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_a = vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_b ELSE "0";

    -- redist3(DELAY,360)
    redist3 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_q, xout => redist3_q, clk => clk, aclr => areset );

    -- vStage_uid267_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,266)@7
    vStage_uid267_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= vStagei_uid263_lzCountVal_uid187_r_uid7_fpMultAddTest_q(7 downto 0);
    vStage_uid267_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= vStage_uid267_lzCountVal_uid187_r_uid7_fpMultAddTest_in(7 downto 0);

    -- vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest(MUX,268)@7
    vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_s <= vCount_uid266_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest: PROCESS (vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_s, rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest_b, vStage_uid267_lzCountVal_uid187_r_uid7_fpMultAddTest_b)
    BEGIN
        CASE (vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= rVStage_uid265_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
            WHEN "1" => vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= vStage_uid267_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
            WHEN OTHERS => vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,270)@7
    rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest_in(7 downto 4);

    -- vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest(LOGICAL,271)@7
    vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_a <= rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
    vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= zs_uid270_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "1" WHEN vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_a = vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_b ELSE "0";

    -- redist2(DELAY,359)
    redist2 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_q, xout => redist2_q, clk => clk, aclr => areset );

    -- vStage_uid273_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,272)@7
    vStage_uid273_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= vStagei_uid269_lzCountVal_uid187_r_uid7_fpMultAddTest_q(3 downto 0);
    vStage_uid273_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= vStage_uid273_lzCountVal_uid187_r_uid7_fpMultAddTest_in(3 downto 0);

    -- vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest(MUX,274)@7
    vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_s <= vCount_uid272_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest: PROCESS (vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_s, rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest_b, vStage_uid273_lzCountVal_uid187_r_uid7_fpMultAddTest_b)
    BEGIN
        CASE (vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= rVStage_uid271_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
            WHEN "1" => vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= vStage_uid273_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
            WHEN OTHERS => vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rVStage_uid277_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,276)@7
    rVStage_uid277_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    rVStage_uid277_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= rVStage_uid277_lzCountVal_uid187_r_uid7_fpMultAddTest_in(3 downto 2);

    -- redist1(DELAY,358)
    redist1 : dspba_delay
    GENERIC MAP ( width => 2, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => rVStage_uid277_lzCountVal_uid187_r_uid7_fpMultAddTest_b, xout => redist1_q, clk => clk, aclr => areset );

    -- vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest(LOGICAL,277)@8
    vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_a <= redist1_q;
    vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= zs_uid276_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "1" WHEN vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_a = vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_b ELSE "0";

    -- vStage_uid279_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,278)@7
    vStage_uid279_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= vStagei_uid275_lzCountVal_uid187_r_uid7_fpMultAddTest_q(1 downto 0);
    vStage_uid279_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= vStage_uid279_lzCountVal_uid187_r_uid7_fpMultAddTest_in(1 downto 0);

    -- redist0(DELAY,357)
    redist0 : dspba_delay
    GENERIC MAP ( width => 2, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => vStage_uid279_lzCountVal_uid187_r_uid7_fpMultAddTest_b, xout => redist0_q, clk => clk, aclr => areset );

    -- vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest(MUX,280)@8
    vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_s <= vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest: PROCESS (vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_s, redist1_q, redist0_q)
    BEGIN
        CASE (vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= redist1_q;
            WHEN "1" => vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= redist0_q;
            WHEN OTHERS => vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rVStage_uid283_lzCountVal_uid187_r_uid7_fpMultAddTest(BITSELECT,282)@8
    rVStage_uid283_lzCountVal_uid187_r_uid7_fpMultAddTest_in <= vStagei_uid281_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    rVStage_uid283_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= rVStage_uid283_lzCountVal_uid187_r_uid7_fpMultAddTest_in(1 downto 1);

    -- vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest(LOGICAL,283)@8
    vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_a <= rVStage_uid283_lzCountVal_uid187_r_uid7_fpMultAddTest_b;
    vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_b <= GND_q;
    vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= "1" WHEN vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_a = vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_b ELSE "0";

    -- r_uid285_lzCountVal_uid187_r_uid7_fpMultAddTest(BITJOIN,284)@8
    r_uid285_lzCountVal_uid187_r_uid7_fpMultAddTest_q <= redist5_q & redist3_q & redist2_q & vCount_uid278_lzCountVal_uid187_r_uid7_fpMultAddTest_q & vCount_uid284_lzCountVal_uid187_r_uid7_fpMultAddTest_q;

    -- aMinusA_uid189_r_uid7_fpMultAddTest(LOGICAL,188)@8
    aMinusA_uid189_r_uid7_fpMultAddTest_a <= r_uid285_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    aMinusA_uid189_r_uid7_fpMultAddTest_b <= cAmA_uid188_r_uid7_fpMultAddTest_q;
    aMinusA_uid189_r_uid7_fpMultAddTest_q <= "1" WHEN aMinusA_uid189_r_uid7_fpMultAddTest_a = aMinusA_uid189_r_uid7_fpMultAddTest_b ELSE "0";

    -- invAMinusA_uid232_r_uid7_fpMultAddTest(LOGICAL,231)@8
    invAMinusA_uid232_r_uid7_fpMultAddTest_a <= aMinusA_uid189_r_uid7_fpMultAddTest_q;
    invAMinusA_uid232_r_uid7_fpMultAddTest_q <= not (invAMinusA_uid232_r_uid7_fpMultAddTest_a);

    -- redist20(DELAY,377)
    redist20 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist19_q, xout => redist20_q, clk => clk, aclr => areset );

    -- expXIsMax_uid140_r_uid7_fpMultAddTest(LOGICAL,139)@5
    expXIsMax_uid140_r_uid7_fpMultAddTest_a <= redist28_q;
    expXIsMax_uid140_r_uid7_fpMultAddTest_b <= cstAllOWE_uid13_p_uid6_fpMultAddTest_q;
    expXIsMax_uid140_r_uid7_fpMultAddTest_q <= "1" WHEN expXIsMax_uid140_r_uid7_fpMultAddTest_a = expXIsMax_uid140_r_uid7_fpMultAddTest_b ELSE "0";

    -- invExpXIsMax_uid145_r_uid7_fpMultAddTest(LOGICAL,144)@5
    invExpXIsMax_uid145_r_uid7_fpMultAddTest_a <= expXIsMax_uid140_r_uid7_fpMultAddTest_q;
    invExpXIsMax_uid145_r_uid7_fpMultAddTest_q <= not (invExpXIsMax_uid145_r_uid7_fpMultAddTest_a);

    -- excR_bSig_uid147_r_uid7_fpMultAddTest(LOGICAL,146)@5
    excR_bSig_uid147_r_uid7_fpMultAddTest_a <= InvExpXIsZero_uid146_r_uid7_fpMultAddTest_q;
    excR_bSig_uid147_r_uid7_fpMultAddTest_b <= invExpXIsMax_uid145_r_uid7_fpMultAddTest_q;
    excR_bSig_uid147_r_uid7_fpMultAddTest_q_i <= excR_bSig_uid147_r_uid7_fpMultAddTest_a and excR_bSig_uid147_r_uid7_fpMultAddTest_b;
    excR_bSig_uid147_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excR_bSig_uid147_r_uid7_fpMultAddTest_q_i, xout => excR_bSig_uid147_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist21(DELAY,378)
    redist21 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => excR_bSig_uid147_r_uid7_fpMultAddTest_q, xout => redist21_q, clk => clk, aclr => areset );

    -- redist34(DELAY,391)
    redist34 : dspba_delay
    GENERIC MAP ( width => 8, depth => 3, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist33_q, xout => redist34_q, clk => clk, aclr => areset );

    -- expXIsMax_uid126_r_uid7_fpMultAddTest(LOGICAL,125)@8
    expXIsMax_uid126_r_uid7_fpMultAddTest_a <= redist34_q;
    expXIsMax_uid126_r_uid7_fpMultAddTest_b <= cstAllOWE_uid13_p_uid6_fpMultAddTest_q;
    expXIsMax_uid126_r_uid7_fpMultAddTest_q <= "1" WHEN expXIsMax_uid126_r_uid7_fpMultAddTest_a = expXIsMax_uid126_r_uid7_fpMultAddTest_b ELSE "0";

    -- invExpXIsMax_uid131_r_uid7_fpMultAddTest(LOGICAL,130)@8
    invExpXIsMax_uid131_r_uid7_fpMultAddTest_a <= expXIsMax_uid126_r_uid7_fpMultAddTest_q;
    invExpXIsMax_uid131_r_uid7_fpMultAddTest_q <= not (invExpXIsMax_uid131_r_uid7_fpMultAddTest_a);

    -- excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest(LOGICAL,124)@8
    excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_a <= redist34_q;
    excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_b <= cstAllZWE_uid15_p_uid6_fpMultAddTest_q;
    excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_q <= "1" WHEN excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_a = excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_b ELSE "0";

    -- InvExpXIsZero_uid132_r_uid7_fpMultAddTest(LOGICAL,131)@8
    InvExpXIsZero_uid132_r_uid7_fpMultAddTest_a <= excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_q;
    InvExpXIsZero_uid132_r_uid7_fpMultAddTest_q <= not (InvExpXIsZero_uid132_r_uid7_fpMultAddTest_a);

    -- excR_aSig_uid133_r_uid7_fpMultAddTest(LOGICAL,132)@8
    excR_aSig_uid133_r_uid7_fpMultAddTest_a <= InvExpXIsZero_uid132_r_uid7_fpMultAddTest_q;
    excR_aSig_uid133_r_uid7_fpMultAddTest_b <= invExpXIsMax_uid131_r_uid7_fpMultAddTest_q;
    excR_aSig_uid133_r_uid7_fpMultAddTest_q <= excR_aSig_uid133_r_uid7_fpMultAddTest_a and excR_aSig_uid133_r_uid7_fpMultAddTest_b;

    -- signRReg_uid233_r_uid7_fpMultAddTest(LOGICAL,232)@8
    signRReg_uid233_r_uid7_fpMultAddTest_a <= excR_aSig_uid133_r_uid7_fpMultAddTest_q;
    signRReg_uid233_r_uid7_fpMultAddTest_b <= redist21_q;
    signRReg_uid233_r_uid7_fpMultAddTest_c <= redist20_q;
    signRReg_uid233_r_uid7_fpMultAddTest_d <= invAMinusA_uid232_r_uid7_fpMultAddTest_q;
    signRReg_uid233_r_uid7_fpMultAddTest_q <= signRReg_uid233_r_uid7_fpMultAddTest_a and signRReg_uid233_r_uid7_fpMultAddTest_b and signRReg_uid233_r_uid7_fpMultAddTest_c and signRReg_uid233_r_uid7_fpMultAddTest_d;

    -- redist18(DELAY,375)
    redist18 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist17_q, xout => redist18_q, clk => clk, aclr => areset );

    -- redist25(DELAY,382)
    redist25 : dspba_delay
    GENERIC MAP ( width => 1, depth => 3, reset_kind => "ASYNC" )
    PORT MAP ( xin => excZ_bSig_uid119_uid139_r_uid7_fpMultAddTest_q, xout => redist25_q, clk => clk, aclr => areset );

    -- excAZBZSigASigB_uid237_r_uid7_fpMultAddTest(LOGICAL,236)@8
    excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_a <= excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_q;
    excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_b <= redist25_q;
    excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_c <= redist20_q;
    excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_d <= redist18_q;
    excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_q <= excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_a and excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_b and excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_c and excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_d;

    -- excBZARSigA_uid238_r_uid7_fpMultAddTest(LOGICAL,237)@8
    excBZARSigA_uid238_r_uid7_fpMultAddTest_a <= redist25_q;
    excBZARSigA_uid238_r_uid7_fpMultAddTest_b <= excR_aSig_uid133_r_uid7_fpMultAddTest_q;
    excBZARSigA_uid238_r_uid7_fpMultAddTest_c <= redist20_q;
    excBZARSigA_uid238_r_uid7_fpMultAddTest_q <= excBZARSigA_uid238_r_uid7_fpMultAddTest_a and excBZARSigA_uid238_r_uid7_fpMultAddTest_b and excBZARSigA_uid238_r_uid7_fpMultAddTest_c;

    -- signRZero_uid239_r_uid7_fpMultAddTest(LOGICAL,238)@8
    signRZero_uid239_r_uid7_fpMultAddTest_a <= excBZARSigA_uid238_r_uid7_fpMultAddTest_q;
    signRZero_uid239_r_uid7_fpMultAddTest_b <= excAZBZSigASigB_uid237_r_uid7_fpMultAddTest_q;
    signRZero_uid239_r_uid7_fpMultAddTest_q <= signRZero_uid239_r_uid7_fpMultAddTest_a or signRZero_uid239_r_uid7_fpMultAddTest_b;

    -- fracXIsZero_uid141_r_uid7_fpMultAddTest(LOGICAL,140)@5
    fracXIsZero_uid141_r_uid7_fpMultAddTest_a <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
    fracXIsZero_uid141_r_uid7_fpMultAddTest_b <= redist27_q;
    fracXIsZero_uid141_r_uid7_fpMultAddTest_q <= "1" WHEN fracXIsZero_uid141_r_uid7_fpMultAddTest_a = fracXIsZero_uid141_r_uid7_fpMultAddTest_b ELSE "0";

    -- excI_bSig_uid143_r_uid7_fpMultAddTest(LOGICAL,142)@5
    excI_bSig_uid143_r_uid7_fpMultAddTest_a <= expXIsMax_uid140_r_uid7_fpMultAddTest_q;
    excI_bSig_uid143_r_uid7_fpMultAddTest_b <= fracXIsZero_uid141_r_uid7_fpMultAddTest_q;
    excI_bSig_uid143_r_uid7_fpMultAddTest_q_i <= excI_bSig_uid143_r_uid7_fpMultAddTest_a and excI_bSig_uid143_r_uid7_fpMultAddTest_b;
    excI_bSig_uid143_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excI_bSig_uid143_r_uid7_fpMultAddTest_q_i, xout => excI_bSig_uid143_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist23(DELAY,380)
    redist23 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => excI_bSig_uid143_r_uid7_fpMultAddTest_q, xout => redist23_q, clk => clk, aclr => areset );

    -- sigBBInf_uid234_r_uid7_fpMultAddTest(LOGICAL,233)@8
    sigBBInf_uid234_r_uid7_fpMultAddTest_a <= redist18_q;
    sigBBInf_uid234_r_uid7_fpMultAddTest_b <= redist23_q;
    sigBBInf_uid234_r_uid7_fpMultAddTest_q <= sigBBInf_uid234_r_uid7_fpMultAddTest_a and sigBBInf_uid234_r_uid7_fpMultAddTest_b;

    -- fracXIsZero_uid127_r_uid7_fpMultAddTest(LOGICAL,126)@6
    fracXIsZero_uid127_r_uid7_fpMultAddTest_a <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
    fracXIsZero_uid127_r_uid7_fpMultAddTest_b <= redist32_q;
    fracXIsZero_uid127_r_uid7_fpMultAddTest_q_i <= "1" WHEN fracXIsZero_uid127_r_uid7_fpMultAddTest_a = fracXIsZero_uid127_r_uid7_fpMultAddTest_b ELSE "0";
    fracXIsZero_uid127_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracXIsZero_uid127_r_uid7_fpMultAddTest_q_i, xout => fracXIsZero_uid127_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist30(DELAY,387)
    redist30 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracXIsZero_uid127_r_uid7_fpMultAddTest_q, xout => redist30_q, clk => clk, aclr => areset );

    -- excI_aSig_uid129_r_uid7_fpMultAddTest(LOGICAL,128)@8
    excI_aSig_uid129_r_uid7_fpMultAddTest_a <= expXIsMax_uid126_r_uid7_fpMultAddTest_q;
    excI_aSig_uid129_r_uid7_fpMultAddTest_b <= redist30_q;
    excI_aSig_uid129_r_uid7_fpMultAddTest_q <= excI_aSig_uid129_r_uid7_fpMultAddTest_a and excI_aSig_uid129_r_uid7_fpMultAddTest_b;

    -- sigAAInf_uid235_r_uid7_fpMultAddTest(LOGICAL,234)@8
    sigAAInf_uid235_r_uid7_fpMultAddTest_a <= redist20_q;
    sigAAInf_uid235_r_uid7_fpMultAddTest_b <= excI_aSig_uid129_r_uid7_fpMultAddTest_q;
    sigAAInf_uid235_r_uid7_fpMultAddTest_q <= sigAAInf_uid235_r_uid7_fpMultAddTest_a and sigAAInf_uid235_r_uid7_fpMultAddTest_b;

    -- signRInf_uid236_r_uid7_fpMultAddTest(LOGICAL,235)@8
    signRInf_uid236_r_uid7_fpMultAddTest_a <= sigAAInf_uid235_r_uid7_fpMultAddTest_q;
    signRInf_uid236_r_uid7_fpMultAddTest_b <= sigBBInf_uid234_r_uid7_fpMultAddTest_q;
    signRInf_uid236_r_uid7_fpMultAddTest_q <= signRInf_uid236_r_uid7_fpMultAddTest_a or signRInf_uid236_r_uid7_fpMultAddTest_b;

    -- signRInfRZRReg_uid240_r_uid7_fpMultAddTest(LOGICAL,239)@8
    signRInfRZRReg_uid240_r_uid7_fpMultAddTest_a <= signRInf_uid236_r_uid7_fpMultAddTest_q;
    signRInfRZRReg_uid240_r_uid7_fpMultAddTest_b <= signRZero_uid239_r_uid7_fpMultAddTest_q;
    signRInfRZRReg_uid240_r_uid7_fpMultAddTest_c <= signRReg_uid233_r_uid7_fpMultAddTest_q;
    signRInfRZRReg_uid240_r_uid7_fpMultAddTest_q_i <= signRInfRZRReg_uid240_r_uid7_fpMultAddTest_a or signRInfRZRReg_uid240_r_uid7_fpMultAddTest_b or signRInfRZRReg_uid240_r_uid7_fpMultAddTest_c;
    signRInfRZRReg_uid240_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => signRInfRZRReg_uid240_r_uid7_fpMultAddTest_q_i, xout => signRInfRZRReg_uid240_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- fracXIsNotZero_uid142_r_uid7_fpMultAddTest(LOGICAL,141)@5
    fracXIsNotZero_uid142_r_uid7_fpMultAddTest_a <= fracXIsZero_uid141_r_uid7_fpMultAddTest_q;
    fracXIsNotZero_uid142_r_uid7_fpMultAddTest_q <= not (fracXIsNotZero_uid142_r_uid7_fpMultAddTest_a);

    -- excN_bSig_uid144_r_uid7_fpMultAddTest(LOGICAL,143)@5
    excN_bSig_uid144_r_uid7_fpMultAddTest_a <= expXIsMax_uid140_r_uid7_fpMultAddTest_q;
    excN_bSig_uid144_r_uid7_fpMultAddTest_b <= fracXIsNotZero_uid142_r_uid7_fpMultAddTest_q;
    excN_bSig_uid144_r_uid7_fpMultAddTest_q_i <= excN_bSig_uid144_r_uid7_fpMultAddTest_a and excN_bSig_uid144_r_uid7_fpMultAddTest_b;
    excN_bSig_uid144_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excN_bSig_uid144_r_uid7_fpMultAddTest_q_i, xout => excN_bSig_uid144_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- redist22(DELAY,379)
    redist22 : dspba_delay
    GENERIC MAP ( width => 1, depth => 3, reset_kind => "ASYNC" )
    PORT MAP ( xin => excN_bSig_uid144_r_uid7_fpMultAddTest_q, xout => redist22_q, clk => clk, aclr => areset );

    -- fracXIsNotZero_uid128_r_uid7_fpMultAddTest(LOGICAL,127)@8
    fracXIsNotZero_uid128_r_uid7_fpMultAddTest_a <= redist30_q;
    fracXIsNotZero_uid128_r_uid7_fpMultAddTest_q <= not (fracXIsNotZero_uid128_r_uid7_fpMultAddTest_a);

    -- excN_aSig_uid130_r_uid7_fpMultAddTest(LOGICAL,129)@8
    excN_aSig_uid130_r_uid7_fpMultAddTest_a <= expXIsMax_uid126_r_uid7_fpMultAddTest_q;
    excN_aSig_uid130_r_uid7_fpMultAddTest_b <= fracXIsNotZero_uid128_r_uid7_fpMultAddTest_q;
    excN_aSig_uid130_r_uid7_fpMultAddTest_q_i <= excN_aSig_uid130_r_uid7_fpMultAddTest_a and excN_aSig_uid130_r_uid7_fpMultAddTest_b;
    excN_aSig_uid130_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excN_aSig_uid130_r_uid7_fpMultAddTest_q_i, xout => excN_aSig_uid130_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- excRNaN2_uid227_r_uid7_fpMultAddTest(LOGICAL,226)@9
    excRNaN2_uid227_r_uid7_fpMultAddTest_a <= excN_aSig_uid130_r_uid7_fpMultAddTest_q;
    excRNaN2_uid227_r_uid7_fpMultAddTest_b <= redist22_q;
    excRNaN2_uid227_r_uid7_fpMultAddTest_q <= excRNaN2_uid227_r_uid7_fpMultAddTest_a or excRNaN2_uid227_r_uid7_fpMultAddTest_b;

    -- redist16(DELAY,373)
    redist16 : dspba_delay
    GENERIC MAP ( width => 1, depth => 3, reset_kind => "ASYNC" )
    PORT MAP ( xin => effSub_uid154_r_uid7_fpMultAddTest_q, xout => redist16_q, clk => clk, aclr => areset );

    -- redist24(DELAY,381)
    redist24 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist23_q, xout => redist24_q, clk => clk, aclr => areset );

    -- redist29(DELAY,386)
    redist29 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excI_aSig_uid129_r_uid7_fpMultAddTest_q, xout => redist29_q, clk => clk, aclr => areset );

    -- excAIBISub_uid228_r_uid7_fpMultAddTest(LOGICAL,227)@9
    excAIBISub_uid228_r_uid7_fpMultAddTest_a <= redist29_q;
    excAIBISub_uid228_r_uid7_fpMultAddTest_b <= redist24_q;
    excAIBISub_uid228_r_uid7_fpMultAddTest_c <= redist16_q;
    excAIBISub_uid228_r_uid7_fpMultAddTest_q <= excAIBISub_uid228_r_uid7_fpMultAddTest_a and excAIBISub_uid228_r_uid7_fpMultAddTest_b and excAIBISub_uid228_r_uid7_fpMultAddTest_c;

    -- excRNaN_uid229_r_uid7_fpMultAddTest(LOGICAL,228)@9
    excRNaN_uid229_r_uid7_fpMultAddTest_a <= excAIBISub_uid228_r_uid7_fpMultAddTest_q;
    excRNaN_uid229_r_uid7_fpMultAddTest_b <= excRNaN2_uid227_r_uid7_fpMultAddTest_q;
    excRNaN_uid229_r_uid7_fpMultAddTest_q <= excRNaN_uid229_r_uid7_fpMultAddTest_a or excRNaN_uid229_r_uid7_fpMultAddTest_b;

    -- invExcRNaN_uid241_r_uid7_fpMultAddTest(LOGICAL,240)@9
    invExcRNaN_uid241_r_uid7_fpMultAddTest_a <= excRNaN_uid229_r_uid7_fpMultAddTest_q;
    invExcRNaN_uid241_r_uid7_fpMultAddTest_q <= not (invExcRNaN_uid241_r_uid7_fpMultAddTest_a);

    -- VCC(CONSTANT,1)
    VCC_q <= "1";

    -- signRPostExc_uid242_r_uid7_fpMultAddTest(LOGICAL,241)@9
    signRPostExc_uid242_r_uid7_fpMultAddTest_a <= invExcRNaN_uid241_r_uid7_fpMultAddTest_q;
    signRPostExc_uid242_r_uid7_fpMultAddTest_b <= signRInfRZRReg_uid240_r_uid7_fpMultAddTest_q;
    signRPostExc_uid242_r_uid7_fpMultAddTest_q_i <= signRPostExc_uid242_r_uid7_fpMultAddTest_a and signRPostExc_uid242_r_uid7_fpMultAddTest_b;
    signRPostExc_uid242_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => signRPostExc_uid242_r_uid7_fpMultAddTest_q_i, xout => signRPostExc_uid242_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- cRBit_uid201_r_uid7_fpMultAddTest(CONSTANT,200)
    cRBit_uid201_r_uid7_fpMultAddTest_q <= "01000";

    -- leftShiftStage2Idx1Rng1_uid352_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,351)@8
    leftShiftStage2Idx1Rng1_uid352_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(26 downto 0);
    leftShiftStage2Idx1Rng1_uid352_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStage2Idx1Rng1_uid352_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(26 downto 0);

    -- leftShiftStage2Idx1_uid353_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITJOIN,352)@8
    leftShiftStage2Idx1_uid353_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage2Idx1Rng1_uid352_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b & GND_q;

    -- leftShiftStage1Idx3Rng6_uid347_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,346)@8
    leftShiftStage1Idx3Rng6_uid347_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(21 downto 0);
    leftShiftStage1Idx3Rng6_uid347_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStage1Idx3Rng6_uid347_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(21 downto 0);

    -- leftShiftStage1Idx3Pad6_uid346_fracPostNormExt_uid190_r_uid7_fpMultAddTest(CONSTANT,345)
    leftShiftStage1Idx3Pad6_uid346_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= "000000";

    -- leftShiftStage1Idx3_uid348_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITJOIN,347)@8
    leftShiftStage1Idx3_uid348_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage1Idx3Rng6_uid347_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b & leftShiftStage1Idx3Pad6_uid346_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;

    -- leftShiftStage1Idx2Rng4_uid344_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,343)@8
    leftShiftStage1Idx2Rng4_uid344_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(23 downto 0);
    leftShiftStage1Idx2Rng4_uid344_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStage1Idx2Rng4_uid344_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(23 downto 0);

    -- leftShiftStage1Idx2_uid345_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITJOIN,344)@8
    leftShiftStage1Idx2_uid345_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage1Idx2Rng4_uid344_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b & zs_uid270_lzCountVal_uid187_r_uid7_fpMultAddTest_q;

    -- leftShiftStage1Idx1Rng2_uid341_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,340)@8
    leftShiftStage1Idx1Rng2_uid341_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(25 downto 0);
    leftShiftStage1Idx1Rng2_uid341_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStage1Idx1Rng2_uid341_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(25 downto 0);

    -- leftShiftStage1Idx1_uid342_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITJOIN,341)@8
    leftShiftStage1Idx1_uid342_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage1Idx1Rng2_uid341_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b & zs_uid276_lzCountVal_uid187_r_uid7_fpMultAddTest_q;

    -- leftShiftStage0Idx3Rng24_uid336_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,335)@8
    leftShiftStage0Idx3Rng24_uid336_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= redist11_q(3 downto 0);
    leftShiftStage0Idx3Rng24_uid336_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStage0Idx3Rng24_uid336_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(3 downto 0);

    -- leftShiftStage0Idx3Pad24_uid335_fracPostNormExt_uid190_r_uid7_fpMultAddTest(CONSTANT,334)
    leftShiftStage0Idx3Pad24_uid335_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= "000000000000000000000000";

    -- leftShiftStage0Idx3_uid337_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITJOIN,336)@8
    leftShiftStage0Idx3_uid337_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage0Idx3Rng24_uid336_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b & leftShiftStage0Idx3Pad24_uid335_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;

    -- redist4(DELAY,361)
    redist4 : dspba_delay
    GENERIC MAP ( width => 12, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => vStage_uid260_lzCountVal_uid187_r_uid7_fpMultAddTest_b, xout => redist4_q, clk => clk, aclr => areset );

    -- leftShiftStage0Idx2_uid334_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITJOIN,333)@8
    leftShiftStage0Idx2_uid334_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= redist4_q & zs_uid256_lzCountVal_uid187_r_uid7_fpMultAddTest_q;

    -- leftShiftStage0Idx1Rng8_uid330_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,329)@8
    leftShiftStage0Idx1Rng8_uid330_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= redist11_q(19 downto 0);
    leftShiftStage0Idx1Rng8_uid330_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStage0Idx1Rng8_uid330_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(19 downto 0);

    -- leftShiftStage0Idx1_uid331_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITJOIN,330)@8
    leftShiftStage0Idx1_uid331_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage0Idx1Rng8_uid330_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b & cstAllZWE_uid15_p_uid6_fpMultAddTest_q;

    -- redist11(DELAY,368)
    redist11 : dspba_delay
    GENERIC MAP ( width => 28, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracGRS_uid186_r_uid7_fpMultAddTest_q, xout => redist11_q, clk => clk, aclr => areset );

    -- leftShiftStageSel4Dto3_uid338_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,337)@8
    leftShiftStageSel4Dto3_uid338_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= r_uid285_lzCountVal_uid187_r_uid7_fpMultAddTest_q;
    leftShiftStageSel4Dto3_uid338_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStageSel4Dto3_uid338_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(4 downto 3);

    -- leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest(MUX,338)@8
    leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s <= leftShiftStageSel4Dto3_uid338_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b;
    leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest: PROCESS (leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s, redist11_q, leftShiftStage0Idx1_uid331_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q, leftShiftStage0Idx2_uid334_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q, leftShiftStage0Idx3_uid337_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s) IS
            WHEN "00" => leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= redist11_q;
            WHEN "01" => leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage0Idx1_uid331_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN "10" => leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage0Idx2_uid334_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN "11" => leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage0Idx3_uid337_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- leftShiftStageSel2Dto1_uid349_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,348)@8
    leftShiftStageSel2Dto1_uid349_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= r_uid285_lzCountVal_uid187_r_uid7_fpMultAddTest_q(2 downto 0);
    leftShiftStageSel2Dto1_uid349_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStageSel2Dto1_uid349_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(2 downto 1);

    -- leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest(MUX,349)@8
    leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s <= leftShiftStageSel2Dto1_uid349_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b;
    leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest: PROCESS (leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s, leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q, leftShiftStage1Idx1_uid342_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q, leftShiftStage1Idx2_uid345_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q, leftShiftStage1Idx3_uid348_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s) IS
            WHEN "00" => leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage0_uid339_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN "01" => leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage1Idx1_uid342_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN "10" => leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage1Idx2_uid345_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN "11" => leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage1Idx3_uid348_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- leftShiftStageSel0Dto0_uid354_fracPostNormExt_uid190_r_uid7_fpMultAddTest(BITSELECT,353)@8
    leftShiftStageSel0Dto0_uid354_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in <= r_uid285_lzCountVal_uid187_r_uid7_fpMultAddTest_q(0 downto 0);
    leftShiftStageSel0Dto0_uid354_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b <= leftShiftStageSel0Dto0_uid354_fracPostNormExt_uid190_r_uid7_fpMultAddTest_in(0 downto 0);

    -- leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest(MUX,354)@8
    leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s <= leftShiftStageSel0Dto0_uid354_fracPostNormExt_uid190_r_uid7_fpMultAddTest_b;
    leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest: PROCESS (leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s, leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q, leftShiftStage2Idx1_uid353_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q)
    BEGIN
        CASE (leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_s) IS
            WHEN "0" => leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage1_uid350_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN "1" => leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= leftShiftStage2Idx1_uid353_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
            WHEN OTHERS => leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- LSB_uid199_r_uid7_fpMultAddTest(BITSELECT,198)@8
    LSB_uid199_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(4 downto 0));
    LSB_uid199_r_uid7_fpMultAddTest_b <= LSB_uid199_r_uid7_fpMultAddTest_in(4 downto 4);

    -- Guard_uid198_r_uid7_fpMultAddTest(BITSELECT,197)@8
    Guard_uid198_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(3 downto 0));
    Guard_uid198_r_uid7_fpMultAddTest_b <= Guard_uid198_r_uid7_fpMultAddTest_in(3 downto 3);

    -- Round_uid197_r_uid7_fpMultAddTest(BITSELECT,196)@8
    Round_uid197_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(2 downto 0));
    Round_uid197_r_uid7_fpMultAddTest_b <= Round_uid197_r_uid7_fpMultAddTest_in(2 downto 2);

    -- Sticky1_uid196_r_uid7_fpMultAddTest(BITSELECT,195)@8
    Sticky1_uid196_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(1 downto 0));
    Sticky1_uid196_r_uid7_fpMultAddTest_b <= Sticky1_uid196_r_uid7_fpMultAddTest_in(1 downto 1);

    -- Sticky0_uid195_r_uid7_fpMultAddTest(BITSELECT,194)@8
    Sticky0_uid195_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q(0 downto 0));
    Sticky0_uid195_r_uid7_fpMultAddTest_b <= Sticky0_uid195_r_uid7_fpMultAddTest_in(0 downto 0);

    -- rndBitCond_uid200_r_uid7_fpMultAddTest(BITJOIN,199)@8
    rndBitCond_uid200_r_uid7_fpMultAddTest_q <= LSB_uid199_r_uid7_fpMultAddTest_b & Guard_uid198_r_uid7_fpMultAddTest_b & Round_uid197_r_uid7_fpMultAddTest_b & Sticky1_uid196_r_uid7_fpMultAddTest_b & Sticky0_uid195_r_uid7_fpMultAddTest_b;

    -- rBi_uid202_r_uid7_fpMultAddTest(LOGICAL,201)@8
    rBi_uid202_r_uid7_fpMultAddTest_a <= rndBitCond_uid200_r_uid7_fpMultAddTest_q;
    rBi_uid202_r_uid7_fpMultAddTest_b <= cRBit_uid201_r_uid7_fpMultAddTest_q;
    rBi_uid202_r_uid7_fpMultAddTest_q <= "1" WHEN rBi_uid202_r_uid7_fpMultAddTest_a = rBi_uid202_r_uid7_fpMultAddTest_b ELSE "0";

    -- roundBit_uid203_r_uid7_fpMultAddTest(LOGICAL,202)@8
    roundBit_uid203_r_uid7_fpMultAddTest_a <= rBi_uid202_r_uid7_fpMultAddTest_q;
    roundBit_uid203_r_uid7_fpMultAddTest_q_i <= not (roundBit_uid203_r_uid7_fpMultAddTest_a);
    roundBit_uid203_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => roundBit_uid203_r_uid7_fpMultAddTest_q_i, xout => roundBit_uid203_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- oneCST_uid192_r_uid7_fpMultAddTest(CONSTANT,191)
    oneCST_uid192_r_uid7_fpMultAddTest_q <= "00000001";

    -- expInc_uid193_r_uid7_fpMultAddTest(ADD,192)@8
    expInc_uid193_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("0" & redist34_q);
    expInc_uid193_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("0" & oneCST_uid192_r_uid7_fpMultAddTest_q);
    expInc_uid193_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(expInc_uid193_r_uid7_fpMultAddTest_a) + UNSIGNED(expInc_uid193_r_uid7_fpMultAddTest_b));
    expInc_uid193_r_uid7_fpMultAddTest_q <= expInc_uid193_r_uid7_fpMultAddTest_o(8 downto 0);

    -- expPostNorm_uid194_r_uid7_fpMultAddTest(SUB,193)@8
    expPostNorm_uid194_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("0" & expInc_uid193_r_uid7_fpMultAddTest_q);
    expPostNorm_uid194_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("00000" & r_uid285_lzCountVal_uid187_r_uid7_fpMultAddTest_q);
    expPostNorm_uid194_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(expPostNorm_uid194_r_uid7_fpMultAddTest_a) - UNSIGNED(expPostNorm_uid194_r_uid7_fpMultAddTest_b));
    expPostNorm_uid194_r_uid7_fpMultAddTest_q <= expPostNorm_uid194_r_uid7_fpMultAddTest_o(9 downto 0);

    -- fracPostNorm_uid191_r_uid7_fpMultAddTest(BITSELECT,190)@8
    fracPostNorm_uid191_r_uid7_fpMultAddTest_in <= leftShiftStage2_uid355_fracPostNormExt_uid190_r_uid7_fpMultAddTest_q;
    fracPostNorm_uid191_r_uid7_fpMultAddTest_b <= fracPostNorm_uid191_r_uid7_fpMultAddTest_in(27 downto 1);

    -- fracPostNormRndRange_uid204_r_uid7_fpMultAddTest(BITSELECT,203)@8
    fracPostNormRndRange_uid204_r_uid7_fpMultAddTest_in <= fracPostNorm_uid191_r_uid7_fpMultAddTest_b(25 downto 0);
    fracPostNormRndRange_uid204_r_uid7_fpMultAddTest_b <= fracPostNormRndRange_uid204_r_uid7_fpMultAddTest_in(25 downto 2);

    -- expFracR_uid205_r_uid7_fpMultAddTest(BITJOIN,204)@8
    expFracR_uid205_r_uid7_fpMultAddTest_q <= expPostNorm_uid194_r_uid7_fpMultAddTest_q & fracPostNormRndRange_uid204_r_uid7_fpMultAddTest_b;

    -- redist9(DELAY,366)
    redist9 : dspba_delay
    GENERIC MAP ( width => 34, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => expFracR_uid205_r_uid7_fpMultAddTest_q, xout => redist9_q, clk => clk, aclr => areset );

    -- rndExpFrac_uid206_r_uid7_fpMultAddTest(ADD,205)@9
    rndExpFrac_uid206_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("0" & redist9_q);
    rndExpFrac_uid206_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("0000000000000000000000000000000000" & roundBit_uid203_r_uid7_fpMultAddTest_q);
    rndExpFrac_uid206_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(rndExpFrac_uid206_r_uid7_fpMultAddTest_a) + UNSIGNED(rndExpFrac_uid206_r_uid7_fpMultAddTest_b));
    rndExpFrac_uid206_r_uid7_fpMultAddTest_q <= rndExpFrac_uid206_r_uid7_fpMultAddTest_o(34 downto 0);

    -- expRPreExc_uid220_r_uid7_fpMultAddTest(BITSELECT,219)@9
    expRPreExc_uid220_r_uid7_fpMultAddTest_in <= rndExpFrac_uid206_r_uid7_fpMultAddTest_q(31 downto 0);
    expRPreExc_uid220_r_uid7_fpMultAddTest_b <= expRPreExc_uid220_r_uid7_fpMultAddTest_in(31 downto 24);

    -- redist7(DELAY,364)
    redist7 : dspba_delay
    GENERIC MAP ( width => 8, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => expRPreExc_uid220_r_uid7_fpMultAddTest_b, xout => redist7_q, clk => clk, aclr => areset );

    -- rndExpFracOvfBits_uid211_r_uid7_fpMultAddTest(BITSELECT,210)@9
    rndExpFracOvfBits_uid211_r_uid7_fpMultAddTest_in <= rndExpFrac_uid206_r_uid7_fpMultAddTest_q(33 downto 0);
    rndExpFracOvfBits_uid211_r_uid7_fpMultAddTest_b <= rndExpFracOvfBits_uid211_r_uid7_fpMultAddTest_in(33 downto 32);

    -- rOvfExtraBits_uid212_r_uid7_fpMultAddTest(COMPARE,211)@9
    rOvfExtraBits_uid212_r_uid7_fpMultAddTest_cin <= GND_q;
    rOvfExtraBits_uid212_r_uid7_fpMultAddTest_a <= STD_LOGIC_VECTOR("00" & rndExpFracOvfBits_uid211_r_uid7_fpMultAddTest_b) & '0';
    rOvfExtraBits_uid212_r_uid7_fpMultAddTest_b <= STD_LOGIC_VECTOR("00" & zocst_uid178_r_uid7_fpMultAddTest_q) & rOvfExtraBits_uid212_r_uid7_fpMultAddTest_cin(0);
    rOvfExtraBits_uid212_r_uid7_fpMultAddTest_o <= STD_LOGIC_VECTOR(UNSIGNED(rOvfExtraBits_uid212_r_uid7_fpMultAddTest_a) - UNSIGNED(rOvfExtraBits_uid212_r_uid7_fpMultAddTest_b));
    rOvfExtraBits_uid212_r_uid7_fpMultAddTest_n(0) <= not (rOvfExtraBits_uid212_r_uid7_fpMultAddTest_o(4));

    -- wEP2AllOwE_uid207_r_uid7_fpMultAddTest(CONSTANT,206)
    wEP2AllOwE_uid207_r_uid7_fpMultAddTest_q <= "0011111111";

    -- rndExp_uid208_r_uid7_fpMultAddTest(BITSELECT,207)@9
    rndExp_uid208_r_uid7_fpMultAddTest_in <= rndExpFrac_uid206_r_uid7_fpMultAddTest_q(33 downto 0);
    rndExp_uid208_r_uid7_fpMultAddTest_b <= rndExp_uid208_r_uid7_fpMultAddTest_in(33 downto 24);

    -- rOvfEQMax_uid209_r_uid7_fpMultAddTest(LOGICAL,208)@9
    rOvfEQMax_uid209_r_uid7_fpMultAddTest_a <= rndExp_uid208_r_uid7_fpMultAddTest_b;
    rOvfEQMax_uid209_r_uid7_fpMultAddTest_b <= wEP2AllOwE_uid207_r_uid7_fpMultAddTest_q;
    rOvfEQMax_uid209_r_uid7_fpMultAddTest_q <= "1" WHEN rOvfEQMax_uid209_r_uid7_fpMultAddTest_a = rOvfEQMax_uid209_r_uid7_fpMultAddTest_b ELSE "0";

    -- rOvf_uid213_r_uid7_fpMultAddTest(LOGICAL,212)@9
    rOvf_uid213_r_uid7_fpMultAddTest_a <= rOvfEQMax_uid209_r_uid7_fpMultAddTest_q;
    rOvf_uid213_r_uid7_fpMultAddTest_b <= rOvfExtraBits_uid212_r_uid7_fpMultAddTest_n;
    rOvf_uid213_r_uid7_fpMultAddTest_q <= rOvf_uid213_r_uid7_fpMultAddTest_a or rOvf_uid213_r_uid7_fpMultAddTest_b;

    -- regInputs_uid221_r_uid7_fpMultAddTest(LOGICAL,220)@8
    regInputs_uid221_r_uid7_fpMultAddTest_a <= excR_aSig_uid133_r_uid7_fpMultAddTest_q;
    regInputs_uid221_r_uid7_fpMultAddTest_b <= redist21_q;
    regInputs_uid221_r_uid7_fpMultAddTest_q_i <= regInputs_uid221_r_uid7_fpMultAddTest_a and regInputs_uid221_r_uid7_fpMultAddTest_b;
    regInputs_uid221_r_uid7_fpMultAddTest_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => regInputs_uid221_r_uid7_fpMultAddTest_q_i, xout => regInputs_uid221_r_uid7_fpMultAddTest_q, clk => clk, aclr => areset );

    -- rInfOvf_uid224_r_uid7_fpMultAddTest(LOGICAL,223)@9
    rInfOvf_uid224_r_uid7_fpMultAddTest_a <= regInputs_uid221_r_uid7_fpMultAddTest_q;
    rInfOvf_uid224_r_uid7_fpMultAddTest_b <= rOvf_uid213_r_uid7_fpMultAddTest_q;
    rInfOvf_uid224_r_uid7_fpMultAddTest_q <= rInfOvf_uid224_r_uid7_fpMultAddTest_a and rInfOvf_uid224_r_uid7_fpMultAddTest_b;

    -- excRInfVInC_uid225_r_uid7_fpMultAddTest(BITJOIN,224)@9
    excRInfVInC_uid225_r_uid7_fpMultAddTest_q <= rInfOvf_uid224_r_uid7_fpMultAddTest_q & redist22_q & excN_aSig_uid130_r_uid7_fpMultAddTest_q & redist24_q & redist29_q & redist16_q;

    -- excRInf_uid226_r_uid7_fpMultAddTest(LOOKUP,225)@9
    excRInf_uid226_r_uid7_fpMultAddTest: PROCESS (excRInfVInC_uid225_r_uid7_fpMultAddTest_q)
    BEGIN
        -- Begin reserved scope level
        CASE (excRInfVInC_uid225_r_uid7_fpMultAddTest_q) IS
            WHEN "000000" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "000001" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "000010" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "1";
            WHEN "000011" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "1";
            WHEN "000100" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "1";
            WHEN "000101" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "1";
            WHEN "000110" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "1";
            WHEN "000111" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "001000" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "001001" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "001010" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "001011" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "001100" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "001101" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "001110" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "001111" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "010000" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "010001" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "010010" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "010011" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "010100" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "010101" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "010110" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "010111" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "011000" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "011001" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "011010" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "011011" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "011100" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "011101" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "011110" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "011111" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "100000" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "1";
            WHEN "100001" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "100010" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "100011" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "100100" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "100101" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "100110" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "100111" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "101000" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "101001" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "101010" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "101011" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "101100" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "101101" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "101110" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "101111" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "110000" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "110001" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "110010" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "110011" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "110100" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "110101" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "110110" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "110111" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "111000" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "111001" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "111010" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "111011" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "111100" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "111101" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "111110" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN "111111" => excRInf_uid226_r_uid7_fpMultAddTest_q <= "0";
            WHEN OTHERS => -- unreachable
                           excRInf_uid226_r_uid7_fpMultAddTest_q <= (others => '-');
        END CASE;
        -- End reserved scope level
    END PROCESS;

    -- redist10(DELAY,367)
    redist10 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => aMinusA_uid189_r_uid7_fpMultAddTest_q, xout => redist10_q, clk => clk, aclr => areset );

    -- rUdfExtraBit_uid217_r_uid7_fpMultAddTest(BITSELECT,216)@9
    rUdfExtraBit_uid217_r_uid7_fpMultAddTest_in <= STD_LOGIC_VECTOR(rndExpFrac_uid206_r_uid7_fpMultAddTest_q(33 downto 0));
    rUdfExtraBit_uid217_r_uid7_fpMultAddTest_b <= rUdfExtraBit_uid217_r_uid7_fpMultAddTest_in(33 downto 33);

    -- wEP2AllZ_uid214_r_uid7_fpMultAddTest(CONSTANT,213)
    wEP2AllZ_uid214_r_uid7_fpMultAddTest_q <= "0000000000";

    -- rUdfEQMin_uid216_r_uid7_fpMultAddTest(LOGICAL,215)@9
    rUdfEQMin_uid216_r_uid7_fpMultAddTest_a <= rndExp_uid208_r_uid7_fpMultAddTest_b;
    rUdfEQMin_uid216_r_uid7_fpMultAddTest_b <= wEP2AllZ_uid214_r_uid7_fpMultAddTest_q;
    rUdfEQMin_uid216_r_uid7_fpMultAddTest_q <= "1" WHEN rUdfEQMin_uid216_r_uid7_fpMultAddTest_a = rUdfEQMin_uid216_r_uid7_fpMultAddTest_b ELSE "0";

    -- rUdf_uid218_r_uid7_fpMultAddTest(LOGICAL,217)@9
    rUdf_uid218_r_uid7_fpMultAddTest_a <= rUdfEQMin_uid216_r_uid7_fpMultAddTest_q;
    rUdf_uid218_r_uid7_fpMultAddTest_b <= rUdfExtraBit_uid217_r_uid7_fpMultAddTest_b;
    rUdf_uid218_r_uid7_fpMultAddTest_q <= rUdf_uid218_r_uid7_fpMultAddTest_a or rUdf_uid218_r_uid7_fpMultAddTest_b;

    -- redist26(DELAY,383)
    redist26 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist25_q, xout => redist26_q, clk => clk, aclr => areset );

    -- redist31(DELAY,388)
    redist31 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excZ_aSig_uid118_uid125_r_uid7_fpMultAddTest_q, xout => redist31_q, clk => clk, aclr => areset );

    -- excRZeroVInC_uid222_r_uid7_fpMultAddTest(BITJOIN,221)@9
    excRZeroVInC_uid222_r_uid7_fpMultAddTest_q <= redist10_q & rUdf_uid218_r_uid7_fpMultAddTest_q & regInputs_uid221_r_uid7_fpMultAddTest_q & redist26_q & redist31_q;

    -- excRZero_uid223_r_uid7_fpMultAddTest(LOOKUP,222)@9
    excRZero_uid223_r_uid7_fpMultAddTest: PROCESS (excRZeroVInC_uid222_r_uid7_fpMultAddTest_q)
    BEGIN
        -- Begin reserved scope level
        CASE (excRZeroVInC_uid222_r_uid7_fpMultAddTest_q) IS
            WHEN "00000" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "00001" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "00010" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "00011" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "1";
            WHEN "00100" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "00101" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "00110" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "00111" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "01000" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "01001" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "01010" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "01011" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "1";
            WHEN "01100" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "1";
            WHEN "01101" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "01110" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "01111" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "10000" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "10001" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "10010" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "10011" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "1";
            WHEN "10100" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "1";
            WHEN "10101" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "10110" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "10111" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "11000" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "11001" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "11010" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "11011" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "1";
            WHEN "11100" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "1";
            WHEN "11101" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "11110" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN "11111" => excRZero_uid223_r_uid7_fpMultAddTest_q <= "0";
            WHEN OTHERS => -- unreachable
                           excRZero_uid223_r_uid7_fpMultAddTest_q <= (others => '-');
        END CASE;
        -- End reserved scope level
    END PROCESS;

    -- concExc_uid230_r_uid7_fpMultAddTest(BITJOIN,229)@9
    concExc_uid230_r_uid7_fpMultAddTest_q <= excRNaN_uid229_r_uid7_fpMultAddTest_q & excRInf_uid226_r_uid7_fpMultAddTest_q & excRZero_uid223_r_uid7_fpMultAddTest_q;

    -- redist6(DELAY,363)
    redist6 : dspba_delay
    GENERIC MAP ( width => 3, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => concExc_uid230_r_uid7_fpMultAddTest_q, xout => redist6_q, clk => clk, aclr => areset );

    -- excREnc_uid231_r_uid7_fpMultAddTest(LOOKUP,230)@10
    excREnc_uid231_r_uid7_fpMultAddTest: PROCESS (redist6_q)
    BEGIN
        -- Begin reserved scope level
        CASE (redist6_q) IS
            WHEN "000" => excREnc_uid231_r_uid7_fpMultAddTest_q <= "01";
            WHEN "001" => excREnc_uid231_r_uid7_fpMultAddTest_q <= "00";
            WHEN "010" => excREnc_uid231_r_uid7_fpMultAddTest_q <= "10";
            WHEN "011" => excREnc_uid231_r_uid7_fpMultAddTest_q <= "10";
            WHEN "100" => excREnc_uid231_r_uid7_fpMultAddTest_q <= "11";
            WHEN "101" => excREnc_uid231_r_uid7_fpMultAddTest_q <= "11";
            WHEN "110" => excREnc_uid231_r_uid7_fpMultAddTest_q <= "11";
            WHEN "111" => excREnc_uid231_r_uid7_fpMultAddTest_q <= "11";
            WHEN OTHERS => -- unreachable
                           excREnc_uid231_r_uid7_fpMultAddTest_q <= (others => '-');
        END CASE;
        -- End reserved scope level
    END PROCESS;

    -- expRPostExc_uid250_r_uid7_fpMultAddTest(MUX,249)@10
    expRPostExc_uid250_r_uid7_fpMultAddTest_s <= excREnc_uid231_r_uid7_fpMultAddTest_q;
    expRPostExc_uid250_r_uid7_fpMultAddTest: PROCESS (expRPostExc_uid250_r_uid7_fpMultAddTest_s, cstAllZWE_uid15_p_uid6_fpMultAddTest_q, redist7_q, cstAllOWE_uid13_p_uid6_fpMultAddTest_q)
    BEGIN
        CASE (expRPostExc_uid250_r_uid7_fpMultAddTest_s) IS
            WHEN "00" => expRPostExc_uid250_r_uid7_fpMultAddTest_q <= cstAllZWE_uid15_p_uid6_fpMultAddTest_q;
            WHEN "01" => expRPostExc_uid250_r_uid7_fpMultAddTest_q <= redist7_q;
            WHEN "10" => expRPostExc_uid250_r_uid7_fpMultAddTest_q <= cstAllOWE_uid13_p_uid6_fpMultAddTest_q;
            WHEN "11" => expRPostExc_uid250_r_uid7_fpMultAddTest_q <= cstAllOWE_uid13_p_uid6_fpMultAddTest_q;
            WHEN OTHERS => expRPostExc_uid250_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- fracRPreExc_uid219_r_uid7_fpMultAddTest(BITSELECT,218)@9
    fracRPreExc_uid219_r_uid7_fpMultAddTest_in <= rndExpFrac_uid206_r_uid7_fpMultAddTest_q(23 downto 0);
    fracRPreExc_uid219_r_uid7_fpMultAddTest_b <= fracRPreExc_uid219_r_uid7_fpMultAddTest_in(23 downto 1);

    -- redist8(DELAY,365)
    redist8 : dspba_delay
    GENERIC MAP ( width => 23, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracRPreExc_uid219_r_uid7_fpMultAddTest_b, xout => redist8_q, clk => clk, aclr => areset );

    -- fracRPostExc_uid246_r_uid7_fpMultAddTest(MUX,245)@10
    fracRPostExc_uid246_r_uid7_fpMultAddTest_s <= excREnc_uid231_r_uid7_fpMultAddTest_q;
    fracRPostExc_uid246_r_uid7_fpMultAddTest: PROCESS (fracRPostExc_uid246_r_uid7_fpMultAddTest_s, cstZeroWF_uid14_p_uid6_fpMultAddTest_q, redist8_q, oneFracRPostExc2_uid95_p_uid6_fpMultAddTest_q)
    BEGIN
        CASE (fracRPostExc_uid246_r_uid7_fpMultAddTest_s) IS
            WHEN "00" => fracRPostExc_uid246_r_uid7_fpMultAddTest_q <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
            WHEN "01" => fracRPostExc_uid246_r_uid7_fpMultAddTest_q <= redist8_q;
            WHEN "10" => fracRPostExc_uid246_r_uid7_fpMultAddTest_q <= cstZeroWF_uid14_p_uid6_fpMultAddTest_q;
            WHEN "11" => fracRPostExc_uid246_r_uid7_fpMultAddTest_q <= oneFracRPostExc2_uid95_p_uid6_fpMultAddTest_q;
            WHEN OTHERS => fracRPostExc_uid246_r_uid7_fpMultAddTest_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- R_uid251_r_uid7_fpMultAddTest(BITJOIN,250)@10
    R_uid251_r_uid7_fpMultAddTest_q <= signRPostExc_uid242_r_uid7_fpMultAddTest_q & expRPostExc_uid250_r_uid7_fpMultAddTest_q & fracRPostExc_uid246_r_uid7_fpMultAddTest_q;

    -- xOut(GPOUT,4)@10
    q <= R_uid251_r_uid7_fpMultAddTest_q;

END normal;
